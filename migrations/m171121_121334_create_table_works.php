<?php

use yii\db\Migration;

class m171121_121334_create_table_works extends Migration
{
    public function safeUp()
    {
        $this->createTable('works', [
            'id' => $this->primaryKey(),
            'subject_id' => $this->integer()->comment('Дисциплина'),
            'theme' => $this->string()->comment('Тема работы'),
            'work' => $this->string()->comment('Работа (ссылка на файл)'),
            'review' => $this->string()->comment('Рецензия (ссылка на файл)'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);

        $this->createIndex(
            'idx-works-subjects',
            'works',
            'subject_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-works-subjects',
            'works',
            'subject_id',
            'subjects',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-works-subjects', 'works');

        $this->dropIndex('idx-works-subjects', 'works');

        $this->dropTable('works');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171121_121334_create_table_works cannot be reverted.\n";

        return false;
    }
    */
}
