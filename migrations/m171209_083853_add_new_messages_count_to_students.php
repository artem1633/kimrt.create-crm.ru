<?php

use yii\db\Migration;

class m171209_083853_add_new_messages_count_to_students extends Migration
{
    public function safeUp()
    {
        $this->addColumn('student', 'new_messages', $this->integer()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('student', 'new_messages');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171209_083853_add_new_messages_count_to_students cannot be reverted.\n";

        return false;
    }
    */
}
