<?php

use yii\db\Migration;

class m171130_060331_add_student_id_to_autobiography extends Migration
{
    public function safeUp()
    {
        $this->addColumn('autobiography', 'student_id', $this->integer()->null());

        $this->createIndex(
            'idx-autobiography-student',
            'autobiography',
            'student_id'
        );

        $this->addForeignKey(
            'fk-autobiography-student',
            'autobiography',
            'student_id',
            'student',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-autobiography-student', 'autobiography');

        $this->dropIndex('idx-autobiography-student', 'autobiography');

        $this->dropColumn('autobiography', 'student_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_060331_add_student_id_to_autobiography cannot be reverted.\n";

        return false;
    }
    */
}
