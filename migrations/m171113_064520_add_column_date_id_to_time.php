<?php

use yii\db\Migration;

class m171113_064520_add_column_date_id_to_time extends Migration
{
    public function safeUp()
    {
        $this->addColumn('time', 'date_id', $this->integer()->unsigned()->null());

        /*
         * При добавлении столбца, сразу привязал к существующей дате
         */
        \app\models\Time::updateAll(['date_id'=>1]);
    }

    public function safeDown()
    {
        $this->dropColumn('time', 'date_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171113_064520_add_column_date_id_to_time cannot be reverted.\n";

        return false;
    }
    */
}
