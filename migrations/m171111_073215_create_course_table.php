<?php

use yii\db\Migration;

/**
 * Handles the creation of table `course`.
 */
class m171111_073215_create_course_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('course', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Наименование'),
            'comment' => $this->string()->comment('Коментарий'),
        ]);

        /*-----------------------------------*/
        $this->createIndex(
            'idx-applications-course_id',
            'student',
            'course_id'
        );

        $this->addForeignKey(
            'fk-applications-course_id',
            'student',
            'course_id',
            'course',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        /*-----------------------------------*/
        $this->dropForeignKey(
            'fk-applications-course_id',
            'student'
        );

        $this->dropIndex(
            'idx-applications-course_id',
            'student'
        );
        $this->dropTable('course');
    }
}
