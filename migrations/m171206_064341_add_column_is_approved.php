<?php

use yii\db\Migration;

class m171206_064341_add_column_is_approved extends Migration
{
    public function safeUp()
    {
        $this->addColumn('additional_education', 'is_approved', $this->boolean()->null());
        $this->addColumn('art', 'is_approved', $this->boolean()->null());
        $this->addColumn('contests', 'is_approved', $this->boolean()->null());
        $this->addColumn('extracurricular', 'is_approved', $this->boolean()->null());
        $this->addColumn('olympiads', 'is_approved', $this->boolean()->null());
        $this->addColumn('scientific_projects', 'is_approved', $this->boolean()->null());
        $this->addColumn('sport', 'is_approved', $this->boolean()->null());
        $this->addColumn('works', 'is_approved', $this->boolean()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('additional_education', 'is_approved');
        $this->dropColumn('art', 'is_approved');
        $this->dropColumn('contests', 'is_approved');
        $this->dropColumn('extracurricular', 'is_approved');
        $this->dropColumn('olympiads', 'is_approved');
        $this->dropColumn('scientific_projects', 'is_approved');
        $this->dropColumn('sport', 'is_approved');
        $this->dropColumn('works', 'is_approved');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171206_064341_add_column_is_approved cannot be reverted.\n";

        return false;
    }
    */
}
