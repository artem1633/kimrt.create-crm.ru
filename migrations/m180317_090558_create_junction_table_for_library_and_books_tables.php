<?php

use yii\db\Migration;

/**
 * Handles the creation of table `library_books`.
 * Has foreign keys to the tables:
 *
 * - `library`
 * - `books`
 */
class m180317_090558_create_junction_table_for_library_and_books_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('library_books', [
            'library_id' => $this->integer()->comment('Библиотека'),
            'books_id' => $this->integer()->comment('Издание'),
            'quantity' => $this->integer()->notNull()->unsigned()->defaultValue(0)->comment('Количество копий'),
            'PRIMARY KEY(library_id, books_id)',
        ]);

        $this->addCommentOnTable('library_books', 'Утилизирующая таблица для отображения остатков копий изданий в библиотеках');

        // creates index for column `library_id`
        $this->createIndex(
            'idx-library_books-library_id',
            'library_books',
            'library_id'
        );

        // add foreign key for table `library`
        $this->addForeignKey(
            'fk-library_books-library_id',
            'library_books',
            'library_id',
            'library',
            'id',
            'CASCADE'
        );

        // creates index for column `books_id`
        $this->createIndex(
            'idx-library_books-books_id',
            'library_books',
            'books_id'
        );

        // add foreign key for table `books`
        $this->addForeignKey(
            'fk-library_books-books_id',
            'library_books',
            'books_id',
            'books',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `library`
        $this->dropForeignKey(
            'fk-library_books-library_id',
            'library_books'
        );

        // drops index for column `library_id`
        $this->dropIndex(
            'idx-library_books-library_id',
            'library_books'
        );

        // drops foreign key for table `books`
        $this->dropForeignKey(
            'fk-library_books-books_id',
            'library_books'
        );

        // drops index for column `books_id`
        $this->dropIndex(
            'idx-library_books-books_id',
            'library_books'
        );

        $this->dropTable('library_books');
    }
}
