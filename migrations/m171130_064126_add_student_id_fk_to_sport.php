<?php

use yii\db\Migration;

class m171130_064126_add_student_id_fk_to_sport extends Migration
{
    public function safeUp()
    {
        $this->addColumn('sport', 'student_id', $this->integer()->null());

        $this->createIndex(
            'idx-sport-student',
            'sport',
            'student_id'
        );

        $this->addForeignKey(
            'fk-sport-student',
            'sport',
            'student_id',
            'student',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-sport-student', 'sport');

        $this->dropIndex('idx-sport-student', 'sport');

        $this->dropColumn('sport', 'student_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_064126_add_student_id_fk_to_sport cannot be reverted.\n";

        return false;
    }
    */
}
