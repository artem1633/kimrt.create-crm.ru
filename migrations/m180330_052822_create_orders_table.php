<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m180330_052822_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'record_book' => $this->string(255),
            'book_id' => $this->integer(),
            'library_fond' => $this->integer(),
            'planing_date' => $this->date(),
        ]);

        $this->createIndex('idx-orders-user_id', 'orders', 'user_id', false);
        $this->addForeignKey("fk-orders-user_id", "orders", "user_id", "users", "id");

        $this->createIndex('idx-orders-book_id', 'orders', 'book_id', false);
        $this->addForeignKey("fk-orders-book_id", "orders", "book_id", "books", "id");

        $this->createIndex('idx-orders-library_fond', 'orders', 'library_fond', false);
        $this->addForeignKey("fk-orders-library_fond", "orders", "library_fond", "foundation", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-orders-user_id','orders');
        $this->dropIndex('idx-orders-user_id','orders');

        $this->dropForeignKey('fk-orders-book_id','orders');
        $this->dropIndex('idx-orders-book_id','orders');

        $this->dropForeignKey('fk-orders-library_fond','orders');
        $this->dropIndex('idx-orders-library_fond','orders');

        $this->dropTable('orders');
    }
}
