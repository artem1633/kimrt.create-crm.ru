<?php

use yii\db\Migration;

class m171129_091402_add_user_id_fk_to_students extends Migration
{
    public function safeUp()
    {
        $this->addColumn('student', 'user_id', $this->integer()->null());

        $this->createIndex(
            'idx-student-users',
            'student',
            'user_id'
        );

        $this->addForeignKey(
            'fk-student-users',
            'student',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-student-users', 'student');

        $this->dropIndex('idx-student-users', 'student');

        $this->dropColumn('student', 'user_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171129_091402_add_user_id_fk_to_students cannot be reverted.\n";

        return false;
    }
    */
}
