<?php

use yii\db\Migration;

class m171116_145841_add_columns_group_id_room_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('schedule', 'group_id', $this->integer()->unsigned()->null());
        $this->addColumn('schedule', 'room_id', $this->integer()->unsigned()->null());

        /*
         * При добавлении столбца, сразу привязал к существующей дате
         */
        \app\models\Schedule::updateAll(['date_id'=>1]);
    }

    public function safeDown()
    {
        $this->dropColumn('schedule', 'group_id');
        $this->dropColumn('schedule', 'room_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171116_145841_add_columns_group_id_room_id cannot be reverted.\n";

        return false;
    }
    */
}
