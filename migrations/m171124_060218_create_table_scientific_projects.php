<?php

use yii\db\Migration;

class m171124_060218_create_table_scientific_projects extends Migration
{
    public function safeUp()
    {
        $this->createTable('scientific_projects', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название конференции'),
            'level' => $this->string()->comment('Уровень (внутривузовский, районный)'),
            'date' => $this->date()->comment('Дата конференции'),
            'theme' => $this->string()->comment('Тема выступления'),
            'publication' => $this->string()->comment('Наличие публикации'),
            'work' => $this->string()->comment('Загрузка файла'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('scientific_projects');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171124_060218_create_table_scientific_projects cannot be reverted.\n";

        return false;
    }
    */
}
