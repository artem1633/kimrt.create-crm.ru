<?php

use yii\db\Migration;

class m171121_100209_create_table_autobiography extends Migration
{
    public function safeUp()
    {
        $this->createTable('autobiography', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'text' => $this->text()->comment('Текст автобиографии'),
            'is_approved' => $this->boolean()->comment('Проверено админом'),
            'comment' => $this->string()->comment('Коментарий'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);

        $this->createIndex(
            'idx-autobiography-user',
            'autobiography',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-autobiography-user',
            'autobiography',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-autobiography-user', 'autobiography');

        $this->dropIndex('idx-autobiography-user', 'autobiography');

        $this->dropTable('autobiography');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171121_100209_create_table_autobiography cannot be reverted.\n";

        return false;
    }
    */
}
