<?php

use yii\db\Migration;

class m171113_050318_create_table_teachers extends Migration
{
    public function safeUp()
    {
        $this->createTable('teachers', [
            'id' => $this->primaryKey()->unsigned(),
            'full_name' => $this->string()->comment('ФИО преподавателя'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);

        $this->insert('teachers', [
            'full_name' => 'Иванов И.И.',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('teachers');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171113_050318_create_table_teachers cannot be reverted.\n";

        return false;
    }
    */
}
