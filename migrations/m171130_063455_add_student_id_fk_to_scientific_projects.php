<?php

use yii\db\Migration;

class m171130_063455_add_student_id_fk_to_scientific_projects extends Migration
{
    public function safeUp()
    {
        $this->addColumn('scientific_projects', 'student_id', $this->integer()->null());

        $this->createIndex(
            'idx-scientific_projects-student',
            'scientific_projects',
            'student_id'
        );

        $this->addForeignKey(
            'fk-scientific_projects-student',
            'scientific_projects',
            'student_id',
            'student',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-scientific_projects-student', 'scientific_projects');

        $this->dropIndex('idx-scientific_projects-student', 'scientific_projects');

        $this->dropColumn('scientific_projects', 'student_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_063455_add_student_id_fk_to_scientific_projects cannot be reverted.\n";

        return false;
    }
    */
}
