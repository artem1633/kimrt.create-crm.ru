<?php

use yii\db\Migration;

class m171113_050829_add_column_teacher_to_scheule extends Migration
{
    public function safeUp()
    {
        $this->addColumn('schedule', 'teacher_id', $this->integer()->unsigned()->null());

        /*
         * При добавлении учителя, привязываю к существующему учителю
         */
        \app\models\Schedule::updateAll(['teacher_id'=>1]);
    }

    public function safeDown()
    {
        $this->dropColumn('schedule', 'teacher_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171113_050829_add_column_teacher_to_scheule cannot be reverted.\n";

        return false;
    }
    */
}
