<?php

use yii\db\Migration;

class m171204_053554_add_fk_student_id_to_academic_perfomance extends Migration
{
    public function safeUp()
    {
        $this->addColumn('academic_perfomance', 'student_id', $this->integer()->null());

        $this->createIndex(
            'idx-academic_perfomance-student',
            'academic_perfomance',
            'student_id'
        );

        $this->addForeignKey(
            'fk-academic_perfomance-student',
            'academic_perfomance',
            'student_id',
            'student',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-academic_perfomance-student', 'academic_perfomance');

        $this->dropIndex('idx-academic_perfomance-student', 'academic_perfomance');

        $this->dropColumn('academic_perfomance', 'student_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171204_053554_add_fk_student_id_to_academic_perfomance cannot be reverted.\n";

        return false;
    }
    */
}
