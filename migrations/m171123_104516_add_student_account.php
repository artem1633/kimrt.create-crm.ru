<?php

use yii\db\Migration;

class m171123_104516_add_student_account extends Migration
{
    public function safeUp()
    {
        $this->insert('users', [
            'name' => 'Студент',
            'login' => 'student',
            'password' => md5('student'),
            'type' => 'student',
            'is_deletable' => false,
        ]);
    }

    public function safeDown()
    {
        echo "m171123_104516_add_student_account cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171123_104516_add_student_account cannot be reverted.\n";

        return false;
    }
    */
}
