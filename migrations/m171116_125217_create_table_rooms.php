<?php

use yii\db\Migration;

class m171116_125217_create_table_rooms extends Migration
{
    public function safeUp()
    {
        $this->createTable('rooms', [
            'id' => $this->primaryKey()->unsigned(),
            'number' => $this->string()->comment('Номер аудитории'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);

        /*
         * Сохранение первичных данных, чтобы не заполнять вручную
         */
        $this->insert('rooms', [
            'number' => '302',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('rooms');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171116_125217_create_table_rooms cannot be reverted.\n";

        return false;
    }
    */
}
