<?php

use yii\db\Migration;

class m171123_172028_create_table_additional_education extends Migration
{
    public function safeUp()
    {
        $this->createTable('additional_education', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название программы'),
            'hours' => $this->string()->comment('Количество часов'),
            'location_time' => $this->string()->comment('Место и время обучения'),
            'document_name' => $this->time()->comment('Название документа'),
            'document' => $this->string()->comment('Ссылка на загруженный файл'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('additional_education');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171123_172028_create_table_additional_education cannot be reverted.\n";

        return false;
    }
    */
}
