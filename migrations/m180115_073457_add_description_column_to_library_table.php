<?php

use yii\db\Migration;

/**
 * Handles adding description to table `library`.
 */
class m180115_073457_add_description_column_to_library_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('library', 'description', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('library', 'description');
    }
}
