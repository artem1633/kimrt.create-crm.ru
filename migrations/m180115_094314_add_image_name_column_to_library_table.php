<?php

use yii\db\Migration;

/**
 * Handles adding image_name to table `library`.
 */
class m180115_094314_add_image_name_column_to_library_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('library', 'image_name', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('library', 'image_name');
    }
}
