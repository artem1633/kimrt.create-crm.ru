<?php

use yii\db\Migration;

/**
 * Handles the creation of table `books_sections`.
 */
class m180315_215202_create_books_sections_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('books_sections', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование раздела'),
        ]);
        $this->addCommentOnTable('books_sections', 'Разделы книг');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('books_sections');
    }
}
