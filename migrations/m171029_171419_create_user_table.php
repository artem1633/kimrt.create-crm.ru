<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m171029_171419_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Имя'),
            'login' => $this->string()->notNull()->comment('Логин'),
            'password' => $this->string()->notNull()->comment('Зашифрованный пароль'),
            'type' => $this->string()->notNull()->comment('Тип пользователя пароль'),
            'is_deletable' => $this->boolean()->defaultValue(true)->comment('Можно удалить или нельзя'),
            'comment' => $this->string()->comment('Коментарий'),
        ]);

        $this->insert('users', [
            'name' => 'Администратор',
            'login' => 'admin',
            'password' => md5('admin'),
            'type' => 'admin',
            'is_deletable' => false,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
