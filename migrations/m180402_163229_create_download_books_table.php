<?php

use yii\db\Migration;

/**
 * Handles the creation of table `download_books`.
 */
class m180402_163229_create_download_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('download_books', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'book_id' => $this->integer(),
            'foundation_id' => $this->integer(),
            'return_date' => $this->date(),
        ]);

        $this->createIndex('idx-download_books-user_id', 'download_books', 'user_id', false);
        $this->addForeignKey("fk-download_books-user_id", "download_books", "user_id", "users", "id");

        $this->createIndex('idx-download_books-book_id', 'download_books', 'book_id', false);
        $this->addForeignKey("fk-download_books-book_id", "download_books", "book_id", "books", "id");

        $this->createIndex('idx-download_books-foundation_id', 'download_books', 'foundation_id', false);
        $this->addForeignKey("fk-download_books-foundation_id", "download_books", "foundation_id", "foundation", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-download_books-user_id','download_books');
        $this->dropIndex('idx-download_books-user_id','download_books');

        $this->dropForeignKey('fk-download_books-book_id','download_books');
        $this->dropIndex('idx-download_books-book_id','download_books');

        $this->dropForeignKey('fk-download_books-foundation_id','download_books');
        $this->dropIndex('idx-download_books-foundation_id','download_books');

        $this->dropTable('download_books');
    }
}
