<?php

use yii\db\Migration;

class m171130_063942_add_student_id_fk_to_contests extends Migration
{
    public function safeUp()
    {
        $this->addColumn('contests', 'student_id', $this->integer()->null());

        $this->createIndex(
            'idx-contests-student',
            'contests',
            'student_id'
        );

        $this->addForeignKey(
            'fk-contests-student',
            'contests',
            'student_id',
            'student',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-contests-student', 'contests');

        $this->dropIndex('idx-contests-student', 'contests');

        $this->dropColumn('contests', 'student_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_063942_add_student_id_fk_to_contests cannot be reverted.\n";

        return false;
    }
    */
}
