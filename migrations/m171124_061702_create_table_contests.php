<?php

use yii\db\Migration;

class m171124_061702_create_table_contests extends Migration
{
    public function safeUp()
    {
        $this->createTable('contests', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название конкурса'),
            'level' => $this->string()->comment('Уровень (внутривузовский, районный)'),
            'date' => $this->date()->comment('Дата проведения'),
            'theme' => $this->string()->comment('Тема работы'),
            'result' => $this->string()->comment('Результат (сертификат, грамота)'),
            'work' => $this->string()->comment('Загрузка файла'),
            'document' => $this->string()->comment('Загрузка файла'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('contests');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171124_061702_create_table_contests cannot be reverted.\n";

        return false;
    }
    */
}
