<?php

use yii\db\Migration;

/**
 * Handles the creation of table `library`.
 */
class m180113_164307_create_library_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('library', [
            'id' => $this->primaryKey(),
            'ssilka' => $this->string(255)->notNull(),
            'logotip' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('library');
    }
}
