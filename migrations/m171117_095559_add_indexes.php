<?php

use yii\db\Migration;

class m171117_095559_add_indexes extends Migration
{
    public function safeUp()
    {
        $this->execute("SET foreign_key_checks = 0;");
        // creates index for column `author_id`
        $this->createIndex(
            'idx-schedule-time',
            'schedule',
            'time_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-schedule-time',
            'schedule',
            'time_id',
            'time',
            'id',
            'CASCADE'
        );

        // creates index for column `author_id`
        $this->createIndex(
            'idx-schedule-week_days',
            'schedule',
            'day_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-schedule-week_days',
            'schedule',
            'day_id',
            'week_days',
            'id',
            'CASCADE'
        );

        // creates index for column `author_id`
        $this->createIndex(
            'idx-schedule-teachers',
            'schedule',
            'teacher_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-schedule-teachers',
            'schedule',
            'teacher_id',
            'teachers',
            'id',
            'CASCADE'
        );

        $this->execute("SET foreign_key_checks = 1;");
    }

    public function safeDown()
    {
        $this->execute("SET foreign_key_checks = 0;");
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-schedule-time',
            'schedule'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-schedule-time',
            'schedule'
        );
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-schedule-week_days',
            'schedule'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-schedule-week_days',
            'schedule'
        );
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-schedule-teachers',
            'schedule'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-schedule-teachers',
            'schedule'
        );
        $this->execute("SET foreign_key_checks = 1;");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171117_095559_add_indexes cannot be reverted.\n";

        return false;
    }
    */
}
