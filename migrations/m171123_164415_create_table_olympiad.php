<?php

use yii\db\Migration;

class m171123_164415_create_table_olympiad extends Migration
{
    public function safeUp()
    {
        $this->createTable('olympiads', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'level' => $this->string()->comment('Уровень (районный, городской)'),
            'location' => $this->string()->comment('Место проведения'),
            'time' => $this->time()->comment('Время проведения'),
            'result' => $this->string()->comment('Результат (грамота, диплом)'),
            'document' => $this->string()->comment('Ссылка на загруженный файл'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('olympiads');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171123_164415_create_table_olympiad cannot be reverted.\n";

        return false;
    }
    */
}
