<?php

use yii\db\Migration;

class m171209_082752_remove_fk_and_index_from_messages extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk-messages-student', 'messages');

        $this->dropIndex('idx-messages-student', 'messages');
    }

    public function safeDown()
    {
        $this->createIndex(
            'idx-messages-student',
            'messages',
            'student_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-messages-student',
            'messages',
            'student_id',
            'student',
            'id',
            'CASCADE'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171209_082752_remove_fk_and_index_from_messages cannot be reverted.\n";

        return false;
    }
    */
}
