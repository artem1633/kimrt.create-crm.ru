<?php

use yii\db\Migration;

class m171124_061948_create_table_extracurricular extends Migration
{
    public function safeUp()
    {
        $this->createTable('extracurricular', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название мероприятия'),
            'level' => $this->string()->comment('Уровень (внутривузовский, районный)'),
            'date_period' => $this->string()->comment('Период проведения'),
            'partisipation' => $this->string()->comment('Форма участия'),
            'document' => $this->string()->comment('Загрузка файла'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('extracurricular');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171124_061948_create_table_extracurricular cannot be reverted.\n";

        return false;
    }
    */
}
