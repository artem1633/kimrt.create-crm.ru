<?php

use yii\db\Migration;

class m171130_064040_add_student_id_fk_to_extracurricular extends Migration
{
    public function safeUp()
    {
        $this->addColumn('extracurricular', 'student_id', $this->integer()->null());

        $this->createIndex(
            'idx-extracurricular-student',
            'extracurricular',
            'student_id'
        );

        $this->addForeignKey(
            'fk-extracurricular-student',
            'extracurricular',
            'student_id',
            'student',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-extracurricular-student', 'extracurricular');

        $this->dropIndex('idx-extracurricular-student', 'extracurricular');

        $this->dropColumn('extracurricular', 'student_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_064040_add_student_id_fk_to_extracurricular cannot be reverted.\n";

        return false;
    }
    */
}
