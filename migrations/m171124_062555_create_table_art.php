<?php

use yii\db\Migration;

class m171124_062555_create_table_art extends Migration
{
    public function safeUp()
    {
        $this->createTable('art', [
            'id' => $this->primaryKey(),
            'type' => $this->string()->comment('Вид деятельности'),
            'level' => $this->string()->comment('Уровень (внутривузовский, районный)'),
            'competition_name' => $this->string()->comment('Название конкурса'),
            'date' => $this->date()->comment('Дата'),
            'result' => $this->string()->comment('Результат (сертификат, грамота)'),
            'work' => $this->string()->comment('Загрузка файла'),
            'document' => $this->string()->comment('Загрузка файла'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('art');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171124_062555_create_table_art cannot be reverted.\n";

        return false;
    }
    */
}
