<?php

use yii\db\Migration;

/**
 * Handles the creation of table `student`.
 */
class m171111_064952_create_student_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('student', [
            'id' => $this->primaryKey(),
            'code' => $this->string()->comment('Шифр'),
            'surname' => $this->string()->comment('Фамилия'),
            'name' => $this->string()->comment('Имя'),
            'patronymic' => $this->string()->comment('Отчество'),
            'date_birth' => $this->date()->comment('Дата рождения'),
            'floor' => $this->integer()->comment('Пол'),
                'group_id' => $this->integer()->comment('Связь с группой'),
            'level' => $this->string()->comment('Уровень оброзования'),
                'specialty_id' => $this->integer()->comment('Специальность'),
                'course_id' => $this->integer()->comment('Курс'),
            'training' => $this->string()->comment('Форма обучения'),
            'payment' => $this->string()->comment('Форма оплаты'),
            'enrollment' => $this->string()->comment('Зачисление'),
            'enrollment_date' => $this->date()->comment('Дата Зачисление'),
            'expulsion' => $this->string()->comment('Отчислин'),
            'expulsion_date' => $this->date()->comment('Дата Отчислин'),
            'status' => $this->string()->comment('Статус'),
            'comment' => $this->string()->comment('Коментарий'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('student');
    }
}
