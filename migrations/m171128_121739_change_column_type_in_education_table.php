<?php

use yii\db\Migration;

class m171128_121739_change_column_type_in_education_table extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('additional_education', 'document_name');

        $this->addColumn('additional_education', 'document_name', $this->string());
    }

    public function safeDown()
    {
        echo "m171128_121739_change_column_type_in_education_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171128_121739_change_column_type_in_education_table cannot be reverted.\n";

        return false;
    }
    */
}
