<?php

use yii\db\Migration;

/**
 * Handles the creation of table `group`.
 */
class m171111_070149_create_group_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('group', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Наименование'),
            'comment' => $this->string()->comment('Коментарий'),
        ]);


        /*-----------------------------------*/
        $this->createIndex(
            'idx-applications-group_id',
            'student',
            'group_id'
        );

        $this->addForeignKey(
            'fk-applications-group_id',
            'student',
            'group_id',
            'group',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        /*-----------------------------------*/
        $this->dropForeignKey(
            'fk-applications-group_id',
            'student'
        );

        $this->dropIndex(
            'idx-applications-group_id',
            'student'
        );
        $this->dropTable('group');
    }
}
