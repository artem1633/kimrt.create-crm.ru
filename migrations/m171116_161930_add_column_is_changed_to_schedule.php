<?php

use yii\db\Migration;

class m171116_161930_add_column_is_changed_to_schedule extends Migration
{
    public function safeUp()
    {
        $this->addColumn('schedule', 'is_changed', $this->boolean()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('schedule', 'is_changed');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171116_161930_add_column_is_changed_to_schedule cannot be reverted.\n";

        return false;
    }
    */
}
