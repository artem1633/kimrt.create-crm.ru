<?php

use yii\db\Migration;

class m171121_121328_create_table_subjects extends Migration
{
    public function safeUp()
    {
        $this->createTable('subjects', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название дисциплины'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('subjects');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171121_121328_create_table_subjects cannot be reverted.\n";

        return false;
    }
    */
}
