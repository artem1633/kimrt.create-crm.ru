<?php

use yii\db\Migration;

class m171111_093030_create_table_schedule extends Migration
{
    public function safeUp()
    {
        $this->createTable('schedule', [
            'id' => $this->primaryKey(),
            'time_id' => $this->integer()->unsigned()->comment('Время'),
            'day_id' => $this->integer()->unsigned()->comment('День недели'),
            'subject' => $this->string()->comment('Предмет'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);

        /*
         * Расписание на понедельник
         */
        $this->insert('schedule', [
            'time_id' => 1,
            'day_id' => 1,
            'subject' => 'Русский язык и литература, а.437б',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        $this->insert('schedule', [
            'time_id' => 2,
            'day_id' => 1,
            'subject' => 'Химия а. 104и',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        $this->insert('schedule', [
            'time_id' => 3,
            'day_id' => 1,
            'subject' => 'Физика',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        /*
         * Расписание на вторник
         */
        $this->insert('schedule', [
            'time_id' => 1,
            'day_id' => 2,
            'subject' => 'ОБЖ, а.108и',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        $this->insert('schedule', [
            'time_id' => 2,
            'day_id' => 2,
            'subject' => 'Билогия а.553б',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        $this->insert('schedule', [
            'time_id' => 3,
            'day_id' => 2,
            'subject' => 'Английский язык',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        /*
         * Расписание на среду
         */
        $this->insert('schedule', [
            'time_id' => 1,
            'day_id' => 3,
            'subject' => 'Физика, а.224и',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        $this->insert('schedule', [
            'time_id' => 2,
            'day_id' => 3,
            'subject' => 'Математика а.212и',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        $this->insert('schedule', [
            'time_id' => 3,
            'day_id' => 3,
            'subject' => 'История',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        /*
         * Расписание на чеверг
         */
        $this->insert('schedule', [
            'time_id' => 1,
            'day_id' => 4,
            'subject' => 'Русский язык и литература, а.437б',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        $this->insert('schedule', [
            'time_id' => 2,
            'day_id' => 4,
            'subject' => 'Обществознание а.263 / Английский',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        $this->insert('schedule', [
            'time_id' => 3,
            'day_id' => 4,
            'subject' => 'Математика',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        /*
         * Расписание на пятницу
         */
        $this->insert('schedule', [
            'time_id' => 1,
            'day_id' => 5,
            'subject' => 'Физическая культура а.С/з',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        $this->insert('schedule', [
            'time_id' => 2,
            'day_id' => 5,
            'subject' => 'Экология',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        $this->insert('schedule', [
            'time_id' => 3,
            'day_id' => 5,
            'subject' => 'Информатика',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('schedule');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_093030_create_table_schedule cannot be reverted.\n";

        return false;
    }
    */
}
