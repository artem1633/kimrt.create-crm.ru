<?php

use yii\db\Migration;

class m171130_063206_add_student_id_fk_to_olympiad extends Migration
{
    public function safeUp()
    {
        $this->addColumn('olympiads', 'student_id', $this->integer()->null());

        $this->createIndex(
            'idx-olympiads-student',
            'olympiads',
            'student_id'
        );

        $this->addForeignKey(
            'fk-olympiads-student',
            'olympiads',
            'student_id',
            'student',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-olympiads-student', 'olympiads');

        $this->dropIndex('idx-olympiads-student', 'olympiads');

        $this->dropColumn('olympiads', 'student_id');
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_063206_add_student_id_fk_to_olympiad cannot be reverted.\n";

        return false;
    }
    */
}
