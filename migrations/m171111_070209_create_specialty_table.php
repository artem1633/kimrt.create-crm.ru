<?php

use yii\db\Migration;

/**
 * Handles the creation of table `specialty`.
 */
class m171111_070209_create_specialty_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('specialty', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Наименование'),
            'comment' => $this->string()->comment('Коментарий'),
        ]);


        /*-----------------------------------*/
        $this->createIndex(
            'idx-applications-specialty_id',
            'student',
            'specialty_id'
        );

        $this->addForeignKey(
            'fk-applications-specialty_id',
            'student',
            'specialty_id',
            'specialty',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        /*-----------------------------------*/
        $this->dropForeignKey(
            'fk-applications-specialty_id',
            'student'
        );

        $this->dropIndex(
            'idx-applications-specialty_id',
            'student'
        );
        $this->dropTable('specialty');
    }
}
