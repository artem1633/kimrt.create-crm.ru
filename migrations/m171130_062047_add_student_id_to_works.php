<?php

use yii\db\Migration;

class m171130_062047_add_student_id_to_works extends Migration
{
    public function safeUp()
    {
        $this->addColumn('works', 'student_id', $this->integer()->null());

        $this->createIndex(
            'idx-works-student',
            'works',
            'student_id'
        );

        $this->addForeignKey(
            'fk-works-student',
            'works',
            'student_id',
            'student',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-works-student', 'works');

        $this->dropIndex('idx-works-student', 'works');

        $this->dropColumn('works', 'student_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_062047_add_student_id_to_works cannot be reverted.\n";

        return false;
    }
    */
}
