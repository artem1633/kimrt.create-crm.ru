<?php

use yii\db\Migration;

class m171113_064448_create_table_dates extends Migration
{
    public function safeUp()
    {
        $this->createTable('dates', [
            'id' => $this->primaryKey()->unsigned(),
            'week_start' => $this->date()->comment('Дата начала недели'),
            'week_end' => $this->date()->comment('Дата окончания недели'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);

        /*
         * При создании таблицы, заполняю первичными данными
         */
        $this->insert('dates', [
            'week_start' => '2017-11-13',
            'week_end' => '2017-11-18',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('dates');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171113_064448_create_table_dates cannot be reverted.\n";

        return false;
    }
    */
}
