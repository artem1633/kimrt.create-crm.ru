<?php

use yii\db\Migration;

class m171130_064208_add_student_id_fk_to_art extends Migration
{
    public function safeUp()
    {
        $this->addColumn('art', 'student_id', $this->integer()->null());

        $this->createIndex(
            'idx-art-student',
            'art',
            'student_id'
        );

        $this->addForeignKey(
            'fk-art-student',
            'art',
            'student_id',
            'student',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-art-student', 'art');

        $this->dropIndex('idx-art-student', 'art');

        $this->dropColumn('art', 'student_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_064208_add_student_id_fk_to_art cannot be reverted.\n";

        return false;
    }
    */
}
