<?php

use yii\db\Migration;

/**
 * Handles adding end_date to table `orders`.
 */
class m180404_182310_add_end_date_column_to_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders', 'end_date', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders', 'end_date');
    }
}
