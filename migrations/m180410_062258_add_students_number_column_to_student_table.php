<?php

use yii\db\Migration;

/**
 * Handles adding students_number to table `student`.
 */
class m180410_062258_add_students_number_column_to_student_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('student', 'students_number', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('student', 'students_number');
    }
}
