<?php

use yii\db\Migration;

/**
 * Handles adding id_book to table `books`.
 */
class m180411_120926_add_id_book_column_to_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('books', 'id_book', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('books', 'id_book');
    }
}
