<?php

use yii\db\Migration;

/**
 * Handles adding id_book to table `foundations_book`.
 */
class m180411_150249_add_id_book_column_to_foundations_book_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('foundations_book', 'id_book', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('foundations_book', 'id_book');
    }
}
