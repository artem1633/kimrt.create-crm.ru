<?php

use yii\db\Migration;

class m171111_112856_create_table_time extends Migration
{
    public function safeUp()
    {
        $this->createTable('time', [
            'id' => $this->primaryKey()->unsigned(),
            'start' => $this->time()->comment('Время начала занятия'),
            'end' => $this->time()->comment('Время окончания занятия'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);

        $this->insert('time', [
            'start' => '09:00',
            'end' => '10:20',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);
        $this->insert('time', [
            'start' => '10:00',
            'end' => '11:20',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);
        $this->insert('time', [
            'start' => '12:00',
            'end' => '13:20',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('time');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_112856_create_table_time cannot be reverted.\n";

        return false;
    }
    */
}
