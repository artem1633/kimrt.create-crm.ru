<?php

use yii\db\Migration;

class m171124_062156_create_table_sport extends Migration
{
    public function safeUp()
    {
        $this->createTable('sport', [
            'id' => $this->primaryKey(),
            'sport' => $this->string()->comment('Вид спорта'),
            'level' => $this->string()->comment('Уровень (внутривузовский, районный)'),
            'competition' => $this->string()->comment('Название соревнований'),
            'date' => $this->date()->comment('Дата соревнований'),
            'result' => $this->string()->comment('Результат (сертификат, грамота)'),
            'document' => $this->string()->comment('Загрузка файла'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('sport');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171124_062156_create_table_sport cannot be reverted.\n";

        return false;
    }
    */
}
