<?php

use yii\db\Migration;

/**
 * Handles the creation of table `foundation`.
 */
class m180330_052810_create_foundation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('foundation', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);

        $this->insert('foundation', [
            'name' => 'Библиотека Б.Хмельницкого',
        ]);

        $this->insert('foundation', [
            'name' => 'Библиотека Костина',
        ]);

        $this->insert('foundation', [
            'name' => 'Библиотека Никольская',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('foundation');
    }
}
