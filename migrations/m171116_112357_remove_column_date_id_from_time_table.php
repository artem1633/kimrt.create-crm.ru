<?php

use yii\db\Migration;

class m171116_112357_remove_column_date_id_from_time_table extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('time', 'date_id');
    }

    public function safeDown()
    {
        $this->addColumn('time', 'date_id', $this->integer()->unsigned()->null());

        \app\models\Time::updateAll(['date_id'=>1]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171116_112357_remove_column_date_id_from_time_table cannot be reverted.\n";

        return false;
    }
    */
}
