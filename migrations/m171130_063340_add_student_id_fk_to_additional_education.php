<?php

use yii\db\Migration;

class m171130_063340_add_student_id_fk_to_additional_education extends Migration
{
    public function safeUp()
    {
        $this->addColumn('additional_education', 'student_id', $this->integer()->null());

        $this->createIndex(
            'idx-additional_education-student',
            'additional_education',
            'student_id'
        );

        $this->addForeignKey(
            'fk-additional_education-student',
            'additional_education',
            'student_id',
            'student',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-additional_education-student', 'additional_education');

        $this->dropIndex('idx-additional_education-student', 'additional_education');

        $this->dropColumn('additional_education', 'student_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_063340_add_student_id_fk_to_additional_education cannot be reverted.\n";

        return false;
    }
    */
}
