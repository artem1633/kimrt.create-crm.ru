<?php

use yii\db\Migration;

class m171115_110712_add_column_date_to_schedule extends Migration
{
    public function safeUp()
    {
        $this->addColumn('schedule', 'date_id', $this->integer()->unsigned()->null());

        /*
         * При добавлении столбца, сразу привязал к существующей дате
         */
        \app\models\Schedule::updateAll(['date_id'=>1]);
    }

    public function safeDown()
    {
        $this->dropColumn('schedule', 'date_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171115_110712_add_column_date_to_schedule cannot be reverted.\n";

        return false;
    }
    */
}
