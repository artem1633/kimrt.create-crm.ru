<?php

use yii\db\Migration;

class m171207_071538_create_table_messages extends Migration
{
    public function safeUp()
    {
        $this->createTable('messages', [
            'id' => $this->primaryKey(),
            'theme' => $this->string()->comment('Тема проекта'),
            'text' => $this->text()->comment('Текст'),
            'status' => $this->string()->comment('Статус'),
            'student_id' => $this->integer()->comment('Студент'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);

        $this->createIndex(
            'idx-messages-student',
            'messages',
            'student_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-messages-student',
            'messages',
            'student_id',
            'student',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-messages-student', 'messages');

        $this->dropIndex('idx-messages-student', 'messages');

        $this->dropTable('messages');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171207_071538_create_table_messages cannot be reverted.\n";

        return false;
    }
    */
}
