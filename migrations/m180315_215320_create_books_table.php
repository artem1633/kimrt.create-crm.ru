<?php

use yii\db\Migration;

/**
 * Handles the creation of table `books`.
 */
class m180315_215320_create_books_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('books', [
            'id' => $this->primaryKey(),
            'author' => $this->string()->comment('Автор'),
            'title' => $this->string()->comment('Заглавие'),
            'publishing_address' => $this->string()->comment('Адрес издательства'),
            'publishing_name' => $this->string()->comment('Наименование издательства'),
            'publishing_year' => $this->integer()->comment('Год издания'),
            'book_section_id' => $this->integer()->comment('Раздел, к которому относится книга'),
            'bbk' => $this->string()->comment('Код ББК'),
        ]);
        $this->addCommentOnTable('books', 'Книги (книжный фонд)');

        $this->createIndex(
            'idx-books-book_section_id',
            'books',
            'book_section_id'
        );

        $this->addForeignKey(
            'fk-books-book_section_id',
            'books',
            'book_section_id',
            'books_sections',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-books-book_section_id',
            'books'
        );

        $this->dropIndex(
            'idx-books-book_section_id',
            'books'
        );

        $this->dropTable('books');
    }
}
