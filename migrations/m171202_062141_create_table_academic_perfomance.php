<?php

use yii\db\Migration;

class m171202_062141_create_table_academic_perfomance extends Migration
{
    public function safeUp()
    {
        $this->createTable('academic_perfomance', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer()->comment('Курс'),
            'semester' => $this->string()->comment('Семестр (период)'),
            'subject_id' => $this->integer()->comment('Дисциплина'),
            'hours' => $this->string()->comment('Объем в ЗЕ/час'),
            'controll' => $this->string()->comment('Форма контроля'),
            'result' => $this->string()->comment('Результат'),
            'ball' => $this->string()->comment('Балл'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);

        $this->createIndex(
            'idx-academic_perfomance-course',
            'academic_perfomance',
            'course_id'
        );

        $this->addForeignKey(
            'fk-academic_perfomance-course',
            'academic_perfomance',
            'course_id',
            'course',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-academic_perfomance-subjects',
            'academic_perfomance',
            'subject_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-academic_perfomance-subjects',
            'academic_perfomance',
            'subject_id',
            'subjects',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-academic_perfomance-course', 'academic_perfomance');

        $this->dropIndex('idx-academic_perfomance-course', 'academic_perfomance');

        $this->dropForeignKey('fk-works-subjects', 'works');

        $this->dropIndex('idx-works-subjects', 'works');

        $this->dropTable('academic_perfomance');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171202_062141_create_table_academic_perfomance cannot be reverted.\n";

        return false;
    }
    */
}
