<?php

use yii\db\Migration;

/**
 * Handles the creation of table `changing`.
 */
class m180418_022755_create_changing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('changing', [
            'id' => $this->primaryKey(),
            'table_name' => $this->string(255),
            'line_id' => $this->integer(),
            'date_time' => $this->datetime(),
            'user_id' => $this->integer(),
            'field' => $this->string(255),
            'old_value' => $this->string(1000),
            'new_value' => $this->string(1000),
        ]);

        $this->createIndex('idx-changing-user_id', 'changing', 'user_id', false);
        $this->addForeignKey("fk-changing-user_id", "changing", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-changing-user_id','changing');
        $this->dropIndex('idx-changing-user_id','changing');
        
        $this->dropTable('changing');
    }
}
