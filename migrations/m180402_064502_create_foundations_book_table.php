<?php

use yii\db\Migration;

/**
 * Handles the creation of table `foundations_book`.
 */
class m180402_064502_create_foundations_book_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('foundations_book', [
            'id' => $this->primaryKey(),
            'foundation_id' => $this->integer(),
            'book_id' => $this->integer(),
            'quantity' => $this->integer(),
            'number' => $this->integer(),
        ]);

        $this->createIndex('idx-foundations_book-foundation_id', 'foundations_book', 'foundation_id', false);
        $this->addForeignKey("fk-foundations_book-foundation_id", "foundations_book", "foundation_id", "foundation", "id");

        $this->createIndex('idx-foundations_book-book_id', 'foundations_book', 'book_id', false);
        $this->addForeignKey("fk-foundations_book-book_id", "foundations_book", "book_id", "books", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-foundations_book-foundation_id','foundations_book');
        $this->dropIndex('idx-foundations_book-foundation_id','foundations_book');

        $this->dropForeignKey('fk-foundations_book-book_id','foundations_book');
        $this->dropIndex('idx-foundations_book-book_id','foundations_book');

        $this->dropTable('foundations_book');
    }
}
