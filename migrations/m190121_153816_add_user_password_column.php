<?php

use yii\db\Migration;

/**
 * Class m190121_153816_add_user_password_column
 */
class m190121_153816_add_user_password_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    $this->addColumn('users', 'passwd', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190121_153816_add_user_password_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190121_153816_add_user_password_column cannot be reverted.\n";

        return false;
    }
    */
}
