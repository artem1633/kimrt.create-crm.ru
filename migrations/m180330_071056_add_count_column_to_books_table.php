<?php

use yii\db\Migration;

/**
 * Handles adding count to table `books`.
 */
class m180330_071056_add_count_column_to_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('books', 'count', $this->integer());
        $this->addColumn('books', 'library_fond', $this->integer());

        $this->createIndex('idx-books-library_fond', 'books', 'library_fond', false);
        $this->addForeignKey("fk-books-library_fond", "books", "library_fond", "foundation", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-books-library_fond','books');
        $this->dropIndex('idx-books-library_fond','books');

        $this->dropColumn('books', 'count');
        $this->dropColumn('books', 'library_fond');
    }
}
