<?php

use yii\db\Migration;

class m171111_113125_create_table_week_days extends Migration
{
    public function safeUp()
    {
        $this->createTable('week_days', [
            'id' => $this->primaryKey()->unsigned(),
            'day' => $this->string()->comment('День недели'),
            'created_at' => $this->timestamp()->null(),
            'updated_at' => $this->timestamp()->null(),
        ]);

        $this->insert('week_days', [
            'day' => 'Понедельник',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        $this->insert('week_days', [
            'day' => 'Вторник',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        $this->insert('week_days', [
            'day' => 'Среда',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        $this->insert('week_days', [
            'day' => 'Четверг',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        $this->insert('week_days', [
            'day' => 'Пятница',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);

        $this->insert('week_days', [
            'day' => 'Суббота',
            'created_at' => date('Y-m-d h:i:m'),
            'updated_at' => date('Y-m-d h:i:m'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('week_days');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_113125_create_table_week_days cannot be reverted.\n";

        return false;
    }
    */
}
