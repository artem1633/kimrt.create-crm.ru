<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "books".
 *
 * @property integer $id
 * @property string $author
 * @property string $title
 * @property string $publishing_address
 * @property string $publishing_name
 * @property string $publishing_year
 * @property integer $book_section_id
 * @property string $bbk
 *
 * @property BooksSections $bookSection
 * @property LibraryBooks[] $libraryBooks
 * @property Library[] $libraries
 */
class Books extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public $search_type;
    public $search_condition;
    public $search_area;
    public $search_found;
    public $search_advanced;
    public $ordering;
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['book_section_id', 'publishing_year', 'library_fond'], 'integer'],
            [['title', 'author', 'library_fond', 'count', 'id_book'], 'required'],
            [['count'], 'integer', 'min' => 0],
            [['author', 'title', 'publishing_address', 'publishing_name', 'bbk', 'id_book'], 'string', 'max' => 255],
            [['book_section_id'], 'exist', 'skipOnError' => true, 'targetClass' => BooksSections::className(), 'targetAttribute' => ['book_section_id' => 'id']],
            [['library_fond'], 'exist', 'skipOnError' => true, 'targetClass' => Foundation::className(), 'targetAttribute' => ['library_fond' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Код',
            'id_book' => 'ИД книга',
            'author' => 'Автор',
            'title' => 'Заголовок',
            'publishing_address' => 'Место издания',
            'publishing_name' => 'Издательство',
            'publishing_year' => 'Год издания',
            'book_section_id' => 'Раздел',
            'library_fond' => 'Фонд библиотеки',
            'bbk' => 'Код ББК',
            'count' => 'Количество',
            'search_type' => 'Тип',
            'search_condition' => 'Условия поиска ',
            'search_area' => 'Поля',
            'search_found' => 'Библиотечные фонды ',
        ];
    }

    public function setChanging($old, $new)
    {            
        if($old->author != $new->author) Changing::setToChangeTable('books', $new->id, 'Автор', $old->author, $new->author );
        if($old->title != $new->title) Changing::setToChangeTable('books', $new->id, 'Заголовок', $old->title, $new->title );
        if($old->publishing_address != $new->publishing_address) Changing::setToChangeTable('books', $new->id, 'Место издания', $old->publishing_address, $new->publishing_address );
        if($old->publishing_name != $new->publishing_name) Changing::setToChangeTable('books', $new->id, 'Издательство', $old->publishing_name, $new->publishing_name );
        if($old->publishing_year != $new->publishing_year) Changing::setToChangeTable('books', $new->id, 'Год издания', $old->publishing_year, $new->publishing_year );
        if($old->book_section_id != $new->book_section_id) Changing::setToChangeTable('books', $new->id, 'Фонд библиотеки', $old->bookSection->name, $new->bookSection->name );
        if($old->library_fond != $new->library_fond) Changing::setToChangeTable('books', $new->id, 'Фонд библиотеки', $old->libraryFond->name, $new->libraryFond->name );
        if($old->bbk != $new->bbk) Changing::setToChangeTable('books', $new->id, 'Код ББК', $old->bbk, $new->bbk );
        if($old->count != $new->count) Changing::setToChangeTable('books', $new->id, 'Количество', $old->count, $new->count );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookSection()
    {
        return $this->hasOne(BooksSections::className(), ['id' => 'book_section_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibraryBooks()
    {
        return $this->hasMany(LibraryBooks::className(), ['books_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibraries()
    {
        return $this->hasMany(Library::className(), ['id' => 'library_id'])->viaTable('library_books', ['books_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibraryFond()
    {
        return $this->hasOne(Foundation::className(), ['id' => 'library_fond']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['book_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoundationsBooks()
    {
        return $this->hasMany(FoundationsBook::className(), ['book_id' => 'id']);
    }

    public function findBook()
    {
        /*$found = Foundation::find()->where(['name' => $this->library_fond ])->one();
        if($found == null) return false;*/

        $book = Books::find()->where(
            [
                'author' => $this->author, 
                'title' => $this->title, 
                'publishing_address' => $this->publishing_address, 
                'publishing_name' => $this->publishing_name, 
                'publishing_year' => $this->publishing_year, 
                'book_section_id' => $this->book_section_id, 
                'library_fond' => $this->library_fond,
                'id_book' => $this->id_book,
                'bbk' => $this->bbk,
            ])->one();

        if($book == null) return null;
        else return $book;
    }

    public function getNumber()
    {
        $book = Books::find()->where(
            [
                'author' => $this->author, 
                'title' => $this->title, 
                'publishing_address' => $this->publishing_address, 
                'publishing_name' => $this->publishing_name, 
                'publishing_year' => $this->publishing_year, 
                'book_section_id' => $this->book_section_id, 
                //'library_fond' => $this->library_fond,
                'bbk' => $this->bbk,
            ])->one();

        if($book == null) $this->id;
        else {
            $found = FoundationsBook::find()->where(['book_id' => $book->id ])->one();
            if($found == null) return $this->id;
            else return $found->number;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDownloadBooks()
    {
        return $this->hasMany(DownloadBooks::className(), ['book_id' => 'id']);
    }

    public function getTypes()
    {
        return ArrayHelper::map([
            ['id' => '1', 'title' => 'Автор',],
            ['id' => '2', 'title' => 'Заглавие',],
            ['id' => '3', 'title' => 'Место издания',],
            ['id' => '4', 'title' => 'Издательство',],
            ['id' => '5', 'title' => 'Год издания',],
        ],
        'id', 'title');
    }

    public function getConditions()
    {
        return ArrayHelper::map([
            ['id' => '1', 'title' => 'Слова',],
            ['id' => '2', 'title' => 'Словосочетания',],
        ],
        'id', 'title');
    }

    public function getFoundationList()
    {
        //return  ArrayHelper::map(Foundation::find()->all(), 'id', 'name');

        $founds = Foundation::find()->all();
        $result = [];
        $result [] = [
            'id' => 0,
            'name' => 'Все',
        ];

        foreach ($founds as $value) {
            $result [] = [
                'id' => $value->id,
                'name' => $value->name,
            ];
        }

        return ArrayHelper::map($result,'id', 'name');
    }

    public function getBooksList($post)
    {
        $providers = [];
        $found = Foundation::find()->all();
        foreach ($found as $value) {
            $searchModel = new BooksSearch();
            $dataProvider = $searchModel->searchBooksList($post,$value->id);
            $providers [] = $dataProvider;            
        }

        return $providers;
    }

    public function Changing($old, $new)
    {
        if($old->author != $new->author) return true;
        if($old->title != $new->title) return true;
        if($old->publishing_address != $new->publishing_address) return true;
        if($old->publishing_name != $new->publishing_name) return true;
        if($old->publishing_year != $new->publishing_year) return true;
        if($old->book_section_id != $new->book_section_id) return true;
        if($old->id_book != $new->id_book) return true;
        if($old->bbk != $new->bbk) return true;

        return false;
    }
}
