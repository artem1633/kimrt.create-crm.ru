<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "foundations_book".
 *
 * @property int $id
 * @property int $foundation_id
 * @property int $book_id
 * @property int $quantity
 * @property int $number
 *
 * @property Books $book
 * @property Foundation $foundation
 */
class FoundationsBook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foundations_book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['foundation_id', 'book_id', 'quantity', 'number'], 'integer'],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Books::className(), 'targetAttribute' => ['book_id' => 'id']],
            [['foundation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Foundation::className(), 'targetAttribute' => ['foundation_id' => 'id']],
            [['id_book'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'foundation_id' => 'Foundation ID',
            'book_id' => 'Book ID',
            'quantity' => 'Quantity',
            'number' => 'Number',
            'id_book' => 'ID Book',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Books::className(), ['id' => 'book_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoundation()
    {
        return $this->hasOne(Foundation::className(), ['id' => 'foundation_id']);
    }
}
