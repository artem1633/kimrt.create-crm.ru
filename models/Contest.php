<?php

namespace app\models;

use app\Traits\RelationHelper;
use Yii;
use yii\base\Exception;

/**
 * This is the model class for table "contests".
 *
 * @property integer $id
 * @property string $name
 * @property string $level
 * @property string $date
 * @property string $theme
 * @property string $result
 * @property string $work
 * @property string $document
 * @property string $created_at
 * @property string $updated_at
 */
class Contest extends \yii\db\ActiveRecord
{
    use RelationHelper;

    /**
     *  Файлы для сохранения
     */
    public $file;
    public $fileDocument;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'created_at', 'updated_at', 'document', 'is_approved'], 'safe'],
            [['name', 'level', 'theme', 'result', 'work', 'document'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название конкурса',
            'level' => 'Уровень',
            'date' => 'Дата проведения',
            'theme' => 'Тема работы',
            'result' => 'Результат',
            'work' => 'Работа',
            'document' => 'Документ',
            'is_approved' => 'Одобрено',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата проведения',
        ];
    }
    public function setChanging($old, $new)
    {            
        if($old->name != $new->name) Changing::setToChangeTable('contest', $new->id, 'Название конкурса', $old->name, $new->name );
        if($old->level != $new->level) Changing::setToChangeTable('contest', $new->id, 'Уровень', $old->level, $new->level );
        if($old->date != $new->date) Changing::setToChangeTable('contest', $new->id, 'Дата проведения', $old->date, $new->date );
        if($old->theme != $new->theme) Changing::setToChangeTable('contest', $new->id, 'Тема работы', $old->theme, $new->theme );
        if($old->result != $new->result) Changing::setToChangeTable('contest', $new->id, 'Результат', $old->result, $new->result );
    }

    /**
     *  Загрузка файла
     */
    public function upload($file = "work")
    {
       try {
           /**
            *  TODO: Добавить проверку, если директории нет, то создавать,
            *  TODO: чтобы работы разных пользователей были по папкам
            */
           if ($this->validate()) {
               if ($file == "work") {
                   $link = 'uploads/contest/' . $file . '_' . Yii::$app->user->identity->id . '_' . $this->file->baseName . '_' . date('Y-m-d') . '.' . $this->file->extension;

                   $this->file->saveAs($link);
                   $this->work = Yii::$app->params['siteName'] . $link;
                   $this->update(false);
               }
               else if ($file == "document") {
                   $link = 'uploads/contest/' . $file . '_' . Yii::$app->user->identity->id . '_' . $this->file->baseName . '_' . date('Y-m-d') . '.' . $this->file->extension;

                   $this->file->saveAs($link);
                   $this->document = Yii::$app->params['siteName'] . $link;
                   $this->update(false);
               }

               return true;
           } else {
               return false;
           }
       }
       catch (Exception $exception) {
           return var_dump ("Файл не загружен.");
       }
    }
}
