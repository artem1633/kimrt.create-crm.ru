<?php

namespace app\models;

use app\Traits\RelationHelper;
use Yii;

/**
 * This is the model class for table "extracurricular".
 *
 * @property integer $id
 * @property string $name
 * @property string $level
 * @property string $date_period
 * @property string $partisipation
 * @property string $document
 * @property string $created_at
 * @property string $updated_at
 */
class Extracurricular extends \yii\db\ActiveRecord
{
    use RelationHelper;

    /**
     *  Файлы для сохранения
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'extracurricular';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'is_approved'], 'safe'],
            [['name', 'level', 'date_period', 'partisipation', 'document'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название мероприятия',
            'level' => 'Уровень',
            'date_period' => 'Период проведения',
            'partisipation' => 'Форма участия',
            'document' => 'Документ',
            'is_approved' => 'Одобрено',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }
    public function setChanging($old, $new)
    {            
        if($old->name != $new->name) Changing::setToChangeTable('extracurricular', $new->id, 'Название мероприятия', $old->name, $new->name );
        if($old->level != $new->level) Changing::setToChangeTable('extracurricular', $new->id, 'Уровень', $old->level, $new->level );
        if($old->date_period != $new->date_period) Changing::setToChangeTable('extracurricular', $new->id, 'Период проведения', $old->date_period, $new->date_period );
    }

    /**
     *  Загрузка файла
     */
    public function upload()
    {
        /**
         *  TODO: Добавить проверку, если директории нет, то создавать,
         *  TODO: чтобы работы разных пользователей были по папкам
         */
        if ($this->validate()) {
            $link = 'uploads/extracurricular/' . Yii::$app->user->identity->id . '_' . $this->file->baseName . '_' . date('Y-m-d') . '.' . $this->file->extension;

            $this->file->saveAs($link);
            $this->document = Yii::$app->params['siteName'] . $link;
            $this->update(false);

            return true;
        } else {
            return false;
        }
    }
}
