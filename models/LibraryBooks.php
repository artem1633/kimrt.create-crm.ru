<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "library_books".
 *
 * @property integer $library_id
 * @property integer $books_id
 * @property integer $quantity
 *
 * @property Books $books
 * @property Library $library
 */
class LibraryBooks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'library_books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['library_id', 'books_id'], 'required'],
            [['library_id', 'books_id', 'quantity'], 'integer'],
            [['books_id'], 'exist', 'skipOnError' => true, 'targetClass' => Books::className(), 'targetAttribute' => ['books_id' => 'id']],
            [['library_id'], 'exist', 'skipOnError' => true, 'targetClass' => Library::className(), 'targetAttribute' => ['library_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'library_id' => 'Library ID',
            'books_id' => 'Books ID',
            'quantity' => 'Quantity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasOne(Books::className(), ['id' => 'books_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibrary()
    {
        return $this->hasOne(Library::className(), ['id' => 'library_id']);
    }
}
