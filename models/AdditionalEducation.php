<?php

namespace app\models;

use app\Traits\RelationHelper;
use Yii;

/**
 * This is the model class for table "additional_education".
 *
 * @property integer $id
 * @property string $name
 * @property string $hours
 * @property string $location_time
 * @property string $document_name
 * @property string $document
 * @property string $created_at
 * @property string $updated_at
 */
class AdditionalEducation extends \yii\db\ActiveRecord
{
    use RelationHelper;
    /**
     *  Файлы для сохранения
     */
    public $file;

//    public function __construct()
//    {
//        parent::__construct();
//
//        return exit(1);
//    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'additional_education';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['document_name', 'created_at', 'updated_at', 'is_approved'], 'safe'],
            [['name', 'hours', 'location_time', 'document'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название программы',
            'hours' => 'Количество часов',
            'location_time' => 'Место и время обучения',
            'document_name' => 'Название документа',
            'document' => 'Документ',
            'is_approved' => 'Одобрено',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }
    public function setChanging($old, $new)
    {            
        if($old->name != $new->name) Changing::setToChangeTable('additional-education', $new->id, 'Название программы', $old->name, $new->name );
        if($old->hours != $new->hours) Changing::setToChangeTable('additional-education', $new->id, 'Количество часов', $old->hours, $new->hours );
        if($old->location_time != $new->location_time) Changing::setToChangeTable('additional-education', $new->id, 'Место и время обучения', $old->location_time, $new->location_time );
        if($old->document_name != $new->document_name) Changing::setToChangeTable('additional-education', $new->id, 'Название документа', $old->document_name, $new->document_name );
    }

    /**
     *  Загрузка файла
     */
    public function upload()
    {
        /**
         *  TODO: Добавить проверку, если директории нет, то создавать,
         *  TODO: чтобы работы разных пользователей были по папкам
         */
        if ($this->validate()) {
            $link = 'uploads/additional_education/' . Yii::$app->user->identity->id . '_' . $this->file->baseName . '_' . date('Y-m-d') . '.' . $this->file->extension;

            $this->file->saveAs($link);
            $this->document = Yii::$app->params['siteName'] . $link;
            $this->update(false);

            return true;
        } else {
            return false;
        }
    }
}
