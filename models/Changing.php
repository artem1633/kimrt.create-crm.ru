<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "changing".
 *
 * @property int $id
 * @property string $table_name
 * @property int $line_id
 * @property string $date_time
 * @property int $user_id
 * @property string $field
 * @property string $old_value
 * @property string $new_value
 *
 * @property Users $user
 */
class Changing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'changing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['line_id', 'user_id'], 'integer'],
            [['date_time'], 'safe'],
            [['table_name', 'field'], 'string', 'max' => 255],
            [['old_value', 'new_value'], 'string', 'max' => 1000],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table_name' => 'Раздел',
            'line_id' => 'Line ID',
            'date_time' => 'Даты и времени события',
            'user_id' => 'Пользователь',
            'field' => 'Объект системы',
            'old_value' => 'Состояние объекта до изменения',
            'new_value' => 'Действие',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public function TableList()
    {        
        $result = [];
        
        $result [] = [ 'id' => "zayavka", 'title' => "Заявка", ];
        $result [] = [ 'id' => "client", 'title' => "Клиент", ];
        $result [] = [ 'id' => "zaym", 'title' => "Займ", ];
        $result [] = [ 'id' => "zalog", 'title' => "Залог", ];
        //$result [] = [ 'id' => "", 'title' => "Пользователь", ];        

        return ArrayHelper::map($result,'id', 'title');
    }

    public function getTableName($id)
    {
        if($id == 'books') return 'Книжный фонд';
        if($id == 'student') return 'Студенты';
        if($id == 'users') return 'Пользователи';
        if($id == 'orders') return 'Заказы';
        if($id == 'academic-perfomance') return 'Книжный фонд';
        if($id == 'foundation') return 'Фонды библиотек';
        if($id == 'library') return 'Библиотеки';
        if($id == 'autobiography') return 'Автобиография';
        if($id == 'work') return 'Курсовые и дипломные';
        if($id == 'olympiad') return 'Предметные олимпиады';
        if($id == 'additional-education') return 'Доп. образование';
        if($id == 'scientific-projects') return 'Научные проекты';
        if($id == 'contest') return 'Конкурсы проектов';
        if($id == 'extracurricular') return 'Внеучебное образование';
        if($id == 'sport') return 'Спортивные достижения';
        if($id == 'art') return 'Творческие достижения';
        if($id == 'login') return 'Время входа пользователя на сайт';
        if($id == 'download-books') return 'Электронный формуляр';
        if($id == 'schedule') return 'Расписание';
        if($id == 'time') return 'Время';
        if($id == 'date') return 'Учебный период';
        if($id == 'log') return 'Логи';
        if($id == 'messages') return 'Сообщения';
    }

    public function setToChangeTable($table_name, $id, $field, $old_value, $new_value)
    {
        $old_value = $old_value.'';
        $new_value = $new_value.'';
        $model = new Changing();
        $model->table_name = $table_name;
        $model->line_id = $id;
        $model->date_time = date('Y-m-d H:i:s');
        $model->user_id = \Yii::$app->user->identity->id;
        $model->field = $field; // Поле
        $model->old_value = $old_value;
        $model->new_value = $new_value;
        $model->save();
    }
}
