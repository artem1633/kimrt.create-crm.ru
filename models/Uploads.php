<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "uploads".
 *
 * @property integer $id
 * @property string $name
 * @property string $extension
 * @property string $path
 */
class Uploads extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $File;
    public $path;
    public static function tableName()
    {
        return 'uploads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'extension', 'path'], 'string', 'max' => 255],
            [['File'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'extension' => 'Extension',
            'path' => 'Path',
            'File' => "Файл",
        ];
    }
    public function upload($ssilka)
    {
        $this->File->saveAs('logotips/' . $ssilka . '.' . $this->File->extension);
        return true;
      
    }
}
