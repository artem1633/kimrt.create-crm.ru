<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "week_days".
 *
 * @property integer $id
 * @property string $day
 * @property string $created_at
 * @property string $updated_at
 */
class WeekDay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'week_days';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['day'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'day' => 'День недели',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    public function getSchedule()
    {
        return $this->hasMany(Schedule::className(), ['day_id' => 'id']);
    }

    public static function getDaysArray()
    {
        return ArrayHelper::map(WeekDay::find()->all(), 'id', 'day');
    }
}
