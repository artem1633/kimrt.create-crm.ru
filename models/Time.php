<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "time".
 *
 * @property integer $id
 * @property string $start
 * @property string $end
 * @property string $created_at
 * @property string $updated_at
 */
class Time extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'time';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start', 'end', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'start' => 'Начало',
            'end' => 'Конец',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    public function getSchedule()
    {
        return $this->hasMany(Schedule::className(), ['time_id' => 'id']);
    }

    public static function getTimeArray()
    {
        return ArrayHelper::map(Time::find()->all(), 'id', 'start');
    }
    
    public function setChanging($old, $new)
    {            
        if($old->start != $new->start) Changing::setToChangeTable('time', $new->id, 'Начало', $old->start, $new->start );
        if($old->end != $new->end) Changing::setToChangeTable('time', $new->id, 'Конец', $old->end, $new->end );
    }
}
