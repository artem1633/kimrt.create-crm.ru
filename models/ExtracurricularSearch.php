<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Extracurricular;

/**
 * ExtracurricularSearch represents the model behind the search form about `app\models\Extracurricular`.
 */
class ExtracurricularSearch extends Extracurricular
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'level', 'date_period', 'partisipation', 'document', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $studentId = $this->getStudent();

        $query = Extracurricular::find()->where(['student_id' => $studentId])->andWhere(['not', ['student_id' => null]]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'level', $this->level])
            ->andFilterWhere(['like', 'date_period', $this->date_period])
            ->andFilterWhere(['like', 'partisipation', $this->partisipation])
            ->andFilterWhere(['like', 'document', $this->document]);

        return $dataProvider;
    }
}
