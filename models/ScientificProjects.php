<?php

namespace app\models;

use app\Traits\RelationHelper;
use Yii;

/**
 * This is the model class for table "scientific_projects".
 *
 * @property integer $id
 * @property string $name
 * @property string $level
 * @property string $date
 * @property string $theme
 * @property string $publication
 * @property string $work
 * @property string $created_at
 * @property string $updated_at
 */
class ScientificProjects extends \yii\db\ActiveRecord
{
    use RelationHelper;

    /**
     *  Файлы для сохранения
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scientific_projects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'created_at', 'updated_at', 'is_approved'], 'safe'],
            [['name', 'level', 'theme', 'publication', 'work'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название конференции',
            'level' => 'Уровень',
            'date' => 'Дата конференции',
            'theme' => 'Тема выступления',
            'publication' => 'Наличие публикации',
            'work' => 'Работы',
            'is_approved' => 'Одобрено',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    public function setChanging($old, $new)
    {            
        if($old->name != $new->name) Changing::setToChangeTable('scientific-projects', $new->id, 'Название конференции', $old->name, $new->name );
        if($old->level != $new->level) Changing::setToChangeTable('scientific-projects', $new->id, 'Уровень', $old->level, $new->level );
        if($old->date != $new->date) Changing::setToChangeTable('scientific-projects', $new->id, 'Дата конференции', $old->date, $new->date );
        if($old->theme != $new->theme) Changing::setToChangeTable('scientific-projects', $new->id, 'Тема выступления', $old->theme, $new->theme );
        if($old->publication != $new->publication) Changing::setToChangeTable('scientific-projects', $new->id, 'Наличие публикации', $old->publication, $new->publication );
    }
    /**
     *  Загрузка файла
     */
    public function upload()
    {
        /**
         *  TODO: Добавить проверку, если директории нет, то создавать,
         *  TODO: чтобы работы разных пользователей были по папкам
         */
        if ($this->validate()) {
            $link = 'uploads/scientific_projects/' . Yii::$app->user->identity->id . '_' . $this->file->baseName . '_' . date('Y-m-d') . '.' . $this->file->extension;

            $this->file->saveAs($link);
            $this->document = Yii::$app->params['siteName'] . $link;
            $this->update(false);

            return true;
        } else {
            return false;
        }
    }
}
