<?php

namespace app\models;

use app\Traits\RelationHelper;
use Yii;

/**
 * This is the model class for table "sport".
 *
 * @property integer $id
 * @property string $sport
 * @property string $level
 * @property string $competition
 * @property string $date
 * @property string $result
 * @property string $document
 * @property string $created_at
 * @property string $updated_at
 */
class Sport extends \yii\db\ActiveRecord
{
    use RelationHelper;

    /**
     *  Файлы для сохранения
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sport';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'created_at', 'updated_at', 'is_approved'], 'safe'],
            [['sport', 'level', 'competition', 'result', 'document'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sport' => 'Вид спорта',
            'level' => 'Уровень',
            'competition' => 'Название соревнований',
            'date' => 'Дата соревнований',
            'result' => 'Результат',
            'document' => 'Документ',
            'is_approved' => 'Одобрено',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }
    public function setChanging($old, $new)
    {            
        if($old->sport != $new->sport) Changing::setToChangeTable('sport', $new->id, 'Вид спорта', $old->sport, $new->sport );
        if($old->level != $new->level) Changing::setToChangeTable('sport', $new->id, 'Уровень', $old->level, $new->level );
        if($old->competition != $new->competition) Changing::setToChangeTable('sport', $new->id, 'Название соревнований', $old->competition, $new->competition );
        if($old->date != $new->date) Changing::setToChangeTable('sport', $new->id, 'Дата соревнований', $old->date, $new->date );
        if($old->result != $new->result) Changing::setToChangeTable('sport', $new->id, 'Результат', $old->result, $new->result );
    }

    /**
     *  Загрузка файла
     */
    public function upload()
    {
        /**
         *  TODO: Добавить проверку, если директории нет, то создавать,
         *  TODO: чтобы работы разных пользователей были по папкам
         */
        if ($this->validate()) {
            $link = 'uploads/extracurricular/' . Yii::$app->user->identity->id . '_' . $this->file->baseName . '_' . date('Y-m-d') . '.' . $this->file->extension;

            $this->file->saveAs($link);
            $this->document = Yii::$app->params['siteName'] . $link;
            $this->update(false);

            return true;
        } else {
            return false;
        }
    }
}
