<?php

namespace app\models;

use app\Traits\RelationHelper;
use Yii;

/**
 * This is the model class for table "autobiography".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $is_approved
 * @property string $comment
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Users $user
 */
class Autobiography extends \yii\db\ActiveRecord
{
    use RelationHelper;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'autobiography';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'is_approved'], 'integer'],
            [['created_at', 'updated_at', 'text', 'is_approved'], 'safe'],
            [['comment'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Студент',
            'text' => 'Текст автобиографии',
            'is_approved' => 'Одобрено',
            'comment' => 'Коментарий',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    public function setChanging($old, $new)
    {            
        if($old->text != $new->text) Changing::setToChangeTable('autobiography', $new->id, 'Текст автобиографии', $old->text, $new->text );
        if($old->comment != $new->comment) Changing::setToChangeTable('autobiography', $new->id, 'Коментарий', $old->comment, $new->comment );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
