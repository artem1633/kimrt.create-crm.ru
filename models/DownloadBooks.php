<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "download_books".
 *
 * @property int $id
 * @property int $user_id
 * @property int $book_id
 * @property int $foundation_id
 * @property string $return_date
 *
 * @property Books $book
 * @property Foundation $foundation
 * @property Users $user
 */
class DownloadBooks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'download_books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'book_id', 'foundation_id'], 'integer'],
            [['return_date'], 'safe'],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Books::className(), 'targetAttribute' => ['book_id' => 'id']],
            [['foundation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Foundation::className(), 'targetAttribute' => ['foundation_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Студент',
            'book_id' => 'Код книги',
            'foundation_id' => 'Фонд библиотеки',
            'return_date' => 'Дата возврата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Books::className(), ['id' => 'book_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoundation()
    {
        return $this->hasOne(Foundation::className(), ['id' => 'foundation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
