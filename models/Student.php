<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "student".
 *
 * @property integer $id
 * @property string $code
 * @property string $surname
 * @property string $name
 * @property string $patronymic
 * @property string $date_birth
 * @property integer $floor
 * @property integer $group_id
 * @property string $level
 * @property integer $specialty_id
 * @property integer $course_id
 * @property string $training
 * @property string $payment
 * @property string $enrollment
 * @property string $enrollment_date
 * @property string $expulsion
 * @property string $expulsion_date
 * @property string $status
 * @property string $comment
 *
 * @property Course $course
 * @property Group $group
 * @property Specialty $specialty
 */
class Student extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_birth', 'enrollment_date', 'expulsion_date', 'user_id', 'new_messages'], 'safe'],
            [['floor', 'group_id', 'specialty_id', 'course_id'], 'integer'],
            [['code', 'surname', 'name', 'patronymic', 'level', 'training', 'payment', 'enrollment', 'expulsion', 'status', 'comment', 'students_number'], 'string', 'max' => 255],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['specialty_id'], 'exist', 'skipOnError' => true, 'targetClass' => Specialty::className(), 'targetAttribute' => ['specialty_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Шифр',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'patronymic' => 'Отчество',
            'date_birth' => 'Дата рождения',
            'floor' => 'Пол',
            'group_id' => 'Группа',
            'level' => 'Уровень',
            'specialty_id' => 'Специальность',
            'course_id' => 'Курс',
            'training' => 'Форма обучения',
            'payment' => 'Форма оплаты',
            'enrollment' => '№ приказа о зачислении',
            'enrollment_date' => 'Дата приказа о зачислении',
            'expulsion' => '№ приказа об отчислении',
            'expulsion_date' => 'Дата приказа об отчислении',
            'status' => 'Статус',
            'comment' => 'Комментарий',
            'user_id' => 'Учетная запись',
            'students_number' => 'Номер зачетки\курсантского',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return array
     */
    public function getAllCourse()
    {
        return  ArrayHelper::map(Course::find()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @return array
     */
    public function getAllGroup()
    {
        return  ArrayHelper::map(Group::find()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialty()
    {
        return $this->hasOne(Specialty::className(), ['id' => 'specialty_id']);
    }

    /**
     * @return array
     */
    public function getAllSpecialty()
    {
        return ArrayHelper::map(Specialty::find()->all(), 'id', 'name');
    }

    /**
     *  Список созданных учеток студентов
     */
    public function getUserAccounts()
    {
        $arrayMap = ArrayHelper::map(User::find()->where(['type' => 'student'])->all(), 'id', 'login');
        $defaultValue = ['0' => 'Выберите учетку'];

        $arrayMap = ArrayHelper::merge($defaultValue, $arrayMap);

        return $arrayMap;
    }
    public function getGenderName($id)
    {
        if( $id == 1 ) return 'Мужской';
        else return 'Женский';
    }

    public function getLevelName($id)
    {
        if( $id == 1 ) return 'Бакалавриат';
        if( $id == 2 ) return 'Специалитет';
        if( $id == 3 ) return 'СПО';
    }

    public function getTrainingName($id)
    {
        if( $id == 1 ) return 'Очная';
        if( $id == 2 ) return 'Заочная';
    }

    public function getPaymentName($id)
    {
        if( $id == 1 ) return 'Бюджет';
        if( $id == 2 ) return 'Договор';
    }

    public function getStatusName($id)
    {
        if( $id == 1 ) return 'Обучается';
        if( $id == 2 ) return 'Отчислен';
        if( $id == 3 ) return 'Выпускник';
    }
    public function setDate($date)
    {
        if($date == null) return '';
        else return \Yii::$app->formatter->asDate($date, 'php:d.m.Y');
    }

    public function setChanging($old, $new)
    {            
        if($old->code != $new->code) Changing::setToChangeTable('student', $new->id, 'Шифр', $old->code, $new->code );
        if($old->surname != $new->surname) Changing::setToChangeTable('student', $new->id, 'Фамилия', $old->surname, $new->surname );
        if($old->name != $new->name) Changing::setToChangeTable('student', $new->id, 'Имя', $old->name, $new->name );
        if($old->patronymic != $new->patronymic) Changing::setToChangeTable('student', $new->id, 'Отчество', $old->patronymic, $new->patronymic );
        if($old->date_birth != $new->date_birth) Changing::setToChangeTable('student', $new->id, 'Дата рождения', $old->setDate($old->date_birth), $new->setDate($new->date_birth) );
        if($old->floor != $new->floor) Changing::setToChangeTable('student', $new->id, 'Пол', $old->getGenderName($old->floor), $new->getGenderName($new->floor) );
        if($old->group_id != $new->group_id) Changing::setToChangeTable('student', $new->id, 'Группа', $old->group->name, $new->group->name );
        if($old->level != $new->level) Changing::setToChangeTable('student', $new->id, 'Уровень', $old->getLevelName($old->level), $new->getLevelName($new->level) );
        if($old->specialty_id != $new->specialty_id) Changing::setToChangeTable('student', $new->id, 'Специальность', $old->specialty->name, $new->specialty->name );
        if($old->course_id != $new->course_id) Changing::setToChangeTable('student', $new->id, 'Курс', $old->course->name, $new->course->name );
        if($old->training != $new->training) Changing::setToChangeTable('student', $new->id, 'Форма обучения', $old->getTrainingName($old->training), $new->getTrainingName($new->training) );
        if($old->payment != $new->payment) Changing::setToChangeTable('student', $new->id, 'Форма оплаты', $old->getPaymentName($old->payment), $new->getPaymentName($new->payment) );
        if($old->enrollment != $new->enrollment) Changing::setToChangeTable('student', $new->id, '№ приказа о зачислении', $old->enrollment, $new->enrollment );
        if($old->enrollment_date != $new->enrollment_date) Changing::setToChangeTable('student', $new->id, 'Дата приказа о зачислении', $old->setDate($old->enrollment_date), $new->setDate($new->enrollment_date) );
        if($old->expulsion != $new->expulsion) Changing::setToChangeTable('student', $new->id, '№ приказа об отчислении', $old->expulsion, $new->expulsion );
        if($old->expulsion_date != $new->expulsion_date) Changing::setToChangeTable('student', $new->id, 'Дата приказа об отчислении', $old->setDate($old->expulsion_date), $new->setDate($new->expulsion_date) );
        if($old->status != $new->status) Changing::setToChangeTable('student', $new->id, 'Статус', $old->getStatusName($old->status), $new->getStatusName($new->status) );
        if($old->comment != $new->comment) Changing::setToChangeTable('student', $new->id, 'Комментарий', $old->comment, $new->comment );
        if($old->user_id != $new->user_id) Changing::setToChangeTable('student', $new->id, 'Учетная запись', $old->user->name, $new->user->name );
        if($old->students_number != $new->students_number) Changing::setToChangeTable('student', $new->id, 'Номер зачетки\курсантского', $old->students_number, $new->students_number );
    }

}
