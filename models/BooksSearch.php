<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Books;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * BooksSearch represents the model behind the search form of app\models\Books.
 *
 * @property string $book_section_name
 */
class BooksSearch extends Books
{

    public $description;
    public $quantity;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['author', 'title', 'name', 'book_section_id', 'publishing_address', 'publishing_name', 'publishing_year', 'bbk', 'description', 'quantity', 'count', 'id_book'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'quantity' => 'Количество',
            'description' => 'Фонд библиотеки'
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Books::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->joinWith('bookSection');

        $query->andFilterWhere([
            'id' => $this->id,
            'publishing_year' => $this->publishing_year,
        ]);

        $query->andFilterWhere(['like', 'author', $this->author])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'publishing_address', $this->publishing_address])
            ->andFilterWhere(['like', 'publishing_name', $this->publishing_name])
            ->andFilterWhere(['like', 'books_sections.name', $this->book_section_id])
            ->andFilterWhere(['like', 'bbk', $this->bbk]);


        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied in
     * libraries foundations
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchInFoundation($params)
    {
        $query = (new Query())->from('books');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->select(['books.*', 'library.description', 'library_books.quantity']);

        $query->leftJoin('library_books', 'books.id = library_books.books_id');
        $query->leftJoin('library', 'library_books.library_id = library.id');
        $query->leftJoin('books_sections', 'books.book_section_id = books_sections.id');

        $query->andFilterWhere([
            'id' => $this->id,
            'publishing_year' => $this->publishing_year,
        ]);

        $query->andFilterWhere(['like', 'author', $this->author])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'publishing_address', $this->publishing_address])
            ->andFilterWhere(['like', 'publishing_name', $this->publishing_name])
            ->andFilterWhere(['like', 'books_sections.name', $this->book_section_id])
            ->andFilterWhere(['like', 'library.description', $this->description])
            ->andFilterWhere(['like', 'library_books.quantity', $this->quantity])
            ->andFilterWhere(['like', 'bbk', $this->bbk]);

        $query->andFilterWhere(['!=', 'library_books.quantity', 0]);

        return $dataProvider;
    }

    public function searchBooksList($post,$id)
    {
        if(!isset($post['BooksSearch']['search_area'])) $post['BooksSearch']['search_area'] = null;
        if(!isset($post['letter'])) $post['letter'] = null;

        if($post['BooksSearch']['search_area'] == null) $books = Books::find()->where(['book_section_id' => $id])->all(); 
        else $books = Books::find()->where(['book_section_id' => $id])->andWhere(['LIKE', 'title', $post['BooksSearch']['search_area']])->all(); 

        $array = [];
        if($post['letter'] != null) {
            foreach ($books as $book) {                
                mb_internal_encoding("UTF-8");
                if( mb_substr($book->title, 0, 1) == $post['letter'] || mb_substr($book->title, 0, 1) == strtolower ($post['letter']) ) $array [] = $book->id;
            }
        }
        else{
            foreach ($books as $book) {                
                $array [] = $book->id;
            }
        }

        $founds_book = FoundationsBook::find()->where(['book_id' => $array])->all();
        $result = [];
        foreach ($founds_book as $value) {
            $result [] = $value->number;
        }
        $query = Books::find()->where(['id'=>array_unique($result)]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['title' => SORT_ASC]]
        ]);

        return $dataProvider;
    }

    public function searchAdvancedList($post,$id)
    {
        if(!isset($post['BooksSearch']['search_found'])) $search_found = 0;
        else $search_found = $post['BooksSearch']['search_found'];
        if(!isset($post['BooksSearch']['search_type'])) $search_type = 1;
        else $search_type = $post['BooksSearch']['search_type'];
        if(!isset($post['BooksSearch']['search_area'])) $search_area = null;
        else $search_area = $post['BooksSearch']['search_area'];
        if(!isset($post['letter'])) $post['letter'] = null;

        if($search_found == 0)
        {
            if($search_area != null){
                if($search_type == 1) $books = Books::find()->where(['book_section_id' => $id, ])->andWhere(['LIKE', 'author', $search_area])->all(); 
                if($search_type == 2) $books = Books::find()->where(['book_section_id' => $id, ])->andWhere(['LIKE', 'title', $search_area])->all(); 
                if($search_type == 3) $books = Books::find()->where(['book_section_id' => $id, ])->andWhere(['LIKE', 'publishing_address', $search_area])->all(); 
                if($search_type == 4) $books = Books::find()->where(['book_section_id' => $id, ])->andWhere(['LIKE', 'publishing_name', $search_area])->all(); 
                if($search_type == 5) $books = Books::find()->where(['book_section_id' => $id, ])->andWhere(['LIKE', 'publishing_year', $search_area])->all(); 
            }
            else $books = Books::find()->where(['book_section_id' => $id])->all(); 
        }
        else 
        {
            if($search_area != null){
                if($search_type == 1) $books = Books::find()->where(['book_section_id' => $id,'library_fond' => $search_found, ])->andWhere(['LIKE', 'author', $search_area])->all(); 
                if($search_type == 2) $books = Books::find()->where(['book_section_id' => $id,'library_fond' => $search_found, ])->andWhere(['LIKE', 'title', $search_area])->all(); 
                if($search_type == 3) $books = Books::find()->where(['book_section_id' => $id,'library_fond' => $search_found, ])->andWhere(['LIKE', 'publishing_address', $search_area])->all(); 
                if($search_type == 4) $books = Books::find()->where(['book_section_id' => $id,'library_fond' => $search_found, ])->andWhere(['LIKE', 'publishing_name', $search_area])->all(); 
                if($search_type == 5) $books = Books::find()->where(['book_section_id' => $id,'library_fond' => $search_found, ])->andWhere(['LIKE', 'publishing_year', $search_area])->all(); 
            }
            else $books = Books::find()->where(['book_section_id' => $id, 'library_fond' => $search_found])->all(); 
        }

        /*echo "<pre>";
        print_r($books);
        echo "</pre>";
        die;*/

        $array = [];
        if($post['letter'] != null) {
            foreach ($books as $book) {                
                mb_internal_encoding("UTF-8");
                if( mb_substr($book->title, 0, 1) == $post['letter'] || mb_substr($book->title, 0, 1) == strtolower ($post['letter']) ) $array [] = $book->id;
            }
        }
        else{
            foreach ($books as $book) {                
                $array [] = $book->id;
            }
        }

        $founds_book = FoundationsBook::find()->where(['book_id' => $array])->all();
        $result = [];
        foreach ($founds_book as $value) {
            $result [] = $value->number;
        }
        $query = Books::find()->where(['id'=>array_unique($result)]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['title' => SORT_ASC]]
        ]);

        return $dataProvider;       
    }
}
