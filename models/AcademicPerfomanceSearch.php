<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AcademicPerfomance;

/**
 * AcademicPerfomanceSearch represents the model behind the search form about `app\models\AcademicPerfomance`.
 */
class AcademicPerfomanceSearch extends AcademicPerfomance
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'subject_id'], 'integer'],
            [['semester', 'hours', 'controll', 'result', 'ball', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (Yii::$app->user->identity->id) {
            if (Yii::$app->user->identity->type == 'student') {
                $userId = Yii::$app->user->identity->id;
                $studentId = Student::find()->where(['user_id' => $userId])->one();
                if ($studentId!=null) {
                    $query = AcademicPerfomance::find()->where(['student_id' => $studentId->id])->andWhere(['not', ['student_id' => null]]);
                } else {
                    $query = AcademicPerfomance::find();
                }
            } else {
                $query = AcademicPerfomance::find();
            }
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'course_id' => $this->course_id,
            'subject_id' => $this->subject_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'hours', $this->hours])
            ->andFilterWhere(['like', 'controll', $this->controll])
            ->andFilterWhere(['like', 'result', $this->result])
            ->andFilterWhere(['like', 'ball', $this->ball]);

        return $dataProvider;
    }
}
