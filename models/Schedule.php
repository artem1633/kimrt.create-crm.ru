<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "schedule".
 *
 * @property integer $id
 * @property integer $time_id
 * @property integer $day_id
 * @property string $subject
 * @property string $created_at
 * @property string $updated_at
 */
class Schedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time_id', 'day_id'], 'integer'],
            [['created_at', 'updated_at', 'time_id', 'day_id', 'teacher_id', 'created_at', 'date_id', 'group_id', 'room_id'], 'safe'],
            [['subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time_id' => 'Время',
            'day_id' => 'День недели',
            'date_id' => 'Дата начала недели',
            'room_id' => 'Аудитория',
            'group_id' => 'Группа',
            'teacher_id' => 'Преподаватель',
            'subject' => 'Предмет',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    public function setChanging($old, $new)
    {            
        if($old->time_id != $new->time_id) Changing::setToChangeTable('schedule', $new->id, 'Время', $old->time->start, $new->time->start );
        if($old->day_id != $new->day_id) Changing::setToChangeTable('schedule', $new->id, 'День недели', $old->weekDay->day, $new->weekDay->day );
        if($old->date_id != $new->date_id) Changing::setToChangeTable('schedule', $new->id, 'Дата начала недели', $old->date->week_start, $new->date->week_start );
        if($old->room_id != $new->room_id) Changing::setToChangeTable('schedule', $new->id, 'Аудитория', $old->room->number, $new->room->number );
        if($old->group_id != $new->group_id) Changing::setToChangeTable('schedule', $new->id, 'Группа', $old->group->name, $new->group->name );
        if($old->teacher_id != $new->teacher_id) Changing::setToChangeTable('schedule', $new->id, 'Преподаватель', $old->teacher->full_name, $new->teacher->full_name );
        if($old->subject != $new->subject) Changing::setToChangeTable('schedule', $new->id, 'Предмет', $old->subject, $new->subject);
    }

    /**
     * @inheritdoc
     */
    public function getTime()
    {
        return $this->hasOne(Time::className(), ['id' => 'time_id']);
    }

    /**
     * @inheritdoc
     */
    public function getDate()
    {
        return $this->hasOne(Date::className(), ['id' => 'date_id']);
    }

    /**
     * @inheritdoc
     */
    public function getWeekDay()
    {
        return $this->hasOne(WeekDay::className(), ['id' => 'day_id']);
    }

    /**
     * @inheritdoc
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'teacher_id']);
    }

    /**
     * @inheritdoc
     */
    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'room_id']);
    }

    /**
     * @inheritdoc
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }
}
