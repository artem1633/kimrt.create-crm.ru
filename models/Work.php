<?php

namespace app\models;

use app\Traits\RelationHelper;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "works".
 *
 * @property integer $id
 * @property integer $subject_id
 * @property string $theme
 * @property string $work
 * @property string $review
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Subjects $subject
 */
class Work extends \yii\db\ActiveRecord
{
    use RelationHelper;

    /**
     *  Файлы для сохранения
     */
    public $file;
    public $fileReview;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'works';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject_id'], 'integer'],
            [['created_at', 'updated_at', 'file', 'is_approved'], 'safe'],
            [['theme', 'work', 'review'], 'string', 'max' => 255],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['subject_id' => 'id']],
//            ['subject_id', ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject_id' => 'Дисциплина',
            'theme' => 'Тема работы',
            'work' => 'Работа (ссылка на файл)',
            'review' => 'Рецензия (ссылка на файл)',
            'is_approved' => 'Одобрено',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    public function setChanging($old, $new)
    {            
        if($old->subject_id != $new->subject_id) Changing::setToChangeTable('work', $new->id, 'Дисциплина', $old->subject->name, $new->subject->name );
        if($old->theme != $new->theme) Changing::setToChangeTable('work', $new->id, 'Тема работы', $old->theme, $new->theme );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    /**
     *  Загрузка файла
     */
    public function upload($file = "courseWork")
    {
        /**
         *  TODO: Добавить проверку, если директории нет, то создавать,
         *  TODO: чтобы работы разных пользователей были по папкам
         */
        if ($this->validate()) {
            if ($file == "courseWork") {
                $link = 'uploads/' . $file . '_' . Yii::$app->user->identity->id . '_' . $this->file->baseName . '_' . date('Y-m-d') . '.' . $this->file->extension;

                $this->file->saveAs($link);
                $this->work = Yii::$app->params['siteName'] . $link;
                $this->update(false);
            }
            else if ($file == "review") {
                $link = 'uploads/' . $file . '_' . Yii::$app->user->identity->id . '_' . $this->file->baseName . '_' . date('Y-m-d') . '.' . $this->file->extension;

                $this->file->saveAs($link);
                $this->review = Yii::$app->params['siteName'] . $link;
                $this->update(false);
            }

            return true;
        } else {
            return false;
        }
    }
}
