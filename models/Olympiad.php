<?php

namespace app\models;

use app\Traits\RelationHelper;
use Yii;

/**
 * This is the model class for table "olympiads".
 *
 * @property integer $id
 * @property string $name
 * @property string $level
 * @property string $location
 * @property string $time
 * @property string $result
 * @property string $document
 * @property string $created_at
 * @property string $updated_at
 */
class Olympiad extends \yii\db\ActiveRecord
{
    use RelationHelper;

    /**
     *  Файлы для сохранения
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'olympiads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time', 'result', 'document', 'created_at', 'updated_at', 'is_approved'], 'safe'],
            [['name', 'level', 'location'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название олимпиады',
            'level' => 'Уровень',
            'location' => 'Место проведения',
            'time' => 'Время проведения',
            'result' => 'Результат',
            'document' => 'Документ',
            'is_approved' => 'Одобрено',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    public function setChanging($old, $new)
    {            
        if($old->name != $new->name) Changing::setToChangeTable('olympiad', $new->id, 'Название олимпиады', $old->name, $new->name );
        if($old->level != $new->level) Changing::setToChangeTable('olympiad', $new->id, 'Уровень', $old->level, $new->level );
        if($old->location != $new->location) Changing::setToChangeTable('olympiad', $new->id, 'Место проведения', $old->location, $new->location );
        if($old->time != $new->time) Changing::setToChangeTable('olympiad', $new->id, 'Время проведения', $old->time, $new->time );
        if($old->result != $new->result) Changing::setToChangeTable('olympiad', $new->id, 'Результат', $old->result, $new->result );
    }

    /**
     *  Загрузка файла
     */
    public function upload()
    {
        /**
         *  TODO: Добавить проверку, если директории нет, то создавать,
         *  TODO: чтобы работы разных пользователей были по папкам
         */
        if ($this->validate()) {
            $link = 'uploads/olympiad_' . Yii::$app->user->identity->id . '_' . $this->file->baseName . '_' . date('Y-m-d') . '.' . $this->file->extension;

            $this->file->saveAs($link);
            $this->document = Yii::$app->params['siteName'] . $link;
            $this->update(false);

            return true;
        } else {
            return false;
        }
    }
}
