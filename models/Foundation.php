<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "foundation".
 *
 * @property int $id
 * @property string $name
 *
 * @property Orders[] $orders
 */
class Foundation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foundation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }

    public function setChanging($old, $new)
    {            
        if($old->name != $new->name) Changing::setToChangeTable('foundation', $new->id, 'Наименование', $old->name, $new->name );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['library_fond' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Books::className(), ['library_fond' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoundationsBooks()
    {
        return $this->hasMany(FoundationsBook::className(), ['book_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDownloadBooks()
    {
        return $this->hasMany(DownloadBooks::className(), ['foundation_id' => 'id']);
    }
}
