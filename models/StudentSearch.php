<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Student;

/**
 * StudentSearch represents the model behind the search form of `app\models\Student`.
 */
class StudentSearch extends Student
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'floor', 'group_id', 'specialty_id', 'course_id'], 'integer'],
            [['code', 'surname', 'name', 'patronymic', 'date_birth', 'level', 'training', 'payment', 'enrollment', 'enrollment_date', 'expulsion', 'expulsion_date', 'status', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Student::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_birth' => $this->date_birth,
            'floor' => $this->floor,
            'group_id' => $this->group_id,
            'specialty_id' => $this->specialty_id,
            'course_id' => $this->course_id,
            'enrollment_date' => $this->enrollment_date,
            'expulsion_date' => $this->expulsion_date,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'patronymic', $this->patronymic])
            ->andFilterWhere(['like', 'level', $this->level])
            ->andFilterWhere(['like', 'training', $this->training])
            ->andFilterWhere(['like', 'payment', $this->payment])
            ->andFilterWhere(['like', 'enrollment', $this->enrollment])
            ->andFilterWhere(['like', 'expulsion', $this->expulsion])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
