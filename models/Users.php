<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property integer $is_deletable
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $new_parol;
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'type'], 'required'],
            [['is_deletable'], 'integer'],
            [['email'], 'email'],
            [['login', 'name', 'password', 'comment', 'new_parol', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'login' => 'Логин',
            'password' => 'Пароль',
            'type' => 'Тип',
            'comment' => 'Комментарий',
            'is_deletable' => 'Удалить',
            'new_parol' => 'Новый пароль',
            'email' => 'Email',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {//если заказ завершен то ставим дату изменения на дату фактическую
            if ($this->isNewRecord){
                $this->passwd = $this->password;
                $this->password = md5($this->password);
                
            }else{
                /*$userInf = User::find()->where(['id'=>$this->id])->one();
                if($userInf->password <> $this->password)$this->password = md5($this->password);*/
            }
            return true;
        } else {
            return false;
        }

    }


    /**
     * @return array
     */
    public function getAllType()
    {
        return  [
            'admin' => 'Администратор',
            'teacher' => 'Учитель',
            'student' => 'Студент',
        ];
    }

    public function getTypeName($name)
    {
        if($name == 'admin') return 'Администратор';
        if($name == 'teacher') return 'Учитель';
        if($name == 'student') return 'Студент';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDownloadBooks()
    {
        return $this->hasMany(DownloadBooks::className(), ['user_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutobiographies()
    {
        return $this->hasMany(Autobiography::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChangings()
    {
        return $this->hasMany(Changing::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['user_id' => 'id']);
    }

    public function setChanging($old, $new)
    {            
        if($old->name != $new->name) Changing::setToChangeTable('users', $new->id, 'ФИО', $old->name, $new->name );
        if($old->login != $new->login) Changing::setToChangeTable('users', $new->id, 'Логин', $old->login, $new->login );
        if($old->type != $new->type) Changing::setToChangeTable('users', $new->id, 'Тип', $this->getTypeName($old->type), $this->getTypeName($new->type) );
        if($old->comment != $new->comment) Changing::setToChangeTable('users', $new->id, 'Комментарий', $old->comment, $new->comment );
        if($old->email != $new->email) Changing::setToChangeTable('users', $new->id, 'Email', $old->email, $new->email );
    }
}
