<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AdditionalEducation;

/**
 * AdditionalEducationSearch represents the model behind the search form about `app\models\AdditionalEducation`.
 */
class AdditionalEducationSearch extends AdditionalEducation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'hours', 'location_time', 'document_name', 'document', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $studentId = $this->getStudent();

        $query = AdditionalEducation::find()->where(['student_id' => $studentId])->andWhere(['not', ['student_id' => null]]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'document_name' => $this->document_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'hours', $this->hours])
            ->andFilterWhere(['like', 'location_time', $this->location_time])
            ->andFilterWhere(['like', 'document', $this->document]);

        return $dataProvider;
    }
}
