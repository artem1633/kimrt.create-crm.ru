<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "library".
 *
 * @property integer $id
 * @property string $ssilka
 * @property string $logotip
 *
 * @property LibraryBooks[] $libraryBooks
 * @property Books[] $books
 */
class Library extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'library';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ssilka', 'logotip'], 'required'],
            [['logotip', 'description', 'image_name'], 'string'],
            [['ssilka'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ssilka' => 'Ссылка',
            'logotip' => 'Название логотипа',
            'description' => 'Название системы',
            'image_name' => 'Имя картинок',
        ];
    }

    public function setChanging($old, $new)
    {            
        if($old->ssilka != $new->ssilka) Changing::setToChangeTable('library', $new->id, 'Ссылка', $old->ssilka, $new->ssilka );
        if($old->logotip != $new->logotip) Changing::setToChangeTable('library', $new->id, 'Название логотипа', $old->logotip, $new->logotip );
        if($old->description != $new->description) Changing::setToChangeTable('library', $new->id, 'Название системы', $old->description, $new->description );
        if($old->image_name != $new->image_name) Changing::setToChangeTable('library', $new->id, 'Имя картинок', $old->image_name, $new->image_name );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibraryBooks()
    {
        return $this->hasMany(LibraryBooks::className(), ['library_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Books::className(), ['id' => 'books_id'])->viaTable('library_books', ['library_id' => 'id']);
    }
}
