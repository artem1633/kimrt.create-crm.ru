<?php

namespace app\models;

use app\Traits\RelationHelper;
use Yii;

/**
 * This is the model class for table "art".
 *
 * @property integer $id
 * @property string $type
 * @property string $level
 * @property string $competition_name
 * @property string $date
 * @property string $result
 * @property string $work
 * @property string $document
 * @property string $created_at
 * @property string $updated_at
 */
class Art extends \yii\db\ActiveRecord
{
    use RelationHelper;
    /**
     *  Файлы для сохранения
     */
    public $file;
    public $fileDocument;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'art';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'created_at', 'updated_at', 'is_approved'], 'safe'],
            [['type', 'level', 'competition_name', 'result', 'work', 'document'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Вид деятельности',
            'level' => 'Уровень',
            'competition_name' => 'Название соревнований',
            'date' => 'Дата соревнований',
            'result' => 'Результат',
            'work' => 'Работа',
            'document' => 'Документ',
            'is_approved' => 'Одобрено',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }
    public function setChanging($old, $new)
    {            
        if($old->type != $new->type) Changing::setToChangeTable('art', $new->id, 'Вид деятельности', $old->type, $new->type );
        if($old->level != $new->level) Changing::setToChangeTable('art', $new->id, 'Уровень', $old->level, $new->level );
        if($old->competition_name != $new->competition_name) Changing::setToChangeTable('art', $new->id, 'Название соревнований', $old->competition_name, $new->competition_name );
        if($old->date != $new->date) Changing::setToChangeTable('art', $new->id, 'Дата соревнований', $old->date, $new->date );
        if($old->result != $new->result) Changing::setToChangeTable('art', $new->id, 'Результат', $old->result, $new->result );
    }

    /**
     *  Загрузка файла
     */
    public function upload($file = "work")
    {
        try {
            /**
             *  TODO: Добавить проверку, если директории нет, то создавать,
             *  TODO: чтобы работы разных пользователей были по папкам
             */
            if ($this->validate()) {
                if ($file == "work") {
                    $link = 'uploads/art/' . $file . '_' . Yii::$app->user->identity->id . '_' . $this->file->baseName . '_' . date('Y-m-d') . '.' . $this->file->extension;

                    $this->file->saveAs($link);
                    $this->work = Yii::$app->params['siteName'] . $link;
                    $this->update(false);
                }
                else if ($file == "document") {
                    $link = 'uploads/art/' . $file . '_' . Yii::$app->user->identity->id . '_' . $this->file->baseName . '_' . date('Y-m-d') . '.' . $this->file->extension;

                    $this->file->saveAs($link);
                    $this->document = Yii::$app->params['siteName'] . $link;
                    $this->update(false);
                }

                return true;
            } else {
                return false;
            }
        }
        catch (Exception $exception) {
            return var_dump ("Файл не загружен.");
        }
    }
}
