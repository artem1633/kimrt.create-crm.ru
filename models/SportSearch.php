<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sport;

/**
 * SportSearch represents the model behind the search form about `app\models\Sport`.
 */
class SportSearch extends Sport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['sport', 'level', 'competition', 'date', 'result', 'document', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $studentId = $this->getStudent();

        $query = Sport::find()->where(['student_id' => $studentId])->andWhere(['not', ['student_id' => null]]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'sport', $this->sport])
            ->andFilterWhere(['like', 'level', $this->level])
            ->andFilterWhere(['like', 'competition', $this->competition])
            ->andFilterWhere(['like', 'result', $this->result])
            ->andFilterWhere(['like', 'document', $this->document]);

        return $dataProvider;
    }
}
