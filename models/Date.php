<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "dates".
 *
 * @property integer $id
 * @property string $date
 * @property string $created_at
 * @property string $updated_at
 */
class Date extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['week_start', 'week_end', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'week_start' => 'Начало недели',
            'week_end' => 'Конец недели',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    public function getTime()
    {
        return $this->hasMany(Time::className(), ['date_id' => 'id']);
    }

    public static function getDateList()
    {
        return ArrayHelper::map(Date::find()->all(), 'id', 'week_start');
    }

    public function setChanging($old, $new)
    {            
        if($old->week_start != $new->week_start) Changing::setToChangeTable('date', $new->id, 'Начало недели', $old->week_start, $new->week_start );
        if($old->week_end != $new->week_end) Changing::setToChangeTable('date', $new->id, 'Конец недели', $old->week_end, $new->week_end );
    }
}
