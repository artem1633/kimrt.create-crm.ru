<?php

namespace app\models;

use app\Traits\RelationHelper;
use app\models\Student;
use Yii;

/**
 * This is the model class for table "messages".
 *
 * @property integer $id
 * @property string $theme
 * @property string $text
 * @property integer $student_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Users $student
 */
class Message extends \yii\db\ActiveRecord
{
    use RelationHelper;

    const STATUS_SEEN = "seen";
    const STATUS_NOT_SEEN = "not_seen";

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['student_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['theme'], 'string', 'max' => 255],
//            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'theme' => 'Тема проекта',
            'text' => 'Текст',
            'student_id' => 'Студент',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id']);
    }

    /**
     * Запись всем студентам о новом сообщении
     */
    public function incrementNewMessages()
    {
        if ($this->student_id == 0) {
            \app\models\Student::updateAll(['new_messages' => 0], ['new_messages' => null]);
            \app\models\Student::updateAllCounters(['new_messages' => '1']);
        }
    }
}
