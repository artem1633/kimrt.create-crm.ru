<?php

namespace app\models;

use app\Traits\RelationHelper;
use Yii;
use yii\helpers\ArrayHelper;
use app\models\Course;
use app\models\Student;

/**
 * This is the model class for table "academic_perfomance".
 *
 * @property integer $id
 * @property integer $course_id
 * @property string $semester
 * @property integer $subject_id
 * @property string $hours
 * @property string $controll
 * @property string $result
 * @property string $ball
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Course $course
 * @property Subjects $subject
 */
class AcademicPerfomance extends \yii\db\ActiveRecord
{
    use RelationHelper;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'academic_perfomance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'subject_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['semester', 'hours', 'controll', 'result', 'ball'], 'string', 'max' => 255],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['subject_id' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Курс',
            'semester' => 'Семестр',
            'subject_id' => 'Дисциплина',
            'hours' => 'Объем',
            'controll' => 'Форма контроля',
            'result' => 'Результат',
            'ball' => 'Балл',
            'student_id' => 'Студент',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата добавления',
        ];
    }

    public function setChanging($old, $new)
    {            
        if($old->course_id != $new->course_id) Changing::setToChangeTable('academic-perfomance', $new->id, 'Курс', $old->course->name, $new->course->name );
        if($old->semester != $new->semester) Changing::setToChangeTable('academic-perfomance', $new->id, 'Семестр', $old->semester, $new->semester );
        if($old->subject_id != $new->subject_id) Changing::setToChangeTable('academic-perfomance', $new->id, 'Дисциплина', $old->subject->name, $new->subject->name );
        if($old->hours != $new->hours) Changing::setToChangeTable('academic-perfomance', $new->id, 'Объем', $old->hours, $new->hours );
        if($old->controll != $new->controll) Changing::setToChangeTable('academic-perfomance', $new->id, 'Форма контроля', $old->controll, $new->controll );
        if($old->result != $new->result) Changing::setToChangeTable('academic-perfomance', $new->id, 'Результат', $old->result, $new->result );
        if($old->ball != $new->ball) Changing::setToChangeTable('academic-perfomance', $new->id, 'Балл', $old->ball, $new->ball );
        if($old->student_id != $new->student_id) 
        {
            $old_student = Student::findOne($old->student_id);
            $new_student = Student::findOne($new->student_id);
            Changing::setToChangeTable('academic-perfomance', $new->id, 'Студент', $old_student->surname . ' ' . $old_student->name . ' ' . $old_student->patronymic, $new_student->surname . ' ' . $new_student->name . ' ' . $new_student->patronymic);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id']);
    }

    /**
     *  Список курсов
     */
    public function getCourses()
    {
        $arrayMap = ArrayHelper::map(Course::find()->all(), 'id', 'name');
        $defaultValue = ['0' => 'Выберите курс'];

        $arrayMap = ArrayHelper::merge($defaultValue, $arrayMap);

        return $arrayMap;
    }

    /**
     *  Список дисциплин
     */
    public function getStudents()
    {
        $arrayMap = ArrayHelper::map(Student::find()->all(), 'id', 'surname');
        $defaultValue = ['0' => 'Выберите студента'];

        $arrayMap = ArrayHelper::merge($defaultValue, $arrayMap);

        return $arrayMap;
    }
}
