<?php

namespace app\Traits;

use app\models\Student;
use Yii;
use yii\helpers\ArrayHelper;
use app\models\Subject;

trait RelationHelper
{
    private $student;

    public function __construct($studentId="Not set.")
    {
        parent::__construct();

        $this->setStudent($studentId);
    }

    public function setStudent($student)
    {
        $this->student = $student;
    }

    public function getStudent()
    {
        $userId = Yii::$app->user->identity->id;

        if ($this->student == "Not set.") {
            $studentId = Student::find()->where(['user_id' => $userId])->one();
            if ($studentId!=null) {
                $studentId = $studentId->id;
            }
        }
        else {
            $studentId = $this->student;
        }

        return $studentId;
    }

    public function getStudentAP()
    {
        if ($this->student == "Not set.") {
            return null;
        }
        else {
            $studentId = Student::find()->where(['id' => $this->student])->one();
            if ($studentId!=null) {
                $studentId = $studentId->id;
            }
        }

        return $studentId;
    }

    /**
     *  Связка со текущим пользователем
     */
    public function attachStudent()
    {
        $this->student_id = Student::findOne(['user_id' => Yii::$app->user->identity->id])->id;

        $this->update(false);
    }

    /**
     *  Список дисциплин
     */
    public function getSubjects()
    {
        $arrayMap = ArrayHelper::map(Subject::find()->all(), 'id', 'name');
        $defaultValue = ['0' => 'Выберите дисциплину'];

        $arrayMap = ArrayHelper::merge($defaultValue, $arrayMap);

        return $arrayMap;
    }

    /**
     *  Список студентов
     */
    public function getStudents()
    {
        $arrayMap = ArrayHelper::map(Student::find()->all(), 'id', 'surname');
        $defaultValue = ['0' => 'Всем студентам'];

        $arrayMap = ArrayHelper::merge($defaultValue, $arrayMap);

        return $arrayMap;
    }

    /**
     * Одобрить портфолио
     */
    public function approve()
    {
        if ($this->is_approved) {
            $this->is_approved = false;
            $this->update(false);
        }
        else {
            $this->is_approved = true;
            $this->update(false);
        }
    }
}