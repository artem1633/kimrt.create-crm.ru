<?php

namespace app\controllers;

use Yii;
use app\models\Books;
use app\models\BooksSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Uploadxml;
use yii\web\UploadedFile;
use app\models\FoundationsBook;
use app\models\BooksSections;
use app\models\Foundation;
use \yii\web\Response;
use app\models\Orders;
use yii\helpers\Html;
use app\models\Changing;

/**
 * BooksController implements the CRUD actions for Books model.
 */
class BooksController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Books models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BooksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new Uploadxml();
        Changing::setToChangeTable('books', \Yii::$app->user->identity->id, '', '', 'Зашел на вкладку Книжный фонд');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    public function actionSearch($advanced = null)
    {
        $post = Yii::$app->request->post();
        if(!isset($post['BooksSearch']['search_type'])) $post['BooksSearch']['search_type'] = 1;
        if(!isset($post['BooksSearch']['search_condition'])) $post['BooksSearch']['search_condition'] = 1;
        if(!isset($post['BooksSearch']['search_area'])) $post['BooksSearch']['search_area'] = null;
        if(!isset($post['BooksSearch']['search_found'])) $post['BooksSearch']['search_found'] = 0;
        if(!isset($post['BooksSearch']['search_advanced'])) $post['BooksSearch']['search_advanced'] = null;

        if($advanced == null) $advanced = $post['BooksSearch']['search_advanced'];
        else $post['BooksSearch']['search_advanced'] = $advanced;

        if($advanced == null) $providers = Books::getBooksList($post);
        $searchModel = new BooksSearch();
        Changing::setToChangeTable('books', \Yii::$app->user->identity->id, '', '', 'Зашел на вкладку Электронный каталог');
        return $this->render('search', [
            'searchModel' => $searchModel,
            'post' => $post,
            'advanced' => $advanced,
        ]);
    }

    public function actionOrder($id,$found_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $model = new Orders();
        $model->library_fond = $found_id;
        $model->book_id = $id;
        $book = Books::findOne($id);


                
        if ($model->load($request->post())) {

            $found_book = FoundationsBook::find()->where(['id' => $model->library_fond])->one();
            if($found_book->quantity < 1){
                return [
                    'title'=> "Ошибка",
                    'size' => 'normal',
                    'content'=>'<span class="text-danger" style ="color:red; font-size:20px;"><center>Выбранная вами книга не осталось на фонде библиотеки</center></span>',
                ];
            }
            $model->library_fond = $found_book->book->library_fond;
            $model->save();
            $book->count = $book->count - 1;
            $book->save();
            \Yii::$app->db->createCommand()->update('foundations_book', ['quantity' => new \yii\db\Expression('quantity - 1')], [ 'book_id' => $id ])->execute();
            Changing::setToChangeTable('orders', $model->id, '', '', 'Заказал книгу под номером №' . $id);
            return [
                'forceClose' => true,
            ];

        } else {
            return [
                'title'=> "Заказать",
                'size' => 'large',
                'content'=>$this->renderAjax('orders_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Оформить заказ ',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }

    public function actionImport()
    {
        $model = new Uploadxml();

        if (Yii::$app->request->isPost && Yii::$app->user->identity->type == "admin") {
            try {
                $model->file = UploadedFile::getInstance($model, 'file');

                $fileName = $model->file->baseName . '.' . $model->file->extension;
                if ($model->file && $model->validate()) {
                    $model->file->saveAs('xml/' . $fileName);
                }

                $filePath = 'xml/' . $fileName;
                $file = file_get_contents($filePath);
                $xml = simplexml_load_string($file);

                foreach ($xml as $book) {

                    $razdel = BooksSections::find()->where(['name' => (string)$book['Section'] ])->one();
                    if($razdel == null){
                        $razdel = new BooksSections();
                        $razdel->name = (string)$book['Section'];
                        $razdel->save();
                    }
                    foreach ($book as $library) {
                        foreach ($library as $key) {
                            $fond = Foundation::find()->where(['name' => (string)$key['name'] ])->one();
                            if($fond == null){
                                $fond = new Foundation();
                                $fond->name = (string)$key['name'];
                                $fond->save();
                            }
                           
                            $model = new Books();
                            $model->id_book = (string)$book['ID_Book'];
                            $model->author = (string)$book['author'];
                            $model->title = (string)$book['Title'];
                            $model->publishing_address = (string)$book['Place_of_Publication'];
                            $model->publishing_name = (string)$book['Publishing_house'];
                            $model->publishing_year = (string)$book['year_o_publishing'];
                            $model->book_section_id = $razdel->id;
                            $model->bbk = (string)$book['BBK_Code'];
                            $model->library_fond = $fond->id;
                            $model->count = (int)$key['quantity'];

                            $old_book = $model->findBook();
                            if( $old_book != null){

                                $found_book = FoundationsBook::find()->where(['foundation_id' => $model->library_fond, 'book_id' => $old_book->id])->one();
                                
                                if($found_book != null){
                                    $found_book->quantity = $model->count;
                                    $found_book->save();  
                                    $old_book->count = $model->count;
                                    $old_book->save();
                                }
                            }
                            else {
                                $model->save();
                                $found_book = new FoundationsBook();
                                $found_book->quantity = $model->count;
                                $found_book->foundation_id = $model->library_fond;
                                $found_book->book_id = $model->id;
                                $found_book->id_book = $model->id_book;
                                $found_book->number = $model->getNumber();
                                $found_book->save();
                            }                            
                        }                          
                    }                    
                }
                                
                return $this->redirect(['index']);
            }
            catch (Exception $exception) {
                return $exception;
            }
        }
    }

    /**
     * @return mixed
     */
    public function actionLoad($id)
    {
        $model = Books::findOne($id);

        if($model == null)
            throw new NotFoundHttpException('Книжный фонд не найдено');

        $book = FoundationsBook::find()->where(['book_id' => $id])->one();
        $books = FoundationsBook::find()->where(['number' => $book->number])->all();

        $xmlMarkup = $this->renderPartial('@app/views/xml/xml_books', [
            'model' => $model,
            'books' => $books,
        ]);

        $xmlTmp = tmpfile();
        $fileInfo = stream_get_meta_data($xmlTmp);
        fwrite($xmlTmp, $xmlMarkup);
        Yii::$app->response->sendFile($fileInfo['uri'], $model->title.'_'.date('d.m.Y H:i:s').'.xml');
        fclose($xmlTmp);
        Changing::setToChangeTable('books', \Yii::$app->user->identity->id, '', '', 'Скачал книгу под номером ' . $id . ' на формате xml');
    }

    /**
     * Displays a single Books model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        Changing::setToChangeTable('books', \Yii::$app->user->identity->id, '', '', 'Посмотрел данные книги под номером №' . $id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionBookCount($a, $id)
    {
        $old = Books::findOne($id);
        $book = Books::find()->where(['id_book' => $old->id_book, 'library_fond' => $id])->one();
        return $book->count;
    }

    /**
     * Creates a new Books model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Books();

        if ($model->load(Yii::$app->request->post())) {

            $book = $model->findBook();
            if( $book != null){
                $found_book = FoundationsBook::find(['foundation_id' => $model->library_fond, 'book_id' => $book->id])->one();
                if($found_book != null){
                    $found_book->quantity = $model->count;
                    $found_book->save();
                    $book->count = $model->count;
                    $book->save();
                }
            }
            else {
                $model->save();
                $found_book = new FoundationsBook();
                $found_book->quantity = $model->count;
                $found_book->foundation_id = $model->library_fond;
                $found_book->book_id = $model->id;
                $found_book->number = $model->getNumber();
                $found_book->save();
            }
            Changing::setToChangeTable('books', \Yii::$app->user->identity->id, '', '', 'Добавил книгу под номером №' . $model->id);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Books model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->setChanging($old, $model);
            $found_book = FoundationsBook::find()->where(['book_id' => $model->id])->one();
            $found_book->quantity = $model->count;
            $found_book->save();
            if($model->Changing($old,$model)){
                $books = FoundationsBook::find()->where(['number' => $found_book->number])->all();
                foreach ($books as $value) {
                    $book = Books::findOne($value->book_id);
                    $book->author = $model->author;
                    $book->title = $model->title;
                    $book->publishing_address = $model->publishing_address;
                    $book->publishing_name = $model->publishing_name;
                    $book->publishing_year = $model->publishing_year;
                    $book->book_section_id = $model->book_section_id;
                    $book->bbk = $model->bbk;
                    $book->id_book = $model->id_book;
                    $book->save();
                    $value->id_book = $model->id_book;
                    $value->save();
                }
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Books model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $foundation_books = FoundationsBook::find()->where(['book_id' => $id])->all();
        foreach ($foundation_books as $value) {
            $value->delete();
        }
        $this->findModel($id)->delete();
        Changing::setToChangeTable('books', \Yii::$app->user->identity->id, '', '', 'Удалил книгу под номером №' . $id);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Books model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Books the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Books::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
