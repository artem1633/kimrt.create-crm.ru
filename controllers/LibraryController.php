<?php

namespace app\controllers;

use Yii;
use app\models\Library;
use app\models\LibrarySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Uploads;
use yii\web\UploadedFile;
use app\models\Changing;
/**
 * LibraryController implements the CRUD actions for Library model.
 */
class LibraryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Library models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LibrarySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Changing::setToChangeTable('library', \Yii::$app->user->identity->id, '', '', 'Зашел на вкладку Библиотека');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Library model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->identity->type == 'student') return $this->redirect(['index']);
        Changing::setToChangeTable('library', \Yii::$app->user->identity->id, '', '', 'Посмотрел данные библиотек под номером №' . $id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Library model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->identity->type == 'student') return $this->redirect(['index']);

        $model = new Library();
        $upload = new Uploads();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $upload->File = UploadedFile::getInstance($upload, 'File');
            if ($upload->File != null) {
                if ($upload->upload($model->id)) {
                    Yii::$app->db->createCommand()->update('library', ['image_name' => $model->id . '.' . $upload->File->extension], ['id' => $model->id])->execute();
                }
            }
            Changing::setToChangeTable('library', \Yii::$app->user->identity->id, '', '', 'Добавил библиотеку под номером №' . $model->id);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'upload' => $upload,
            ]);
        }
    }

    /**
     * Updates an existing Library model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->identity->type == 'student') return $this->redirect(['index']);

        $model = $this->findModel($id);
        $old_model = $this->findModel($id);
        $upload = new Uploads();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $upload->File = UploadedFile::getInstance($upload, 'File');
            if ($upload->File != null) {
                if ($upload->upload($model->id)) {
                    Yii::$app->db->createCommand()->update('library', ['image_name' => $model->id . '.' . $upload->File->extension], ['id' => $model->id])->execute();
                }
            }
            $model->setChanging($old_model, $model);

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'upload' => $upload,
            ]);
        }
    }

    public function actionViewFile($id)
    {
        $model = $this->findModel($id);

        return $this->render('file', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing Library model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->identity->type == 'student') return $this->redirect(['index']);
        $this->findModel($id)->delete();
        Changing::setToChangeTable('library', \Yii::$app->user->identity->id, '', '', 'Удалил библиотеку под номером №' . $id);
        return $this->redirect(['index']);
    }

    public function actionTest()
    {
        $model = Library::find()->select(['library_books.*', 'library.*', 'books.*'])->joinWith(['books']);

        echo $model->createCommand()->getRawSql()."<br><br>";

        $models = $model->asArray()->all();

        foreach ($models as $model)
        {
            echo $model['title'].' '.$model['quantity'].'<br>';
        }

    }

    /**
     * Finds the Library model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Library the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Library::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
