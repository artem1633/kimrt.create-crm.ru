<?php

namespace app\controllers;

use Yii;
use app\models\ScientificProjects;
use app\models\ScientificProjectsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Changing;

/**
 * ScientificProjectsController implements the CRUD actions for ScientificProjects model.
 */
class ScientificProjectsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ScientificProjects models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScientificProjectsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Changing::setToChangeTable('scientific-projects', \Yii::$app->user->identity->id, '', '', 'Зашел на вкладку Научные проекты');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'student' => null,
        ]);
    }

    /**
     * Displays a single ScientificProjects model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        Changing::setToChangeTable('scientific-projects', \Yii::$app->user->identity->id, '', '', 'Посмотрел данные научного проекта под номером №' . $id);

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ScientificProjects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ScientificProjects();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $model->attachStudent();
            Changing::setToChangeTable('scientific-projects', $model->id, '', '', 'Добавил научного проекта под номером №' . $model->id);

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ScientificProjects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $student = Yii::$app->request->get('student');
        $old = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($student != null) {
                return $this->redirect(['/portfolio/index?student=' . $student]);
//                return $this->goBack();
            }
            $model->setChanging($old, $model);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ScientificProjects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        Changing::setToChangeTable('scientific-projects', \Yii::$app->user->identity->id, '', '', 'Удалил научного проекта под номером №' . $id);
        return $this->redirect(['index']);
    }

    /**
     * Finds the ScientificProjects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScientificProjects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ScientificProjects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Одобрить портфолио студента
     *
     */
    public function actionApprove()
    {
        $id = Yii::$app->request->get('id');
        $student = Yii::$app->request->get('student');

        $this->findModel($id)->approve();

        return $this->redirect('/portfolio/index?student=' . $student);
    }
}
