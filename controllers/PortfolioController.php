<?php

namespace app\controllers;

use app\models\AcademicPerfomanceSearch;
use app\models\AdditionalEducation;
use app\models\Art;
use app\models\Contest;
use app\models\Extracurricular;
use app\models\Olympiad;
use app\models\ScientificProjects;
use app\models\Student;
use app\models\Work;
use app\models\Sport;
use Yii;
use app\models\Autobiography;
use app\models\AutobiographySearch;
use app\models\WorkSearch;
use app\models\OlympiadSearch;
use app\models\AdditionalEducationSearch;
use app\models\ScientificProjectsSearch;
use app\models\ContestSearch;
use app\models\ExtracurricularSearch;
use app\models\SportSearch;
use app\models\ArtSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Request;
use kartik\mpdf\Pdf;
use app\models\UploadForm;

/**
 * CourseController implements the CRUD actions for Course model.
 */
class PortfolioController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    /**
     * Lists all Course models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity->type == 'student')
            return $this->redirect('/schedule/index');

        $studentId = Yii::$app->request->get('student');

        $autobiographySearch = new AutobiographySearch($studentId);
        $dataProviderAutography = $autobiographySearch->search(Yii::$app->request->queryParams);

        $workSearch = new WorkSearch($studentId);
        $dataProviderWork = $workSearch->search(Yii::$app->request->queryParams);

        $olympiadSearch = new OlympiadSearch($studentId);
        $dataProviderOlympiad = $olympiadSearch->search(Yii::$app->request->queryParams);

        $educationSearch = new AdditionalEducationSearch($studentId);
        $dataProviderEducation = $educationSearch->search(Yii::$app->request->queryParams);

        $searchModelScience = new ScientificProjectsSearch($studentId);
        $dataProviderScience = $searchModelScience->search(Yii::$app->request->queryParams);

        $searchModelContest = new ContestSearch($studentId);
        $dataProviderContest = $searchModelContest->search(Yii::$app->request->queryParams);

        $searchModelExtracurricular = new ExtracurricularSearch($studentId);
        $dataProviderExtracurricular = $searchModelExtracurricular->search(Yii::$app->request->queryParams);

        $searchModelSport = new SportSearch($studentId);
        $dataProviderSport = $searchModelSport->search(Yii::$app->request->queryParams);

        $searchModelArt = new ArtSearch($studentId);
        $dataProviderArt = $searchModelArt->search(Yii::$app->request->queryParams);

        $searchModelAP = new AcademicPerfomanceSearch($studentId);
        $dataProviderAP = $searchModelAP->search(Yii::$app->request->queryParams);

        $uploadModel = new UploadForm();

        return $this->render('index', [
            'autobiographySearch' => $autobiographySearch,
            'dataProviderAutography' => $dataProviderAutography,
            'workSearch' => $workSearch,
            'dataProviderWork' => $dataProviderWork,
            'olympiadSearch' => $olympiadSearch,
            'dataProviderOlympiad' => $dataProviderOlympiad,
            'educationSearch' => $educationSearch,
            'dataProviderEducation' => $dataProviderEducation,
            'searchModelScience' => $searchModelScience,
            'dataProviderScience' => $dataProviderScience,
            'searchModelContest' => $searchModelContest,
            'dataProviderContest' => $dataProviderContest,
            'searchModelExtracurricular' => $searchModelExtracurricular,
            'dataProviderExtracurricular' => $dataProviderExtracurricular,
            'searchModelSport' => $searchModelSport,
            'dataProviderSport' => $dataProviderSport,
            'searchModelArt' => $searchModelArt,
            'dataProviderArt' => $dataProviderArt,
            'searchModelAP' => $searchModelAP,
            'dataProviderAP' => $dataProviderAP,
            'uploadModel' => $uploadModel,
            'student' => $studentId,
        ]);
    }

    public function actionExportPdf()
    {
        if (Yii::$app->user->identity->type == "student") {
            $userId = Yii::$app->user->identity->id;

            $student = Student::find()->where(['user_id' => $userId])->one();

            if ($student == null) {
                return "Учетная запись не привязана к студенту.";
            }
        }

        if (Yii::$app->user->identity->type == 'admin') {
            $student = Yii::$app->request->get('student');
            $student = Student::find()->where(['id' => $student])->one();
        }

        $autobiography = Autobiography::find()->where(['student_id' => $student->id])->one();

        if ($autobiography != null)
            $autobiography = $autobiography->text;

        $works = Work::find()->where(['student_id' => $student->id])->all();
        $olympiads = Olympiad::find()->where(['student_id' => $student->id])->all();
        $additionalEducation = AdditionalEducation::find()->where(['student_id' => $student->id])->all();
        $scientificProjects = ScientificProjects::find()->where(['student_id' => $student->id])->all();
        $contests = Contest::find()->where(['student_id' => $student->id])->all();
        $extracurricular = Extracurricular::find()->where(['student_id' => $student->id])->all();
        $sport = Sport::find()->where(['student_id' => $student->id])->all();
        $art = Art::find()->where(['student_id' => $student->id])->all();

//        return $this->render('portfolio_html_origin.php', [
//            'student' => $student,
//            'authobiography' => $autobiography,
//            'works' => $works,
//            'olympiads' => $olympiads,
//            'additionalEducation' => $additionalEducation,
//            'scientificProjects' => $scientificProjects,
//            'contests' => $contests,
//            'extracurricular' => $extracurricular,
//            'sport' => $sport,
//            'art' => $art,
//        ]);

        $pdf = new Pdf([
            'mode' =>  Pdf :: MODE_UTF8, // leaner size using standard fonts
            'cssFile' => '@web/css/my.css',
            'cssInline' => 'p.header {
    font-family: "Times New Roman", Times, serif;
    margin: 0 !important;
}

td.special {
    border: 2px double black;
}

td {
    min-height: 75%;
    height: 100%;
}

table.portfolio{
    border-collapse: collapse;
    border: 1px solid black;
}',
            'content' => $this->renderPartial('portfolio_html_origin', [
                'student' => $student,
                'authobiography' => $autobiography,
                'works' => $works,
                'olympiads' => $olympiads,
                'additionalEducation' => $additionalEducation,
                'scientificProjects' => $scientificProjects,
                'contests' => $contests,
                'extracurricular' => $extracurricular,
                'sport' => $sport,
                'art' => $art,
            ]),
            'options' => [
                'title' => 'Export PDF',
                'subject' => 'Export PDF'
            ]
        ]);

        return $pdf->render();
    }
}
