<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\UploadForm;
use yii\web\UploadedFile;
use app\models\Changing;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity->type == 'student')
            return $this->redirect('/schedule/index');

        if (Yii::$app->user->identity->type != 'admin')
            return '404';

        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Changing::setToChangeTable('users', \Yii::$app->user->identity->id, '', '', 'Зашел на вкладку Пользователей');
        $model = new UploadForm();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

        /**
     * Метод загрузки и распознавания данных из xls файла
     */
    public function actionImport() {
        $model = new UploadForm();

        if (Yii::$app->request->isPost && Yii::$app->user->identity->type == "admin") {
            try {
                $model->file = UploadedFile::getInstance($model, 'file');

                $fileName = $model->file->baseName . '.' . $model->file->extension;
                if ($model->file && $model->validate()) {
                    $model->file->saveAs('uploads/users/' . $fileName);
                }

                /**
                 * Распознавание загруженного xls
                 */
                $filePath = 'uploads/users/' . $fileName;

                $csvData = \PHPExcel_IOFactory::load($filePath);
                $csvData = $csvData->getActiveSheet()->toArray(null, true, true, false);

                $count = 0;
                foreach ($csvData as $a) {
                    if ($count==0) {
                        $count ++;
                        continue;
                    }

                    $user = new Users();
                    $user->name = $a[0];
                    $user->login = $a[1];
                    $user->password = $a[2];
                    if($a[3] == 'Администратор') $user->type = 'admin';
                    if($a[3] == 'Учитель') $user->type = 'teacher';
                    if($a[3] == 'Студент') $user->type = 'student';
                    $user->save(false);

                    $count++;
                }
            }
            catch (Exception $exception) {
                return $exception;
            }
        }

        return $this->redirect(['users/index']);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->identity->type != "admin")
            return '404';
        Changing::setToChangeTable('users', \Yii::$app->user->identity->id, '', '', 'Посмотрел данные пользователя под номером №' . $id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->identity->type != "admin")
            return '404';

        $model = new Users();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Changing::setToChangeTable('users', \Yii::$app->user->identity->id, '', '', 'Добавил пользователя под номером №' . $model->id);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->identity->type != "admin")
            return '404';

        $model = $this->findModel($id);
        $old_model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->setChanging($old_model, $model);
            if($model->new_parol != "" | $model->new_parol != null)
            {
                Yii::$app->db->createCommand()->update('users', ['password' => md5($model->new_parol)], [ 'id' => $model->id ])->execute();
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected
     *
     *
     * to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->identity->type != "admin")
            return '404';
        Changing::setToChangeTable('users', \Yii::$app->user->identity->id, '', '','Удалил пользователя под номером №' . $id);

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	 public function print_arr($array) {
        echo '<pre>'.print_r($array, true).'</pre>';
    }
}
