<?php

namespace app\controllers;

use app\models\Course;
use app\models\Student;
use app\models\Subject;
use Yii;
use app\models\AcademicPerfomance;
use app\models\AcademicPerfomanceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadForm;
use app\models\Users;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use app\models\Changing;

/**
 * AcademicPerfomanceController implements the CRUD actions for AcademicPerfomance model.
 */
class AcademicPerfomanceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AcademicPerfomance models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AcademicPerfomanceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $uploadModel = new UploadForm();
        Changing::setToChangeTable('academic-perfomance', \Yii::$app->user->identity->id, '', '', 'Зашел на вкладку Успеваемость');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'uploadModel' => $uploadModel,
            'student' => null,
        ]);
    }

    /**
     * Displays a single AcademicPerfomance model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        Changing::setToChangeTable('academic-perfomance', \Yii::$app->user->identity->id, '', '', 'Посмотрел данные успеваемоста под номером №' . $id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AcademicPerfomance model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AcademicPerfomance();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Changing::setToChangeTable('academic-perfomance', \Yii::$app->user->identity->id, '', '', 'Добавил успеваемости под номером №' . $model->id);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AcademicPerfomance model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->setChanging($old_model, $model);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AcademicPerfomance model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Changing::setToChangeTable('academic-perfomance', \Yii::$app->user->identity->id, '', '', 'Удалил успеваемости под номером №' . $id);
        return $this->redirect(['index']);
    }

    /**
     * Finds the AcademicPerfomance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AcademicPerfomance the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AcademicPerfomance::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Метод загрузки и распознавания данных из xls файла
     */
    public function actionImport() {
        $model = new UploadForm();

        if (Yii::$app->request->isPost && Yii::$app->user->identity->type == "admin") {
            try {
                $model->file = UploadedFile::getInstance($model, 'file');

                $fileName = $model->file->baseName . '.' . $model->file->extension;
                if ($model->file && $model->validate()) {
                    $model->file->saveAs('uploads/csv/' . $fileName);
                }

                /**
                 * Распознавание загруженного xls
                 */
                $filePath = 'uploads/csv/' . $fileName;

                $csvData = \PHPExcel_IOFactory::load($filePath);
                $csvData = $csvData->getActiveSheet()->toArray(null, true, true, false);

                $count = 0;
                foreach ($csvData as $a) {
                    if ($count==0) {
                        $count ++;
                        continue;
                    }

                    $ap = new AcademicPerfomance();
//                    $fullName = $this->getFullName($a[1]);

                    $id = $this->attachCourse($a);
                    if ($id != null)
                        $ap->course_id = $id;

                    $ap->semester = $a[6];

                    $id = $this->attachSubject($a);
                    if ($id != null)
                        $ap->subject_id = $id;

                    $ap->hours = $a[9];
                    $ap->controll = $a[10];
                    $ap->result = $a[11];
                    $ap->ball = $a[12];

                    $id = $this->attachStudent($a);
                    if ($id != null)
                        $ap->student_id = $id;

                    $ap->save(false);

                    $count++;
                }
            }
            catch (Exception $exception) {
                return $exception;
            }
        }

        return $this->redirect(['academic-perfomance/index']);
    }

    /**
     * Обработка фио из файла
     */
//    public function getFullName($cell)
//    {
//        $fullName = ['Нет в файле.', 'Нет в файле.', 'Нет в файле.'];
//        $array = explode(" ", $cell);
//
//        for ($i=0; $i<count($array); $i++) {
//            $fullName[$i] = $array[$i];
//        }
//
//        return $fullName;
//    }

    /**
     * Привязка студента
     */
    public function attachStudent($a)
    {
        $student = Student::find()->where(['code' => $a[0]])->one();

        if ($student != null) {
            return $student->id;
        }
        else {
            return null;
        }
    }

    /**
     * Привязка курса
     */
    public function attachCourse($a)
    {
        $course = Course::find()->where(['name' => $a[5]])->one();

        if ($course != null) {
            return $course->id;
        }
        else {
            return null;
        }
    }

    /**
     * Привязка дисциплины
     */
    public function attachSubject($a)
    {
        $subject = Subject::find()->where(['name' => $a[8]])->one();

        if ($subject != null) {
            return $subject->id;
        }
        else {
            return null;
        }
    }
}
