<?php

namespace app\controllers;

use Yii;
use app\models\Student;
use app\models\StudentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\UploadForm;
use yii\web\UploadedFile;
use app\models\Course;
use app\models\Changing;

/**
 * StudentController implements the CRUD actions for Student model.
 */
class StudentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Student models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity->type == 'student')
            return $this->redirect('/portfolio/index');

        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new UploadForm();
        Changing::setToChangeTable('student', \Yii::$app->user->identity->id, '', '', 'Зашел на вкладку Студенты');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

        /**
     * Метод загрузки и распознавания данных из xls файла
     */
    public function actionImport() {
        $model = new UploadForm();

        if (Yii::$app->request->isPost && Yii::$app->user->identity->type == "admin") {
            try {
                $model->file = UploadedFile::getInstance($model, 'file');

                $fileName = $model->file->baseName . '.' . $model->file->extension;
                if ($model->file && $model->validate()) {
                    $model->file->saveAs('uploads/student/' . $fileName);
                }

                /**
                 * Распознавание загруженного xls
                 */
                $filePath = 'uploads/student/' . $fileName;

                $csvData = \PHPExcel_IOFactory::load($filePath);
                $csvData = $csvData->getActiveSheet()->toArray(null, true, true, false);

                $count = 0;
                foreach ($csvData as $a) {
                    if ($count==0) {
                        $count ++;
                        continue;
                    }

                    $student = new Student();
                    $student->surname = $a[1];
                    $student->name = $a[2];
                    $student->patronymic = $a[3];
                    //
                    //$student->date_birth = $a[4];
                    //$student->date_birth = \Yii::$app->formatter->asDate($a[4], 'php:Y-m-d');
                    $student->date_birth = \DateTime::createFromFormat('m-d-y', $a[4])->format('Y-m-d');
                    
                    if($a[5] == 'Мужской') $student->floor = 1;
                    else $student->floor = 2;

                    if($a[6] == 'Бакалавриат') $student->level = 1;
                    if($a[6] == 'Специалитет') $student->level = 2;
                    if($a[6] == 'СПО') $student->level = 3;
                    
                    if($a[7] == 'Очная') $student->training = 1;
                    else $student->training = 2;
                    
                    $q=0;
                    $spes = \app\models\Specialty::find()->all();
                    foreach ($spes as $value) {
                        if($a[8] == $value->name) {$student->specialty_id = $value->id; $q=1; }
                    }
                    if($q == 0) {
                        $s = new \app\models\Specialty();
                        $s->name = $a[8];
                        $s->save(false);
                        $student->specialty_id = $s->id;
                    }

                    $q=0;
                    $cource = Course::find()->all();
                    foreach ($cource as $value) { 
                        if($a[9] == $value->name) {$student->course_id = $value->id; $q=1;}
                    }
                    if($q == 0) {
                        $s = new Course();
                        $s->name = $a[9];
                        $s->save(false);
                        $student->course_id = $s->id;
                    }

                    $q=0;
                    $group = \app\models\Group::find()->all();
                    foreach ($group as $value) {
                        if($a[10] == $value->name) {$student->group_id = $value->id; $q=1; }
                    }
                    if($q == 0) {
                        $s = new \app\models\Group();
                        $s->name = $a[10];
                        $s->save(false);

                        $student->group_id = $s->id;
                    }

                    if($a[11] == 'Бюджет') $student->payment = 1;
                    else $student->payment = 2;                   

                    $student->enrollment = $a[12];
                    //
                    //$student->enrollment_date = $a[13];
                    //$student->enrollment_date = \Yii::$app->formatter->asDate($a[13], 'php:Y-m-d');
                    $student->enrollment_date = \DateTime::createFromFormat('m-d-y', $a[13])->format('Y-m-d');
                    $student->code = $a[14];      

                    $users1 = \app\models\Users::find()->all();
                    foreach ($users1 as $value) {
                        if($a[14] == $value->login) {$student->user_id = $value->id; }
                    }
                    

                    if($a[15] == 'Обучается') $student->status = 1;
                    if($a[15] == 'Отчислен') $student->status = 2;
                    if($a[15] == 'Выпускник') $student->status = 3;                   

                    $student->save(false);

                    /*echo "<pre>";
                    print_r($student);
                    echo "</pre>";die;*/
                    $count++;
                }
            }
            catch (Exception $exception) {
                return $exception;
            }
        }

        return $this->redirect(['student/index']);
    }

    /**
     * Displays a single Student model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        Changing::setToChangeTable('student', \Yii::$app->user->identity->id, '', '', 'Посмотрел данные студента под номером №' . $id);
        return $this->render('view2', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Student model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Student();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Changing::setToChangeTable('student', \Yii::$app->user->identity->id, '', '', 'Добавил студента под номером №' . $model->id);
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->setChanging($old_model, $model);
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Student model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Changing::setToChangeTable('student', \Yii::$app->user->identity->id, '', '', 'Удалил студента под номером №' . $id);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Student model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Student the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Student::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
