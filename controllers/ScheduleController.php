<?php

namespace app\controllers;

use app\models\Date;
use app\models\Group;
use app\models\Schedule;
use app\models\Teacher;
use app\models\Time;
use app\models\WeekDay;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Request;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\ArrayHelper;
use app\models\Room;
use DateTime;
use app\models\Changing;

class ScheduleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /*
     * Вывод расписания
     */
    public function actionIndex()
    {
        $date = Yii::$app->request->get('date');
        $group = Yii::$app->request->get('group');
        $teacher = Yii::$app->request->get('teacher');
        $room = Yii::$app->request->get('room');

        if ($date==null) {
            $date = date('Y-m-d');
        }

        if(Yii::$app->user->identity != null){
            if(Yii::$app->user->identity->type == 'student'){
                $student = \app\models\Student::find()->where(['user_id' => Yii::$app->user->identity->id])->one();
                if ($student) {
                    $group = $student->group_id;
                }           
            }       
        }



        $date = Date::find()->where(['<=', 'week_start', $date])->andWhere(['>=', 'week_end', $date])->one();

        if ($date==null) {
            $time = null;
        }
        else {
            $time = Time::find()->with([
                'schedule' => function ($query) use ($date, $group, $teacher, $room) {
                    $query->andWhere(['date_id' => $date->id]);

                    if ($group!=null) {
                        $query->andWhere(['group_id' => $group]);
                    }
                    if ($teacher!=null) {
                        $query->andWhere(['teacher_id' => $teacher]);
                    }
                    if ($room!=null) {
                        $query->andWhere(['room_id' => $room]);
                    }
                }
            ])->all();
        }

        $days = WeekDay::find()->all();

        $groups = ArrayHelper::map(Group::find()->all(), 'id', 'name');
        $teachers = ArrayHelper::map(Teacher::find()->all(), 'id', 'full_name');
        $rooms = ArrayHelper::map(Room::find()->all(), 'id', 'number');

        if(isset(Yii::$app->user->identity->id))Changing::setToChangeTable('schedule', \Yii::$app->user->identity->id, '', '', 'Зашел на вкладку Расписание');
        return $this->render('index', [
            'time' => $time,
            'days' => $days,
            'date' => $date,
            'groups' => $groups,
            'teachers' => $teachers,
            'rooms' => $rooms,
        ]);
    }

    /**
     * Копирование расписания
     */
    public function actionCopy()
    {
        /**
         * TODO: Добавить проверку существования базовых периодов и базового расписания
         */
        $lastPeriod = Date::find()->orderBy('week_start DESC')->one();
        $previousPeriod = Date::find()->orderBy('week_start DESC')->all()[1];

        // Прибавляем неделю к самому последнему периоду
        $date = new DateTime($lastPeriod->week_start);
        $startWeek = $date->modify('+1 week');
        $date = new DateTime($lastPeriod->week_start);
        $endWeek = $date->modify('+12 days');

        // Создаем новый период, начало четной недели
        $periodEven = new Date();
        $periodEven->week_start = $startWeek->format('Y-m-d');
        $periodEven->week_end = $endWeek->format('Y-m-d');
        $periodEven->created_at = date('Y-m-d');
        $periodEven->updated_at = date('Y-m-d');
        $periodEven->save(false);

        // Прибавляем 3 недели к предпоследнему периоду
        $date = new DateTime($previousPeriod->week_start);
        $startWeek = $date->modify('+3 weeks');
        $date = new DateTime($previousPeriod->week_start);
        $endWeek = $date->modify('+26 days');

        // Создаем новый период, начало нечетной недели
        $periodNotEven = new Date();
        $periodNotEven->week_start = $startWeek->format('Y-m-d');
        $periodNotEven->week_end = $endWeek->format('Y-m-d');
        $periodNotEven->created_at = date('Y-m-d');
        $periodNotEven->updated_at = date('Y-m-d');
        $periodNotEven->save(false);

        // Копируем расписание на следующие 2 недели

        // на четную, на каждый день недели
        $weekSchedule = Schedule::find()->where(['date_id' => $previousPeriod->id])->all();

        foreach ($weekSchedule as $schedule) {
            //        return var_dump($previousPeriod->id);
            $evenSchedule = new Schedule();
            //        return var_dump($schedule->time_id);
            $evenSchedule->time_id = $schedule->time_id;
            $evenSchedule->day_id = $schedule->day_id;
            $evenSchedule->date_id = $periodEven->id;
            $evenSchedule->room_id = $schedule->room_id;
            $evenSchedule->group_id = $schedule->group_id;
            $evenSchedule->teacher_id = $schedule->teacher_id;
            $evenSchedule->subject = $schedule->subject;
            $evenSchedule->save(false);
        }

        // на нечетную, на каждый день недели
        $weekSchedule = Schedule::find()->where(['date_id' => $lastPeriod->id])->all();

        foreach ($weekSchedule as $schedule) {
            $notEvenSchedule = new Schedule();
            $notEvenSchedule->time_id = $schedule->time_id;
            $notEvenSchedule->day_id = $schedule->day_id;
            $notEvenSchedule->date_id = $periodNotEven->id;
            $notEvenSchedule->room_id = $schedule->room_id;
            $notEvenSchedule->group_id = $schedule->group_id;
            $notEvenSchedule->teacher_id = $schedule->teacher_id;
            $notEvenSchedule->subject = $schedule->subject;
            $notEvenSchedule->save(false);
        }
        Changing::setToChangeTable('schedule', \Yii::$app->user->identity->id, '', '', 'Скопировал расписание');
        return $this->redirect('/schedule/index');
    }
}
