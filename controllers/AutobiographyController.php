<?php

namespace app\controllers;

use Yii;
use app\models\Autobiography;
use app\models\AutobiographySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Changing;

/**
 * AutobiographyController implements the CRUD actions for Autobiography model.
 */
class AutobiographyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Autobiography models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AutobiographySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Changing::setToChangeTable('autobiography', \Yii::$app->user->identity->id, '', '', 'Зашел на вкладку Автобиография');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'student' => null,
        ]);
    }

    /**
     * Displays a single Autobiography model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        Changing::setToChangeTable('autobiography', \Yii::$app->user->identity->id, '', '', 'Посмотрел данные автобиографии под номером №' . $id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Autobiography model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Autobiography();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $model->attachStudent();
            Changing::setToChangeTable('autobiography', $model->id, '', '', 'Добавил автобиографию под номером №' . $model->id);

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'tabs' => false,
            ]);
        }
    }

    /**
     * Updates an existing Autobiography model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $student = Yii::$app->request->get('student');
        $old = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($student != null) {
                return $this->redirect(['/portfolio/index?student=' . $student]);
//                return $this->goBack();
            }
            $model->setChanging($old, $model);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'tabs' => false,
            ]);
        }
    }

    /**
     * Deletes an existing Autobiography model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Changing::setToChangeTable('autobiography', \Yii::$app->user->identity->id, '', '', 'Удалил автобиографию под номером №' . $id);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Autobiography model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Autobiography the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Autobiography::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Одобрить портфолио студента
     *
     */
    public function actionApprove()
    {
        $id = Yii::$app->request->get('id');
        $student = Yii::$app->request->get('student');

        $this->findModel($id)->approve();

        return $this->redirect('/portfolio/index?student=' . $student);
    }
}
