<?php

namespace app\controllers;

use Yii;
use app\models\AdditionalEducation;
use app\models\AdditionalEducationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use app\models\Changing;

/**
 * AdditionalEducationController implements the CRUD actions for AdditionalEducation model.
 */
class AdditionalEducationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AdditionalEducation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdditionalEducationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Changing::setToChangeTable('additional-education', \Yii::$app->user->identity->id, '', '', 'Зашел на вкладку Дополнительное образование');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'student' => null,
        ]);
    }

    /**
     * Displays a single AdditionalEducation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        Changing::setToChangeTable('additional-education', \Yii::$app->user->identity->id, '', '', 'Посмотрел данные дополнительного образования под номером №' . $id);

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AdditionalEducation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AdditionalEducation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->file != null)
                $model->upload();

            $model->attachStudent();
            Changing::setToChangeTable('additional-education', $model->id, '', '', 'Добавил дополнительного образования под номером №' . $model->id);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AdditionalEducation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /**
         *  TODO: Добавить проверку для студента, если одобрено админом
         *  TODO: не должно быть доступа к странице
         */
        $model = $this->findModel($id);
        $student = Yii::$app->request->get('student');
        $old = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($student != null) {
                return $this->redirect(['/portfolio/index?student=' . $student]);
//                return $this->goBack();
            }
            $model->setChanging($old, $model);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AdditionalEducation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Changing::setToChangeTable('additional-education', \Yii::$app->user->identity->id, '', '', 'Удалил дополнительную образованию под номером №' . $id);

        return $this->redirect(['index']);
    }

    /**
     * Finds the AdditionalEducation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdditionalEducation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AdditionalEducation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Одобрить портфолио студента
     *
     */
    public function actionApprove()
    {
        $id = Yii::$app->request->get('id');
        $student = Yii::$app->request->get('student');

        $this->findModel($id)->approve();

        return $this->redirect('/portfolio/index?student=' . $student);
    }
}
