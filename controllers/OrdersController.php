<?php

namespace app\controllers;

use Yii;
use app\models\Orders;
use app\models\OrdersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Books;
use app\models\Changing;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $session = Yii::$app->session;
        $session['params'] = Yii::$app->request->queryParams;
        Changing::setToChangeTable('orders', \Yii::$app->user->identity->id, '', '', 'Зашел на вкладку Заказы');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPrint()
    {
        $session = Yii::$app->session;
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search($session['params']);

        $users = [];
        foreach ($dataProvider->getModels() as $model) {
            $users [] = $model->user->id;
        }
        /*echo "<pre>";
        print_r(array_unique($users));
        echo "</pre>";
        die;*/
        $xmlMarkup = $this->renderPartial('@app/views/xml/xml_all_orders', [
            'dataProvider' => $dataProvider,
            'users' => array_unique($users),
        ]);


        $xmlTmp = tmpfile();
        $fileInfo = stream_get_meta_data($xmlTmp);
        fwrite($xmlTmp, $xmlMarkup);
        Yii::$app->response->sendFile($fileInfo['uri'], "Заказы" . '_' . date('d.m.Y H:i:s').'.xml');
        fclose($xmlTmp);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        Changing::setToChangeTable('orders', \Yii::$app->user->identity->id, '', '', 'Посмотрел данные заказа под номером №' . $id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionLoad($id)
    {
        $model = $this->findModel($id);
        if($model == null) throw new NotFoundHttpException('Заказ не найдено');

        $xmlMarkup = $this->renderPartial('@app/views/xml/xml_order', [
            'model' => $model,
        ]);

        $xmlTmp = tmpfile();
        $fileInfo = stream_get_meta_data($xmlTmp);
        fwrite($xmlTmp, $xmlMarkup);
        Yii::$app->response->sendFile($fileInfo['uri'], "Заказ № " . $model->id.'_'.date('d.m.Y H:i:s').'.xml');
        fclose($xmlTmp);
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Orders();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $text = '
            <table border=1>
                <tr>
                    <th>ФИО</th>
                    <th>Номер зачетки\курсантского</th>
                    <th>Код книги</th>
                    <th>Автор</th>
                    <th>Заглавие</th>
                    <th>Место издания</th>
                    <th>Издательство</th>
                    <th>Год издание</th>
                    <th>Фонд библиотеки</th>
                    <th>Планируемая дата получения</th>
                </tr>
                <tr>
                    <td>'.$model->user->name.'</td>
                    <td>'.$model->record_book.'</td>
                    <td>'.$model->book->id.'</td>
                    <td>'.$model->book->author.'</td>
                    <td>'.$model->book->title.'</td>
                    <td>'.$model->book->publishing_address.'</td>
                    <td>'.$model->book->publishing_name.'</td>
                    <td>'.$model->book->publishing_year.'</td>
                    <td>'.$model->libraryFond->name.'</td>
                    <td>'.($model->planing_date != null ? \Yii::$app->formatter->asDate($model->planing_date, 'php:d.m.Y') : '' ).'</td>
                </tr>        
            </table>';

            $message = \Yii::$app->mailer->compose()
                ->setFrom('kimrtaccaount@gmail.com')
                ->setTo('support@afvgavt.ru')
                ->setSubject("Вам поступил заказ")
                ->setHtmlBody('Уважаемый '.$model->user->name.'!<br>'.$text);

            try{
                    Yii::$app->mailer->send($message);
               } catch(\Swift_TransportException $e) {
                    Yii::$app->session->setFlash('warning', 'Сообщение не был отправлен. Ошибка: '.$e->getMessage());
               } catch(\Exception $e){
                    Yii::$app->session->setFlash('error', 'Сообщение не был отправлен. Ошибка: '.$e->getMessage());
               }
            \Yii::$app->db->createCommand()->update('books', ['count' => new \yii\db\Expression('count - 1')], [ 'id' => $model->book_id ])->execute();
            \Yii::$app->db->createCommand()->update('foundations_book', ['quantity' => new \yii\db\Expression('quantity - 1')], [ 'book_id' => $model->book_id ])->execute();
            Changing::setToChangeTable('orders', \Yii::$app->user->identity->id, '', '', 'Добавил заказа под номером №' . $model->id);
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_model = $this->findModel($id);
        if($model->planing_date != null) $model->planing_date = \Yii::$app->formatter->asDate($model->planing_date, 'php:d.m.Y');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->setChanging($old_model, $model);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionBookList($id)
    {
        $books = Books::find()->where(['library_fond' => $id])->all();
        foreach($books as $data){
            echo "<option value = '" . $data->id . "'>Автор => ".  $data->author . "; Заголовок => " . $data->title . '; Количество =>' . $data->count . "шт. </option>" ;
        }        
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Changing::setToChangeTable('orders', \Yii::$app->user->identity->id, '', '', 'Удалил заказа под номером №' . $id);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
