<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Changing;
use app\models\Users;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionMymenu($id)
    {
        $menu = isset($_SESSION['menu']) ? $_SESSION['menu'] : null;

        if($menu == null)
            $_SESSION['menu'] = 'large';
        else {
            if($menu == 'large') $_SESSION['menu'] = 'small';
            else $_SESSION['menu'] = 'large';
        }
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect(['site/index']);
    }
    
    

    public function actionAvtorizatsiya()
    {
        if(isset(Yii::$app->user->identity->id)){
            return $this->render('error');
        }
        else{
            Yii::$app->user->logout();
            $this->redirect(['login']);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $user = Users::findOne(Yii::$app->user->id);
            if (!$user->passwd) {
                $user->passwd = $model->password;
                $user->save();    
            }
            
            Changing::setToChangeTable('login', \Yii::$app->user->identity->id, '', '', \Yii::$app->user->identity->name . ' зашел в систему');
            return $this->goHome();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Changing::setToChangeTable('login', \Yii::$app->user->identity->id, '', '', \Yii::$app->user->identity->name . ' вышел из системы');
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /*
     * Смена локали
     */
    public function actionChangeLocale()
    {
        $locale = Yii::$app->request->get('locale');
        Yii::$app->session->set('language', $locale);

        $locale = Yii::$app->session->get('language');
        if ($locale != null)
            Yii::$app->language = $locale;

        return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
    }

    public function actionPrintChanging($table)
    {
        \moonland\phpexcel\Excel::export([
            'models' => $table == 'all' ? Changing::find()->all() : Changing::find()->where(['table_name' => $table])->all(),
            'mode' => 'export',
            'fileName' => 'protokol_' . date('d.m.Y') . '.xlsx',
            'columns' => [
                [
                    'attribute' => 'date_time',
                    'value' => function($model) {
                        return \Yii::$app->formatter->asDate($model->date_time, 'php:H:i d.m.Y');
                    },
                ],
                [
                    'attribute' => 'user_id',
                    'value' => function($model) {
                        $user = Users::findOne($model->user_id);
                        return $user->name;
                    },
                ],
                'field',
                'new_value',
                'old_value',
            ],
        ]);
    }

    public function actionDeleteChanging($table)
    {
        if($table == 'all')
        {
            $changings = Changing::find()->all();
            foreach ($changings as $value) {
                $value->delete();
            }
            return $this->redirect([ '/site/all-changing-view']);
        }
        else 
        {
            $changings = Changing::find()->where(['table_name' => $table])->all();
            foreach ($changings as $value) {
                $value->delete();
            }
            return $this->redirect([ '/' . $table . '/index']);
        }
    }

    public function actionViewChanging($table,$title)
    {
        $query = Changing::find()->where(['table_name' => $table]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'pagination' => [ 'pageSize' => 10 ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $changings = Changing::find()->where(['table_name' => $table])->all();
        return $this->render('view-changing', [
            'changings' => $changings,
            'dataProvider' => $dataProvider,
            'url' => '/' . $table . '/index',
            'title' => $title,
        ]);
        /*$request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title'=> "Просмотр",
            'size' => 'large',
            'content'=>$this->renderAjax('view-changing', [
                'changings' => $changings,
                'dataProvider' => $dataProvider,
            ]),
        ];*/
    }

    public function actionAllChangingView()
    {
        $query = Changing::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'pagination' => [ 'pageSize' => 10 ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        Changing::setToChangeTable('log', \Yii::$app->user->identity->id, '', '', 'Зашел на вкладку Логи');
        return $this->render('view-all-changes', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
