<?php

namespace app\controllers;

use app\models\Date;
use app\models\Group;
use app\models\Room;
use app\models\Teacher;
use app\models\Time;
use Yii;
use app\models\Schedule;
use app\models\WeekDay;
use app\models\ScheduleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use app\models\Changing;

/**
 * ScheduleCrudController implements the CRUD actions for Schedule model.
 */
class ScheduleCrudController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Schedule models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity->type == 'student')
            return $this->redirect('/portfolio/index');

        $searchModel = new ScheduleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Changing::setToChangeTable('schedule', \Yii::$app->user->identity->id, '', '', 'Зашел на вкладку Расписание');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Schedule model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        Changing::setToChangeTable('schedule', \Yii::$app->user->identity->id, '', '', 'Посмотрел данные расписания под номером №' . $id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Schedule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Schedule();

        $weekDays = WeekDay::getDaysArray();
        $time = Time::getTimeArray();
        $teachers = Teacher::getTeachersArray();
        $startDate = Date::getDateList();
        $groups = Group::getGroupsArray();
        $rooms = Room::getRoomsArray();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Changing::setToChangeTable('schedule', \Yii::$app->user->identity->id, '', '', 'Добавил расписанию под номером №' . $model->id);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'weekDays' => $weekDays,
                'time' => $time,
                'teachers' => $teachers,
                'startDate' => $startDate,
                'groups' => $groups,
                'rooms' => $rooms,
            ]);
        }
    }

    /**
     * Updates an existing Schedule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_model = $this->findModel($id);

        $weekDays = WeekDay::getDaysArray();
        $time = Time::getTimeArray();
        $teachers = Teacher::getTeachersArray();
        $startDate = Date::getDateList();
        $groups = Group::getGroupsArray();
        $rooms = Room::getRoomsArray();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->is_changed = true;
            $model->save(false);
            $model->setChanging($old_model, $model);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'weekDays' => $weekDays,
                'time' => $time,
                'teachers' => $teachers,
                'startDate' => $startDate,
                'groups' => $groups,
                'rooms' => $rooms,
            ]);
        }
    }

    /**
     * Deletes an existing Schedule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Changing::setToChangeTable('schedule', \Yii::$app->user->identity->id, '', '', 'Удалил расписанию под номером №' . $id);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Schedule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Schedule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Schedule::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
