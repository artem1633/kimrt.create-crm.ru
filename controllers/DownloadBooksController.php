<?php

namespace app\controllers;

use Yii;
use app\models\DownloadBooks;
use app\models\DownloadBooksSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Uploadxml;
use yii\web\UploadedFile;
use app\models\Users;
use app\models\Foundation;
use app\models\Books;
use app\models\Student;
use app\models\FoundationsBook;
use app\models\Changing;

/**
 * DownloadBooksController implements the CRUD actions for DownloadBooks model.
 */
class DownloadBooksController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DownloadBooks models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DownloadBooksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new Uploadxml();
        Changing::setToChangeTable('download-books', \Yii::$app->user->identity->id, '', '', 'Зашел на вкладку Электронный формуляр');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    public function actionImport()
    {
        $model = new Uploadxml();

        if (Yii::$app->request->isPost && Yii::$app->user->identity->type == "admin") {
            try {
                $model->file = UploadedFile::getInstance($model, 'file');

                $fileName = $model->file->baseName . '.' . $model->file->extension;
                if ($model->file && $model->validate()) {
                    $model->file->saveAs('xml/' . $fileName);
                }

                $filePath = 'xml/' . $fileName;
                $file = file_get_contents($filePath);
                $xml = simplexml_load_string($file);

                if($xml != null){

                    foreach ($xml as $talaba) {                      

                        $student = Student::find()->where([ 'students_number' => (string)$talaba['ID_Student'] ])->one();
                        if($student == null) return $this->redirect(['index']);                    

                        foreach ($talaba as $value) {
                            $t = (string)$value['return_date'];
                            $return_data = substr( $t, 0, 4  ) . '-' . substr( $t, 4, 2  ) . '-'. substr( $t, 6, 2  );
                                
                            $fond = Foundation::find()->where(['name' => (string)$value['Library_Foundation'] ])->one();
                            if($fond == null){
                                $fond = new Foundation();
                                $fond->name = (string)$value['Library_Foundation'];
                                $fond->save();
                            }

                            $book = FoundationsBook::find()->where( [ 'id_book' => ((string)$value['ID_Book']) ])->one();
                            if($book == null) continue;

                            $download_book = DownloadBooks::find()->where(['user_id' => $student->user->id, 'book_id' => $book->number, 'foundation_id' => $fond->id, 'return_date' => $return_data ])->one();
                            if($download_book == null){
                                $download_book = new DownloadBooks();
                                $download_book->user_id = $student->user->id;
                                $download_book->book_id = $book->number;
                                $download_book->foundation_id = $fond->id;
                                $download_book->return_date = $return_data;
                                $download_book->save();
                            }
                              
                        }                   
                    }
                }
                
                Changing::setToChangeTable('download-books', \Yii::$app->user->identity->id, '', '', 'Импортировал Электронного формуляра');                
                return $this->redirect(['index']);
            }
            catch (Exception $exception) {
                return $exception;
            }
        }
    }

    /**
     * Displays a single DownloadBooks model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        Changing::setToChangeTable('download-books', \Yii::$app->user->identity->id, '', '', 'Посмотрел данные электронного формуляра под номером №' . $id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DownloadBooks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DownloadBooks();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Changing::setToChangeTable('download-books', \Yii::$app->user->identity->id, '', '', 'Добавил электронного формуляра под номером №' . $model->id);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DownloadBooks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DownloadBooks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Changing::setToChangeTable('download-books', \Yii::$app->user->identity->id, '', '', 'Удалил электронного формуляра под номером №' . $id);
        return $this->redirect(['index']);
    }

    /**
     * Finds the DownloadBooks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DownloadBooks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DownloadBooks::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
