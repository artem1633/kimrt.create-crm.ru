<?php

namespace app\controllers;

use Yii;
use app\models\Contest;
use app\models\ContestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use app\models\Changing;

/**
 * ContestController implements the CRUD actions for Contest model.
 */
class ContestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Changing::setToChangeTable('contest', \Yii::$app->user->identity->id, '', '', 'Зашел на вкладку Конкурсы проектов');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'student' => null,
        ]);
    }

    /**
     * Displays a single Contest model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        Changing::setToChangeTable('contest', \Yii::$app->user->identity->id, '', '', $id . 'Посмотрел данные конкурсного проекта под номером №' . $id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Contest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contest();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->file != null)
                $model->upload('work');

            $model->file = UploadedFile::getInstance($model, 'fileDocument');

            if ($model->file != null)
                $model->upload('document');

            $model->attachStudent();
            Changing::setToChangeTable('contest', $model->id, '', '', 'Добавил конкурсного проекта под номером №' . $model->id);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Contest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $student = Yii::$app->request->get('student');
        $old = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($student != null) {
                return $this->redirect(['/portfolio/index?student=' . $student]);
//                return $this->goBack();
            }
        $model->setChanging($old, $model);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Contest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Changing::setToChangeTable('contest', \Yii::$app->user->identity->id, '', '', 'Удалил конкурсы проектов под номером №' . $id);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Contest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Одобрить портфолио студента
     *
     */
    public function actionApprove()
    {
        $id = Yii::$app->request->get('id');
        $student = Yii::$app->request->get('student');

        $this->findModel($id)->approve();

        return $this->redirect('/portfolio/index?student=' . $student);
    }
}
