<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Specialty */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Специальности', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="specialty-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
