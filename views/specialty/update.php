<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Specialty */

$this->title = 'Изменить';
$this->params['breadcrumbs'][] = ['label' => 'Специальности', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="specialty-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
