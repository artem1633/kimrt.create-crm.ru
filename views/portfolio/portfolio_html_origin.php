<!DOCTYPE>
<html>

<head>
    <link rel="stylesheet" href="../../web/css/my.css">
</head>


<p class="header" align="center">Каспийский институт морского и речного транспорта</p>
<p class="header" align="center">филиал Федерального государственного бюджетного образовательного</p>
<p class="header" align="center">учреждения высшего образования</p>
<p class="header" align="center">«Волжский государственный университет водного транспорта»</p>
<p class="header" align="center">(ФГБОУ ВО «ВГУВТ»)</p>

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>


<p class="header" align="center">ПОРТФОЛИО ОБУЧАЮЩЕГОСЯ</p><br>
<p class="header" align="center">
    <?= $student->surname . ' ' . $student->patronymic . ' ' . $student->name ?>
</p><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

<p class="header" align="center">Астрахань 2017 г.</p>
<br style="page-break-before: always">
<br>
<br>
<br>
<p class="header" align="center">Общие сведения</p><br>

<table width="65%" height="40%" align="center" border="0">
    <tr>
        <td class="special" rowspan="8" width="40%">
            <p align="center">Фото</p>
        </td>
        <td width="14%">
            <br>
        </td>
        <td>
            <p>
                <?= $student->surname ?>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <br>
        </td>
        <td>
            <p>
                <?= $student->name ?>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <br>
        </td>
        <td>
            <p>
                <?= $student->patronymic ?>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <br>
        </td>
        <td>
            <?= $student->date_birth ?>
        </td>
    </tr>
    <tr>
        <td>
            <br>
        </td>
        <td>
            <?= $student->level ?>
        </td>
    </tr>
    <tr>
        <td>
            <br>
        </td>
        <td>
            <?php if ($student->specialty != null) { ?>
                <?= $student->specialty->name ?>
            <?php } ?>
        </td>
    </tr>
    <tr>
        <td>
            <br>
        </td>
        <td>
            <p>
                <?= $student->enrollment_date ?>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <br>
        </td>
        <td>
            <p>
                <?php if ($student->course != null) { ?>
                    <?= $student->course->name ?>
                <?php } ?>

                <?php if ($student->group != null) { ?>
                    <?= $student->group->name ?>
                <?php } ?>
            </p>
        </td>
    </tr>
</table>




<br>

<table width="90%" align="center" border="0">
    <tr>
        <td align="center">
            <p class="header" align="center">Автобиография</p><br>
        </td>
    </tr>
    <tr>
        <td align="justify">
            <p>
                <?= $authobiography ?>
            </p>
        </td>
    </tr>
</table>



<br>

<p class="header" align="center">Дополнительная информация</p><br><br>
<p class="header" align="center">1. Сведения о курсовых и дипломных работах</p>
<br>
<table class="portfolio" width="90%" align="center" border="1">
    <tr>
        <td align="center">№ п/п</td>
        <td align="center">Дисциплина</td>
        <td align="center">Тема работы</td>
        <td align="center">Оценка</td>
    </tr>

    <?php
    foreach ($works as $work) {
        ?>
        <tr>
            <td></td>

            <?php
            if ($work->subject!=null) {
                ?>

                <td align="center"> <?= $work->subject->name ?> </td>

                <?php
            }
            ?>

            <td align="center"> <?= $work->theme ?> </td>
            <td>
            </td>
        </tr>
        <?php
    }
    ?>
</table>

<br>

<p class="header" align="center">2. Участие в предметных олимпиадах</p>
<br>
<table class="portfolio" width="90%" align="center" border="1">
    <tr>
        <td align="center">№ п/п</td>
        <td align="center">Название олимпиады</td>
        <td align="center">Уровень</td>
        <td align="center">Место и время проведения</td>
        <td align="center">Результат</td>
    </tr>

    <?php
    foreach ($olympiads as $olympiad) {
        ?>
        <tr>
            <td></td>
            <td align="center">
                <?= $olympiad->name ?>
            </td>
            <td align="center">
                <?= $olympiad->level ?>
            </td>
            <td align="center">
                <?= $olympiad->location . ' ' . $olympiad->time ?>
            </td>
            <td align="center">
                <?= $olympiad->result ?>
            </td>
        </tr>
        <?php
    }
    ?>

</table>

<br>

<p class="header" align="center">3. Освоение дополнительных образовательных программ</p>
<br>
<table class="portfolio" width="90%" align="center" border="1">
    <tr>
        <td align="center">№ п/п</td>
        <td align="center">Название программы</td>
        <td align="center">Количество часов</td>
        <td align="center">Место и время обучения</td>
        <td align="center">Название документа</td>
    </tr>

    <?php
    foreach ($additionalEducation as $a) {
        ?>
        <tr>
            <td></td>
            <td align="center"><?= $a->name ?></td>
            <td align="center"><?= $a->hours ?></td>
            <td align="center"><?= $a->location_time ?></td>
            <td align="center"><?= $a->document_name ?></td>
        </tr>
        <?php
    }
    ?>
</table>
<br>

<p class="header" align="center">4. Участие в научно-практических конференциях</p>
<br>
<table class="portfolio" width="90%" align="center" border="1">
    <tr>
        <td align="center">№ п/п</td>
        <td align="center">Название конференции</td>
        <td align="center">Уровень</td>
        <td align="center">Дата конференции</td>
        <td align="center">Тема выступления</td>
        <td align="center">Наличие публикации</td>
    </tr>

    <?php
    foreach ($scientificProjects as $s) {
        ?>
        <tr>
            <td align="center">  </td>
            <td align="center"><?= $s->name ?></td>
            <td align="center"><?= $s->level ?></td>
            <td align="center"><?= $s->date ?></td>
            <td align="center"><?= $s->theme ?></td>
            <td align="center"><?= $s->publication ?></td>
        </tr>
        <?php
    }
    ?>
</table>
<br>

<p class="header" align="center">5. Участие в конкурсах проектов</p>
<br>
<table class="portfolio" width="90%" align="center" border="1">
    <tr>
        <td align="center">№ п/п</td>
        <td align="center">Название конкурса</td>
        <td align="center">Уровень</td>
        <td align="center">Дата проведения</td>
        <td align="center">Тема работы</td>
        <td align="center">Результат</td>
    </tr>

    <?php
    foreach ($contests as $contest) {
        ?>
        <tr>
            <td align="center"></td>
            <td align="center"><?= $contest->name ?></td>
            <td align="center"><?= $contest->level ?></td>
            <td align="center"><?= $contest->date ?></td>
            <td align="center"><?= $contest->theme ?></td>
            <td align="center"><?= $contest->result ?></td>
        </tr>
        <?php
    }
    ?>
</table>
<br>

<p class="header" align="center">6. Участие в работе органов студенческого самоуправления и молодежных общественных объединениях</p>
<br>
<table class="portfolio" width="90%" align="center" border="1">
    <tr>
        <td align="center">№ п/п</td>
        <td align="center">Название мероприятия</td>
        <td align="center">Уровень</td>
        <td align="center">Период проведения</td>
        <td align="center">Форма участия</td>
        <td align="center">Результат</td>
    </tr>

    <?php
    foreach ($extracurricular as $e) {
        ?>
        <tr>
            <td ></td>
            <td align="center"><?= $e->name ?></td>
            <td align="center"><?= $e->level ?></td>
            <td align="center"><?= $e->date_period ?></td>
            <td align="center"><?= $e->partisipation ?></td>
            <td align="center"> Нет в crm </td>
        </tr>
        <?php
    }
    ?>
</table>
<br>

<p class="header" align="center">7. Спортивные достижения</p>
<br>
<table class="portfolio" width="90%" align="center" border="1">
    <tr>
        <td align="center">№ п/п</td>
        <td align="center">Вид спорта</td>
        <td align="center">Уровень</td>
        <td align="center">Название соревнований</td>
        <td align="center">Дата соревнований</td>
        <td align="center">Результат</td>
    </tr>

    <?php
    foreach ($sport as $s) {
    ?>
    <tr>
        <td ></td>
        <td><?= $s->sport ?></td>
        <td><?= $s->level ?></td>
        <td><?= $s->competition ?></td>
        <td><?= $s->date ?></td>
        <td><?= $s->result ?></td>
    </tr>
    <?php
    }
    ?>
</table>

<br>

<p class="header" align="center">8. Творческие достижения</p>
<br>
<table class="portfolio" width="90%" align="center" border="1">
    <tr>
        <td align="center">№ п/п</td>
        <td align="center">Вид деятельности</td>
        <td align="center">Уровень</td>
        <td align="center">Название конкурса, фестиваля</td>
        <td align="center">Дата </td>
        <td align="center">Результат</td>
    </tr>

    <?php
    foreach ($art as $a) {
    ?>
        <tr>
            <td></td>
            <td align="center"><?= $a->type ?></td>
            <td align="center"><?= $a->level ?></td>
            <td align="center"><?= $a->competition_name ?></td>
            <td align="center"><?= $a->date ?></td>
            <td align="center"><?= $a->result ?></td>
        </tr>
    <?php
    }
    ?>
</table>
</html>