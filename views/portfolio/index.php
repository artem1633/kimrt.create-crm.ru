

<?php
use yii\bootstrap\Tabs;
use yii\helpers\Html;

echo ' <div class="box box-default">
    <div class="box-body">';

$this->title = 'Портфолио';
echo Html::a('Выгрузить в pdf', ['/portfolio/export-pdf?student=' . $student], ['class'=>'btn btn-success', 'target' => '_blank']);
echo '</div> </div>';


echo Tabs::widget([
    'items' => [
        [
            'label'     =>  'Автобиография',
            'content'   =>  $this->render('/autobiography/index', [
                'searchModel' => $autobiographySearch,
                'dataProvider' => $dataProviderAutography,
                'student' => $student,
            ]),
            'active'    =>  true
        ],
        [
            'label'     => 'Курсовые и дипломные работы',
            'content'   =>  $this->render('/work/index', [
                'searchModel' => $workSearch,
                'dataProvider' => $dataProviderWork,
                'student' => $student,
            ]),
            'headerOptions' => ['class' => 'nav nav-tabs'],
        ],
        [
            'label'     => 'Предметные олимпиады',
            'content'   =>  $this->render('/olympiad/index', [
                'searchModel' => $olympiadSearch,
                'dataProvider' => $dataProviderOlympiad,
                'student' => $student,
            ]),
            'headerOptions' => ['class' => 'nav nav-tabs'],
        ],
        [
            'label'     => 'Дополнительное образование',
            'content'   =>  $this->render('/additional-education/index', [
                'searchModel' => $educationSearch,
                'dataProvider' => $dataProviderEducation,
                'student' => $student,
            ]),
            'headerOptions' => ['class' => 'nav nav-tabs'],
        ],
        [
            'label'     => 'Научные проекты',
            'content'   =>  $this->render('/scientific-projects/index', [
                'searchModel' => $searchModelScience,
                'dataProvider' => $dataProviderScience,
                'student' => $student,
            ]),
            'headerOptions' => ['class' => 'nav nav-tabs'],
        ],
        [
            'label'     => 'Конкурсы проектов',
            'content'   =>  $this->render('/contest/index', [
                'searchModel' => $searchModelContest,
                'dataProvider' => $dataProviderContest,
                'student' => $student,
            ]),
            'headerOptions' => ['class' => 'nav nav-tabs'],
        ],
        [
            'label'     => 'Внеучебная деятельность',
            'content'   =>  $this->render('/extracurricular/index', [
                'searchModel' => $searchModelExtracurricular,
                'dataProvider' => $dataProviderExtracurricular,
                'student' => $student,
            ]),
            'headerOptions' => ['class' => 'nav nav-tabs'],
        ],
        [
            'label'     => 'Спортивные достижения',
            'content'   =>  $this->render('/sport/index', [
                'searchModel' => $searchModelSport,
                'dataProvider' => $dataProviderSport,
                'student' => $student,
            ]),
            'headerOptions' => ['class' => 'nav nav-tabs'],
        ],
        [
            'label'     => 'Творческие достижения',
            'content'   =>  $this->render('/art/index', [
                'searchModel' => $searchModelArt,
                'dataProvider' => $dataProviderArt,
                'student' => $student,
            ]),
            'headerOptions' => ['class' => 'nav nav-tabs'],
        ],
        [
            'label'     => 'Успеваемость',
            'content'   =>  $this->render('/academic-perfomance/index', [
                'searchModel' => $searchModelAP,
                'dataProvider' => $dataProviderAP,
                'uploadModel' => $uploadModel,
                'student' => $student,
            ]),
            'headerOptions' => ['class' => 'nav nav-tabs'],
        ],
    ]
]);

?>

</div>
</div>
