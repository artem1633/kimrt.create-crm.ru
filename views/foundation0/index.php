<?php

use app\models\Books;
use app\models\BooksSections;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фонды библиотек';
$this->params['breadcrumbs'][] = $this->title;

$booksSections = BooksSections::find()->asArray()->all();
$models = Books::find()->asArray()->all();

$datasetNames = array_values(ArrayHelper::map($booksSections, 'id', 'name'));
$datasetPublishings = array_unique(array_values(ArrayHelper::map($models, 'id', 'publishing_name')));
$datasetTitles = array_values(ArrayHelper::map($models, 'id', 'title'));
$datasetAuthor = array_values(ArrayHelper::map($models, 'id', 'author'));
$datasetPublishingsAddresses = array_values(ArrayHelper::map($models, 'id', 'publishing_address'));
$datasetPublishingYears = array_values(ArrayHelper::map($models, 'id', 'publishing_year'));
$datasetBBK = array_values(ArrayHelper::map($models, 'id', 'bbk'));

?>
<div class="books-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('<i class="fa fa-plus"></i> Добавить', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'quantity',
                ],
                [
                    'attribute' => 'description',
                ],
                [
                    'attribute' => 'author',
                    'filterType' => GridView::FILTER_TYPEAHEAD,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['highlight' => true],
                        'dataset' => [[
                            'local' => $datasetAuthor,
                        ]],
                    ],
                ],
                [
                    'attribute' => 'title',
                    'filterType' => GridView::FILTER_TYPEAHEAD,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['highlight' => true],
                        'dataset' => [[
                            'local' => $datasetTitles,
                        ]],
                    ],
                ],
                [
                    'attribute' => 'publishing_address',
                    'filterType' => GridView::FILTER_TYPEAHEAD,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['highlight' => true],
                        'dataset' => [[
                            'local' => $datasetPublishingsAddresses,
                        ]],
                    ],
                ],
                [
                    'attribute' => 'publishing_name',
                    'filterType' => GridView::FILTER_TYPEAHEAD,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['highlight' => true],
                        'dataset' => [[
                            'local' => $datasetPublishings,
                        ]
                        ],
                    ],
                ],
                [
                    'attribute' => 'book_section_id',
                    'value' => 'bookSection.name',
                    'filterType' => GridView::FILTER_TYPEAHEAD,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['highlight' => true],
                        'dataset' => [[
                            'local' => $datasetNames,
                        ]
                        ],
                    ],
                ],
                [
                    'attribute' => 'publishing_year',
                    'hAlign' => GridView::ALIGN_CENTER,
                    'width' => '150px',
                    'filterType' => GridView::FILTER_TYPEAHEAD,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['highlight' => true],
                        'dataset' => [[
                            'local' => $datasetPublishingYears,
                        ]
                        ],
                    ],
                ],
                [
                    'attribute' => 'bbk',
                    'width' => '150px',
                    'hAlign' => GridView::ALIGN_CENTER,
                    'filterType' => GridView::FILTER_TYPEAHEAD,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['highlight' => true],
                        'dataset' => [[
                            'local' => $datasetBBK,
                        ]
                        ],
                    ],
                ],

            ],
        ]); ?>
    </div>
</div>
