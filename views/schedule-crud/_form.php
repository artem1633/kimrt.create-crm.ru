<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Schedule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="schedule-form">
    <div class="box box-default">
        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'date_id')->dropDownList($startDate,
                ['prompt'=>'Дата (начало недели)']) ?>

            <?= $form->field($model, 'day_id')->dropDownList($weekDays,
                ['prompt'=>'День недели']) ?>

            <?= $form->field($model, 'time_id')->dropDownList($time,
                ['prompt'=>'Время начала занятия']) ?>


            <?= $form->field($model, 'teacher_id')->dropDownList($teachers,
                ['prompt'=>'Преподаватель']) ?>

            <?= $form->field($model, 'room_id')->dropDownList($rooms,
                ['prompt'=>'Аудитория']) ?>

            <?= $form->field($model, 'group_id')->dropDownList($groups,
                ['prompt'=>'Группа']) ?>

            <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'created_at')->textInput(['type'=>'hidden', 'maxlength' => true, 'value' => date('Y-m-d')])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
