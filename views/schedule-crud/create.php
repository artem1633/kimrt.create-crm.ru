<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Schedule */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Расписание', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedule-create">

    <?= $this->render('_form', [
        'model' => $model,
        'weekDays' => $weekDays,
        'time' => $time,
        'teachers' => $teachers,
        'startDate' => $startDate,
        'groups' => $groups,
        'rooms' => $rooms,
    ]) ?>

</div>
