<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">
    <div class="box box-default">
        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'library_fond')->label()->widget(\kartik\select2\Select2::classname(), [
                        'data' => $model->getLibraryList(),
                        'options' => [
                            'placeholder' => 'Выберите фонда',
                            'onchange'=>'
                                $.post( "book-list?id='.'"+$(this).val(), function( data ){
                                    $( "select#orders-book_id" ).html( data);
                                });
                            ' 
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'user_id')->label()->widget(\kartik\select2\Select2::classname(), [
                        'data' => $model->getUsersList(),
                        'options' => [
                            'placeholder' => 'Выберите курсанта',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>                    
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'planing_date')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Выберите'],
                        'layout' => '{picker}{input}{remove}',
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy',
                            'startView'=>'year',
                            'todayHighlight' => true,
                        ]
                    ]) ?>
                </div>                
            </div>

            <div class="row">
                <div class="col-md-10">
                    <?= $form->field($model, 'book_id')->label()->widget(\kartik\select2\Select2::classname(), [
                        'data' => $model->getBookList(),
                        'options' => [
                            'placeholder' => 'Выберите книгу',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>                    
                </div>
                <div class="col-md-1">
                    <div class="form-group" style="margin-top: 25px;">
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
