<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = 'Изменить';
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="orders-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
