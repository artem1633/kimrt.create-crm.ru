<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">
    <div class="box box-default">
        <div class="box-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            
                <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
                <?= Html::a('Выгрузить', ['print'], ['class' => 'btn btn-default']) ?>
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-warning"> <b>Логирование </b></button>
                    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li> <?= Html::a( '<b>Просмотр</b>', ['/site/view-changing?table=orders&title='.'Заказы'], [/*'role'=>'modal-remote',*/])?></li>
                        <li> <?= Html::a( '<b>Выгрузка</b>', ['/site/print-changing?table=orders'], [])?></li>
                        <li> <?= Html::a( '<b>Очистка</b>', ['/site/delete-changing?table=orders'], [])?></li>
                    </ul>
                </div>
                <br>
                <br>            

            <div style="width: 100%; overflow: auto"> 
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    [
                        'attribute' => 'library_fond',
                        'content'=>function ($data){
                            return $data->libraryFond->name; 
                        },
                    ],
                    [
                        'attribute' => 'user_id',
                        'content'=>function ($data){
                            return $data->user->name; 
                        },
                    ],
                    'record_book',
                    [
                        'attribute' => 'book_id',
                        'label' => 'Код книги',
                        'content'=>function ($data){
                            return '№ ' . $data->book->id; 
                        },
                    ],
                    [
                        'attribute' => 'book_id',
                        'label' => 'Автор',
                        'content'=>function ($data){
                            return $data->book->author; 
                        },
                    ],
                    [
                        'attribute' => 'book_id',
                        'label' => 'Заглавие',
                        'content'=>function ($data){
                            return $data->book->title; 
                        },
                    ],
                    [
                        'attribute' => 'book_id',
                        'label' => 'Место издания',
                        'content'=>function ($data){
                            return $data->book->publishing_address; 
                        },
                    ],
                    [
                        'attribute' => 'book_id',
                        'label' => 'Издательство',
                        'content'=>function ($data){
                            return $data->book->publishing_name; 
                        },
                    ],
                    [
                        'attribute' => 'book_id',
                        'label' => 'Год издание',
                        'content'=>function ($data){
                            return $data->book->publishing_year; 
                        },
                    ],
                    [
                        'attribute' => 'planing_date',
                        'content'=>function ($data){
                            return \Yii::$app->formatter->asDate($data->planing_date, 'php:d.m.Y'); 
                        },
                    ],


                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'load' => function ($url, $model, $key){
                                return Html::a('<i class="fa fa-upload"></i>', $url);
                            },
                        ],
                    ],
                ],
            ]); ?>
            </div>
        </div>
    </div>
</div>
