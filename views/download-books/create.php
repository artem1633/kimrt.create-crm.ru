<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DownloadBooks */

$this->title = 'Create Download Books';
$this->params['breadcrumbs'][] = ['label' => 'Download Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="download-books-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
