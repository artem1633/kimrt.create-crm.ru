<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use app\models\Student;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DownloadBooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Электронный формуляр';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="box box-default">
        <div class="box-body">

        <?php
        if (Yii::$app->user->identity->type == "admin") {
            ?>
            <div class="box-header">
                <?php $form = ActiveForm::begin(['action'=>'/download-books/import', 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]) ?>

                <?= $form->field($model, 'file')->label(Yii::t('app', 'Загрузить .xml'),['class'=>'btn btn-primary'])
                    ->fileInput(['class'=>'sr-only']) ?>

                <button type="submit" class="btn btn-success btn-flat"><?= Yii::t('app', 'Загрузить') ?></button>

                <?php ActiveForm::end() ?>
            </div>
        <?php } ?>

        </div>
    </div>

    <div class="box box-default">
        <div class="box-body">
           <!--  <p>
               <?php // Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
           </p> -->

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'user_id',
                        'content' => function($data){
                            $student = Student::find()->where(['user_id' => $data->user_id ])->one();
                            if($student != null) return $student->students_number;
                        }
                    ],
                    /*[
                        'attribute' => 'user_id',
                        'content' => function($data){
                            $student = Student::find()->where([ 'user_id' => $data->user_id ])->one();
                            if($student != null) return $student->code;
                        }
                    ],*/
                    [
                        'attribute' => 'book_id',
                        'content' => function($data){
                            return $data->book->id_book;
                        }
                    ],
                    [
                        'attribute' => 'book_id',
                        'label' => 'Заголовок',
                        'content' => function($data){
                            return $data->book->title;
                        }
                    ],                
                    [
                        'attribute' => 'foundation_id',
                        'value' => 'foundation.name',
                    ],                    
                    [
                        'attribute' => 'return_date',
                        'content' => function($data){
                            return \Yii::$app->formatter->asDate($data->return_date, 'php:d.m.Y');
                        }
                    ],

                    ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}'],
                ],
            ]); ?>
        </div>
    </div>
