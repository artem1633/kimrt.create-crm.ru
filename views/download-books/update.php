<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DownloadBooks */

$this->title = 'Update Download Books: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Download Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="download-books-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
