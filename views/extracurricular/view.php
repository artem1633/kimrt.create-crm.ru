<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Extracurricular */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Внеучебное образование', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="extracurricular-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'level',
            'date_period',
            'partisipation',
            'document',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
