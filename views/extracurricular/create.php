<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Extracurricular */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Extracurriculars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="extracurricular-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
