<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Extracurricular */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="extracurricular-form">
    <div class="box box-default"">
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>

        <?php if (Yii::$app->user->identity->type == "admin") { ?>
            <?= $form->field($model, 'is_approved')->checkBox(['label' => 'Утверждено', 'selected' => $model->is_approved, 'disabled' => true]) ?>
        <?php } ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'level')->dropDownList([
            'Внутривузовский' => 'Внутривузовский',
            'Межвузовский' => 'Межвузовский',
            'Районный'=>'Районный',
            'Городской'=>'Городской',
            'Областной'=>'Областной',
            'Федеральный'=>'Федеральный',
            'Международный'=>'Международный',
        ]);
        ?>

        <?= $form->field($model, 'date_period')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'file')->label(Yii::t('app', 'Документ'))
            ->fileInput() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
</div>
