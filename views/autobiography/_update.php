<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Autobiography */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$options = [];
$form = ActiveForm::begin();

if ($model->is_approved && Yii::$app->user->identity->type != "admin") {

//    $options = [
//        'readOnly'=> true
//    ];
//
//    $textInput = $form->field($model, 'text')->textarea($options);

    $textInput = '<fieldset class="form-group field-autobiography-comment has-success">
                <legend>Автобиография</legend>

                <b>' .
                    $model->text .
                '</b>
            </fieldset>';
}
else {
    $textInput = $form->field($model, 'text')->widget(CKEditor::className(), ['preset' => 'basic']);
}

?>

<div class="documents-creditors-form">
    <div class="box box-default">
        <div class="box-body">

            <?= $textInput ?>

            <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?= $form->field($model, 'is_approved')->checkBox(['label' => 'Утверждено', 'selected' => $model->is_approved, 'disabled' => true]) ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
