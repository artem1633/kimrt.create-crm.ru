<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Autobiography */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$options = [];

if ($model->is_approved) {
    $options = [
        'readOnly'=> true
    ];
}
?>

<div class="documents-creditors-form">
    <div class="box box-default">
        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'text')->widget(CKEditor::className(), ['preset' => 'basic']) ?>

            <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
