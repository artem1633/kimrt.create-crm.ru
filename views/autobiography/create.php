
<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Autobiography */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Автобиография', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="autobiography-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
