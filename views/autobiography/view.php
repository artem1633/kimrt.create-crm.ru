<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Autobiography */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Автобиография', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autobiography-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Уверены, что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'user_id',
            'is_approved',
            'comment',
//            'created_at',
//            'updated_at',
        ],
    ]) ?>

</div>
