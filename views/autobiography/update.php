<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Autobiography */

$this->title = 'Изменить';
$this->params['breadcrumbs'][] = ['label' => 'Автобиография', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="autobiography-update">

    <?= $this->render('_update', [
        'model' => $model,
    ]) ?>

</div>
