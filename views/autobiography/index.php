<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AutobiographySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if (Yii::$app->user->identity->type == "student") {
    $this->title = 'Автобиография';
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="autobiography-index">
    <div class="box box-default"">
    <div class="box-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    if (Yii::$app->user->identity->type == "student") {
        ?>

    
        <?= Html::a('<i class="fa fa-plus"></i> Добавить', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    

    <?php
    }

    if (Yii::$app->user->identity->type == "admin") {
        $isVisible = true;
    }
    else {
        $isVisible = false;
    }
    ?>

    <?= GridView::widget([
        'caption' => 'Автобиография',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'user_id',
//            'is_approved',
            'text',

            [
                'header' => 'Button',
                'content' => function($model) use ($student) {
                    return Html::a('Одобрить', '/autobiography/approve?id=' . $model->id  . '&student=' . $student, ['class' => 'btn btn-success']);
                },
                'visible' => $isVisible,
            ],

//            'comment',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url, $model) {
//                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
//                                'title' => Yii::t('app', 'lead-view'),
//                            ]);
                        return false;
                    },

                    'update' => function ($url, $model) use ($student) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '/autobiography/update?id=' . $model->id . '&student=' . $student, [
                            'title' => Yii::t('app', 'lead-update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
</div>
</div>
