<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BooksSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-search">

    <?php $form = ActiveForm::begin(['action'=>'/books/search', 'options' => ['method' => 'post']]) ?>
                
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'search_type')->dropDownList( 
                        $searchModel->getTypes(), 
                        [
                            //'prompt' => 'Выберите',
                            'value' => $post['BooksSearch']['search_type'],
                        ]) 
                    ?>
                </div>

                <div class="col-md-2">
                    <?= $form->field($searchModel, 'search_condition')->dropDownList( $searchModel->getConditions(), ['value' => $post['BooksSearch']['search_condition'],]) ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($searchModel, 'search_found')->dropDownList( $searchModel->getFoundationList(), ['value' => $post['BooksSearch']['search_found'] ]) ?>
                </div>

                <div class="col-md-3">
                    <?= $form->field($searchModel, 'search_area')->textInput(['value' => $post['BooksSearch']['search_area']]) ?>
                </div>

                <?= $form->field($searchModel, 'search_advanced')->hiddenInput(['value' => 1])->label(false) ?>

                <div class="col-md-1" style="margin-top: 25px;">
                    <?= Html::submitButton('Найти', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end() ?>

</div>
