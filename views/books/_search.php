<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BooksSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-search">

    <?php $form = ActiveForm::begin(['action'=>'/books/search', 'options' => ['method' => 'post']]) ?>
    <div class="col-md-11">
        <?= $form->field($searchModel, 'search_area')->textInput(['placeholder' => 'Введите текст для поиска', 'value' => $post['BooksSearch']['search_area'] ])->label('') ?>
    </div>
    <?= $form->field($searchModel, 'search_advanced')->hiddenInput(['value' => null])->label(false) ?>

    <div class="col-md-1">
        <div class="form-group" style="margin-top: 20px;">
            <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
