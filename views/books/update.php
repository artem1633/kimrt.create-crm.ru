<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Books */

$this->title = 'Изменить : ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Издания', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="books-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
