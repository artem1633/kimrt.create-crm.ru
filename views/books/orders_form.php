<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Foundation;

/* @var $this yii\web\View */
/* @var $model app\models\Zayavka */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zayavka-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">

            <div class="col-md-5">
                <?= $form->field($model, 'library_fond')->dropDownList(
                    $model->getFoundList($model->book_id), 
                    [
                        'onchange' => '
                            var a = parseInt( $("#orders-book_id").val() );
                            //var floor = parseInt( $("#house-floor").val() );
                            
                            $.post( "book-count?a="+a+"&id="+$(this).val(), function( data ){
                                //alert(data);$("#count").val(data);
                            });
                        ',
                    ]) ?>                       
            </div>  

            <div class="col-md-4">
                <?php
                    echo $form->field($model, 'planing_date')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy',
                        ]
                    ]);
                ?>                            
            </div> 

            <div class="col-md-3">
                <?php
                    echo $form->field($model, 'end_date')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Выберите дату','value' => date('d.m.Y')],
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy',
                        ]
                    ]);
                ?>
                <?= $form->field($model, 'user_id')->hiddenInput(['value' => \Yii::$app->user->identity->id])->label(false) ?>
                <?= $form->field($model, 'record_book')->hiddenInput(['value' => \Yii::$app->user->identity->id])->label(false) ?>
                <?= $form->field($model, 'book_id')->hiddenInput()->label(false) ?>
            </div>                   
        </div> 

    <?php ActiveForm::end(); ?>    
</div>