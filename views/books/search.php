<?php

use app\models\Books;
use app\models\BooksSections;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use app\models\BooksSearch;
use app\models\Foundation;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\bootstrap\Modal;

CrudAsset::register($this);

$this->title = 'Электронный каталог';
$this->params['breadcrumbs'][] = $this->title;

$booksSections = BooksSections::find()->asArray()->all();
$models = Books::find()->asArray()->all();

?>

<?php if($advanced == 1) { ?>
    <div class="box box-info box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Расширенный поиск </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                <?= Html::a( ' <i class="btn btn-warning fa  text-danger pull-right"> Обычный поиск </i>', ['search?advanced=null'], [])?>
            </div>
        </div>
        <div class="box-body">
          <?php echo $this->render('advanced_search', ['post' => $post, 'searchModel' => $searchModel]); ?>
        </div>
    </div>

<?php } else { ?>

    <div class="box box-warning box-solid ">
        <div class="box-header with-border">
            <h3 class="box-title">Поиск</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                <?= Html::a( ' <i class="btn btn-info fa  text-danger pull-right"> Расширенный поиск </i>', ['search?advanced=1'], [])?>
            </div>
        </div>
        <div class="box-body">
          <?php echo $this->render('_search', ['post' => $post, 'searchModel' => $searchModel]); ?>
        </div>
    </div>
<?php } ?>

<?php echo $this->render('_keyboard', ['post' => $post, 'searchModel' => $searchModel]); ?>

<?php 
    if($advanced == 1) { 

        $sections = BooksSections::find()->all();

        foreach ($sections as $section) {
            $searchModel = new BooksSearch();
            $provider = $searchModel->searchAdvancedList($post,$section->id);
            $class = 'primary';
            if($post['BooksSearch']['search_area'] != null || $post['BooksSearch']['search_found'] != 0  || isset($post['letter'])){
                if($provider->getCount() > 0) $class = 'success';
            }
?>

        <div class="box box-<?=$class?> box-solid collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title"><?=$section->name?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
            </div>
            <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $provider,
                'id'=>'crud-datatable',
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    'author',
                    [
                        'attribute' => 'book_section_id',
                        'value' => 'bookSection.name',
                    ],
                    [
                        'attribute' => 'library_fond',
                        'value' => 'libraryFond.name',
                    ],
                    'id_book',
                    'title',
                    'publishing_address',
                    'publishing_name',
                    'publishing_year',
                    [
                        'attribute' => 'ordering',
                        'header' => 'Заказать',
                        'content' => function ($data) {
                            return Html::a( '<center><i class="fa fa-download"></i></center>', ['order','id' => $data->id, 'found_id' => $data->library_fond ], ['role'=>'modal-remote',]);
                        },
                    ],
                ],
            ]); ?>
            </div>
        </div>
<?php   } 
    }
    else { 

        $sections = BooksSections::find()->all();

        foreach ($sections as $section) {
            $searchModel = new BooksSearch();
            $provider = $searchModel->searchBooksList($post,$section->id);
            $class = 'primary';
            if($post['BooksSearch']['search_area'] != null || isset($post['letter'])){
                if($provider->getCount() > 0) $class = 'success';
            }
    ?>

        <div class="box box-<?=$class?> box-solid collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title"><?=$section->name?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
            </div>
            <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $provider,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    'author',
                    /*[
                        'attribute' => 'book_section_id',
                        'value' => 'bookSection.name',
                    ],*/
                    'id_book',
                    'title',
                    'publishing_address',
                    'publishing_name',
                    'publishing_year',
                    [
                        'attribute' => 'ordering',
                        'header' => 'Заказать',
                        'content' => function ($data) {
                            return Html::a( '<center><i class="fa fa-download"></i></center>', ['order','id' => $data->id, 'found_id' => $data->library_fond ], ['role'=>'modal-remote',]);
                        },
                    ],
                ],
            ]); ?>
            </div>
        </div>
    <?php } ?>
<?php } ?>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size"=>"modal-lg",
    //"size" => "modal-wide",
    "options" => [
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>