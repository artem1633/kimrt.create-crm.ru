<?php

use app\models\Books;
use app\models\BooksSections;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Книжный фонд';
$this->params['breadcrumbs'][] = $this->title;

$booksSections = BooksSections::find()->asArray()->all();
$models = Books::find()->asArray()->all();

$datasetNames = array_values(ArrayHelper::map($booksSections, 'id', 'name'));
$datasetPublishings = array_unique(array_values(ArrayHelper::map($models, 'id', 'publishing_name')));
$datasetTitles = array_values(ArrayHelper::map($models, 'id', 'title'));
$datasetAuthor = array_values(ArrayHelper::map($models, 'id', 'author'));
$datasetPublishingsAddresses = array_values(ArrayHelper::map($models, 'id', 'publishing_address'));
$datasetPublishingYears = array_values(ArrayHelper::map($models, 'id', 'publishing_year'));
$datasetBBK = array_values(ArrayHelper::map($models, 'id', 'bbk'));

?>
    <div class="box box-default">
        <div class="box-body">

        <?php
        if (Yii::$app->user->identity->type == "admin") {
            ?>
            <div class="box-header">
                <?php $form = ActiveForm::begin(['action'=>'/books/import', 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]) ?>

                <?= $form->field($model, 'file')->label(Yii::t('app', 'Загрузить .xml'),['class'=>'btn btn-primary'])
                    ->fileInput(['class'=>'sr-only']) ?>

                <button type="submit" class="btn btn-success btn-flat"><?= Yii::t('app', 'Загрузить') ?></button>

                <?php ActiveForm::end() ?>
            </div>
        <?php } ?>

        </div>
    </div>

<div class="books-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('<i class="fa fa-plus"></i> Добавить', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-warning"> <b>Логирование </b></button>
            <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li> <?= Html::a( '<b>Просмотр</b>', ['/site/view-changing?table=books&title='.'Книжный фонд'], [])?></li>
                <li> <?= Html::a( '<b>Выгрузка</b>', ['/site/print-changing?table=books'], [])?></li>
                <li> <?= Html::a( '<b>Очистка</b>', ['/site/delete-changing?table=books'], [])?></li>
            </ul>
        </div>
    </div>
    <div class="box-body table-responsive">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'width' => '10px',
                    'hAlign' => GridView::ALIGN_CENTER,
                ],
                [
                    'attribute' => 'author',
                    'filterType' => GridView::FILTER_TYPEAHEAD,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['highlight' => true],
                        'dataset' => [[
                            'local' => count($datasetAuthor) != 0 ? $datasetAuthor : [''],
                        ]],
                    ],
                ],
                [
                    'attribute' => 'title',
                    'filterType' => GridView::FILTER_TYPEAHEAD,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['highlight' => true],
                        'dataset' => [[
                                'local' => count($datasetTitles) != 0 ? $datasetTitles : [''],
                        ]],
                    ],
                ],
                [
                    'attribute' => 'publishing_address',
                    'filterType' => GridView::FILTER_TYPEAHEAD,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['highlight' => true],
                        'dataset' => [[
                            'local' => count($datasetPublishingsAddresses) != 0 ? $datasetPublishingsAddresses : [''],
                        ]],
                    ],
                ],
                [
                    'attribute' => 'publishing_name',
                    'filterType' => GridView::FILTER_TYPEAHEAD,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['highlight' => true],
                        'dataset' => [[
                                'local' => count($datasetPublishings) != 0 ? $datasetPublishings : [''],
                            ]
                        ],
                    ],
                ],
                [
                    'attribute' => 'book_section_id',
                    'value' => 'bookSection.name',
                    'filterType' => GridView::FILTER_TYPEAHEAD,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['highlight' => true],
                        'dataset' => [[
                            'local' => count($datasetNames) != 0 ? $datasetNames : [''],
                        ]
                        ],
                    ],
                ],
                [
                    'attribute' => 'publishing_year',
                    'hAlign' => GridView::ALIGN_CENTER,
                    'width' => '150px',
                    'filterType' => GridView::FILTER_TYPEAHEAD,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['highlight' => true],
                        'dataset' => [[
                            'local' => count($datasetPublishingYears) != 0 ? $datasetPublishingYears : [''],
                        ]
                        ],
                    ],
                ],
                [
                    'attribute' => 'bbk',
                    'width' => '150px',
                    'hAlign' => GridView::ALIGN_CENTER,
                    'filterType' => GridView::FILTER_TYPEAHEAD,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['highlight' => true],
                        'dataset' => [[
                            'local' => count($datasetBBK) != 0 ? $datasetBBK : [''],
                        ]
                        ],
                    ],
                ],
                'id_book',
                'count',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{load} {update} {delete}',
                    'buttons' => [
                        'load' => function ($url, $model, $key){
                            return Html::a('<i class="fa fa-upload"></i>', $url);
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
