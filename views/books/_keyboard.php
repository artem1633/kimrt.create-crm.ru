<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="books-search" style="margin-left: 15px;">

    <?php $form = ActiveForm::begin(['action'=>'/books/search', 'options' => ['method' => 'post']]) ?>

    <div class="form-group" >

    	<?= $form->field($searchModel, 'search_advanced')->hiddenInput(['value' => $post['BooksSearch']['search_advanced']])->label(false) ?>
    	<?= $form->field($searchModel, 'search_type')->hiddenInput(['value' => $post['BooksSearch']['search_type']])->label(false) ?>
    	<?= $form->field($searchModel, 'search_condition')->hiddenInput(['value' => $post['BooksSearch']['search_condition']])->label(false) ?>
    	<?= $form->field($searchModel, 'search_found')->hiddenInput(['value' => $post['BooksSearch']['search_found']])->label(false) ?>
    	<?= $form->field($searchModel, 'search_area')->hiddenInput(['value' => $post['BooksSearch']['search_area']])->label(false) ?>

		<?php
		for ($i = 65; $i < 91 ; $i++) { ?>
	        <?= Html::submitButton( chr($i), [ 'name' => 'letter', 'value' => chr($i), 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
		<?php }	?>
	
        <?= Html::submitButton( 'А', [ 'name' => 'letter', 'value' => 'А', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Б', [ 'name' => 'letter', 'value' => 'Б', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'В', [ 'name' => 'letter', 'value' => 'В', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Г', [ 'name' => 'letter', 'value' => 'Г', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Д', [ 'name' => 'letter', 'value' => 'Д', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Е', [ 'name' => 'letter', 'value' => 'Е', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Ж', [ 'name' => 'letter', 'value' => 'Ж', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'З', [ 'name' => 'letter', 'value' => 'З', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'И', [ 'name' => 'letter', 'value' => 'И', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Й', [ 'name' => 'letter', 'value' => 'Й', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'К', [ 'name' => 'letter', 'value' => 'К', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Л', [ 'name' => 'letter', 'value' => 'Л', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'М', [ 'name' => 'letter', 'value' => 'М', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Н', [ 'name' => 'letter', 'value' => 'Н', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'О', [ 'name' => 'letter', 'value' => 'О', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'П', [ 'name' => 'letter', 'value' => 'П', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Р', [ 'name' => 'letter', 'value' => 'Р', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'С', [ 'name' => 'letter', 'value' => 'С', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Т', [ 'name' => 'letter', 'value' => 'Т', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'У', [ 'name' => 'letter', 'value' => 'У', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Ф', [ 'name' => 'letter', 'value' => 'Ф', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Х', [ 'name' => 'letter', 'value' => 'Х', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Ц', [ 'name' => 'letter', 'value' => 'Ц', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Ч', [ 'name' => 'letter', 'value' => 'Ч', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Ш', [ 'name' => 'letter', 'value' => 'Ш', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Щ', [ 'name' => 'letter', 'value' => 'Щ', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Ъ', [ 'name' => 'letter', 'value' => 'Ъ', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Ы', [ 'name' => 'letter', 'value' => 'Ы', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Ь', [ 'name' => 'letter', 'value' => 'Ь', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Э', [ 'name' => 'letter', 'value' => 'Э', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Ю', [ 'name' => 'letter', 'value' => 'Ю', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::submitButton( 'Я', [ 'name' => 'letter', 'value' => 'Я', 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
	
		<?php
		for ($i = 0; $i < 10 ; $i++) { ?>
	        <?= Html::submitButton( $i, [ 'name' => 'letter', 'value' => $i, 'style' => 'margin-top: 4px;', 'class' => 'btn btn-sm btn-primary']) ?>
		<?php }	?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
