<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Books */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Книжный фонд', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
