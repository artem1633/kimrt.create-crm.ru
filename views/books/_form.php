<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\BooksSections;
use app\models\Foundation;

/* @var $this yii\web\View */
/* @var $model app\models\Books */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'library_fond')->dropDownList(ArrayHelper::map(Foundation::find()->all(), 'id', 'name'), ['prompt' => 'Выберите фонда']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'author')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            </div>
        </div>


        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'book_section_id')->dropDownList(ArrayHelper::map(BooksSections::find()->all(), 'id', 'name'), ['prompt' => 'Выберите раздел']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'publishing_address')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'publishing_name')->textInput(['maxlength' => true]) ?>
            </div>


        </div>
        
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'id_book')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'publishing_year')->textInput() ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'bbk')->textInput(['maxlength' => true]) ?>                
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'count')->textInput(['type' => 'number']) ?>
            </div>
        </div>


    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
