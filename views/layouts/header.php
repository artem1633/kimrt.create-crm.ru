<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$fio = '';
if(Yii::$app->user->identity != null){
    $fio = Yii::$app->user->identity->name;
}

?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">KIMRT</span><span class="logo-lg">Личный кабинет</span>', '/info', ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" onclick="$.get('site/mymenu', {'id':1}, function(data){ } );" class="sidebar-toggle" data-toggle="push-menu"
           role="button"><span class="sr-only">Toggle navigation</span> </a>

        <?= Html::a('<span class="logo-mini">ФИО</span><span class="logo-lg">'.$fio.'</span>', '#', ['class' => 'logo', 'style' => 'background-color:#3c8dbc; font-size:14px;']) ?>

        <?php
        if (Yii::$app->user->identity!=null) {
            ?>

            <div class="navbar-custom-menu">

                <ul class="nav navbar-nav">

                    <li class="dropdown user user-menu">
                        <?= Html::a(
                            Yii::t('app', 'Выйти'),
                            ['/site/logout'],
                            ['data-method' => 'post']
                        ) ?>
                    </li>
                </ul>
            </div>

        <?php } ?>

        <?php
        if (Yii::$app->user->identity==null) {
            ?>

            <div class="navbar-custom-menu">

                <ul class="nav navbar-nav">

                    <li class="dropdown user user-menu">
                        <?= Html::a(
                            Yii::t('app', 'Войти'),
                            ['/site/login'],
                            ['data-method' => 'get']
                        ) ?>
                    </li>

                </ul>
            </div>

        <?php } ?>

    </nav>
</header>
