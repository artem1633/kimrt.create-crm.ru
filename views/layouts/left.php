<?php 
use yii\helpers\Html;
?>
<aside class="main-sidebar">

    <section class="sidebar">


        <?php

        if (Yii::$app->user->identity!=null) {
            $student = \app\models\Student::find()->where(['user_id' => Yii::$app->user->identity->id])->one();

            if ($student!=null) {
                $myMessages = \app\models\Message::find()->where(['student_id' => $student->id])->andWhere(['status' => \app\models\Message::STATUS_NOT_SEEN])->count('id');

                $allMessages = $student->new_messages;
                $messages = $myMessages + $allMessages;

                $tabMessages = 'Сообщения
                    <span class="pull-right-container">
					<span class="label label-danger pull-right">' . $messages . '</span>
					</span>';
            }
            else
            {
                $tabMessages = 'Сообщения';
            }

            $image = [
                //['label' => 'Menu ТЕО', 'options' => ['class' => 'header']],
                ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/users'], 'visible' => (Yii::$app->user->identity->type == 'admin')],
                ['label' => 'Студенты', 'icon' => 'book', 'url' => ['/student'], 'visible' => (Yii::$app->user->identity->type == 'admin')],
                ['label' => 'Заказы', 'icon' => 'archive', 'url' => ['/orders'], 'visible' => (Yii::$app->user->identity->type == 'admin')],
                ['label' => 'Успеваемость', 'icon' => 'file-text-o', 'url' => ['/academic-perfomance']],
                ['label' => 'Библиотека', 'icon' => 'book',
                    'items' => [
                        ['label' => 'Книжный фонд', 'icon' => 'book', 'url' => ['/books'], 'visible' => (Yii::$app->user->identity->type == 'admin')],
                        ['label' => 'Электронный каталог', 'icon' => 'book', 'url' => ['/books/search']],
                        ['label' => 'Фонды библиотек', 'icon' => 'book', 'url' => ['/foundation'], 'visible' => (Yii::$app->user->identity->type == 'admin')],
                        ['label' => 'Библиотеки', 'icon' => 'book', 'url' => ['/library']],
                        ['label' => 'Электронный формуляр', 'icon' => 'book', 'url' => ['/download-books']],
                    ],
                ],
                ['label' => 'Справочники', 'icon' => 'wrench', 'url' => ['/portfolio'],
                    'items' => [
                        ['label' => 'Группы', 'icon' => 'book', 'url' => ['/group']],
                        ['label' => 'Специализации', 'icon' => 'book', 'url' => ['/specialty']],
                        ['label' => 'Разделы книг', 'icon' => 'book', 'url' => ['/books-sections']],
                        ['label' => 'Курсы', 'icon' => 'book', 'url' => ['/course']],
                        ['label' => 'Преподаватели', 'icon' => 'black-tie', 'url' => ['/teacher']],
                        ['label' => 'Аудитории', 'icon' => 'cube', 'url' => ['/room']],
                        ['label' => 'Дисцплины', 'icon' => 'book', 'url' => ['/subject']],
                        ['label' => 'Сообщения', 'icon' => 'envelope-o', 'url' => ['/messages']],

                    ],
                    'visible' => (Yii::$app->user->identity->type == 'admin')
                ],

                ['label' => 'Расписание', 'icon' => 'calendar', 'url' => ['/'],
                    'items' => [
                        ['label' => 'Просмотреть', 'icon' => 'eye', 'url' => ['/schedule/index']],
                        ['label' => 'Время', 'icon' => 'clock-o', 'url' => ['/time'], 'visible' => (Yii::$app->user->identity->type == 'admin')],
                        ['label' => 'Учебные периоды', 'icon' => 'calendar', 'url' => ['/date'], 'visible' => (Yii::$app->user->identity->type == 'admin')],
                        ['label' => 'Редактировать расписание', 'icon' => 'pencil-square-o', 'url' => ['/schedule-crud'], 'visible' => (Yii::$app->user->identity->type == 'admin')],
                    ],
                ],

                ['label' => 'Портфолио', 'icon' => 'file-o', 'url' => ['/portfolio'],
                    'items' => [
                        ['label' => 'Автобиография', 'icon' => 'square-o', 'url' => ['/autobiography']],
                        ['label' => 'Курсовые и дипломные', 'icon' => 'square-o', 'url' => ['/work']],
                        ['label' => 'Предметные олимпиады', 'icon' => 'square-o', 'url' => ['/olympiad']],
                        ['label' => 'Доп. образование', 'icon' => 'square-o', 'url' => ['/additional-education']],
                        ['label' => 'Научные проекты', 'icon' => 'square-o', 'url' => ['/scientific-projects']],
                        ['label' => 'Конкурсы проектов', 'icon' => 'square-o', 'url' => ['/contest/index']],
                        ['label' => 'Внеучебная активность', 'icon' => 'square-o', 'url' => ['/extracurricular']],
                        ['label' => 'Спортивные достижения', 'icon' => 'square-o', 'url' => ['/sport']],
                        ['label' => 'Творческие достижения', 'icon' => 'square-o', 'url' => ['/art']],
                        ['label' => 'Выгрузить портфолио', 'icon' => 'file-pdf-o', 'url' => '/portfolio/export-pdf', 'template'=> '<a href="{url}" target="_blank">{label}</a>',
                            'visible' => (Yii::$app->user->identity->type == 'student')],
                    ],
                    'visible' => (Yii::$app->user->identity->type == 'student')
                ],
                ['label' => 'Логи', 'icon' => 'history', 'url' => ['/site/all-changing-view'], 'visible' => (Yii::$app->user->identity->type == 'admin')],

                ['label' => $tabMessages, 'icon' => 'envelope-o', 'url' => ['/messages'], 'encode' => false, 'visible' => (Yii::$app->user->identity->type == 'student')],
                
                ['label' => 'Образовательный портал', 'icon' => 'bank', 'url' => ['#'] , 'options' => ['onclick' => 'document.getElementById("myform").submit(); return false;']],
            ];
        }
        else {
            $image = [
                ['label' => 'Просмотреть', 'icon' => 'eye', 'url' => ['/schedule/index']],
            ];
        }
        ?>

        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => $image,
            ]
        ) ?>

    </section>

</aside>

                    
<form  target="_blank" action="http://e.afvgavt.ru/login/index.php" method="POST" id="myform">
    <input type="hidden" name="username" value="<?=Yii::$app->user->identity->login?>">
    <input type="hidden" name ="password" value="<?=Yii::$app->user->identity->passwd?>">

    <?= Html::submitButton('') ?>

</form>
                    