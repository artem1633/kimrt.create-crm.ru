<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Art */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Творческие достижения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="art-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'type',
            'level',
            'competition_name',
            'date',
            'result',
            'work',
            'document',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
