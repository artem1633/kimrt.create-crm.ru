<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Art */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="art-form">
    <div class="box box-default"">
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'level')->dropDownList([
            'Внутривузовский' => 'Внутривузовский',
            'Межвузовский' => 'Межвузовский',
            'Районный'=>'Районный',
            'Городской'=>'Городской',
            'Областной'=>'Областной',
            'Федеральный'=>'Федеральный',
            'Международный'=>'Международный',
        ]);
        ?>

        <?= $form->field($model, 'competition_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
            //'language' => 'ru',
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]) ?>

        <?= $form->field($model, 'result')->dropDownList([
            'Благ.письмо' => 'Благ.письмо',
            'Сертификат' => 'Сертификат',
            'Грамота'=>'Грамота',
            'Диплом'=>'Диплом',
        ]);
        ?>

        <?= $form->field($model, 'result')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'file')->label(Yii::t('app', 'Документ'))
            ->fileInput() ?>

        <?= $form->field($model, 'fileDocument')->label(Yii::t('app', 'Рецензия (файл)'))
            ->fileInput() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php if (Yii::$app->user->identity->type == "admin") { ?>
            <?= $form->field($model, 'is_approved')->checkBox(['label' => 'Утверждено', 'selected' => $model->is_approved, 'disabled' => true]) ?>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
</div>
