<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ArtSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="art-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'level') ?>

    <?= $form->field($model, 'competition_name') ?>

    <?= $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'result') ?>

    <?php // echo $form->field($model, 'work') ?>

    <?php // echo $form->field($model, 'document') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Сбросить', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
