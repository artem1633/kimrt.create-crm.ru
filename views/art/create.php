<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Art */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Творческие достижения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="art-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
