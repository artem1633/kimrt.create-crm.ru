<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Art */

$this->title = 'Изменить';
$this->params['breadcrumbs'][] = ['label' => 'Творческие достижения', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="art-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
