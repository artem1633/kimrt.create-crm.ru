<?php
use kartik\date\DatePicker;
use yii\helpers\Html;

$this->title = 'Расписание';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Javascrip функция для подсветки ячейки таблицы -->
<script type="text/javascript">
    function showChanges(id) {
        var b = document.getElementById("_" + id);
        var td = b.parentNode;
        td.className = 'danger';
    }
</script>

<?php
if (!Yii::$app->user->isGuest) {
    if (Yii::$app->user->identity->type == "admin") {
    ?>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="container">
                    <div class="box-header">
                        <span> <?= Html::a('Скопировать', ['copy'], ['class' => 'btn btn-success btn-flat']) ?> Скопировать последние 2 недели </span>
                    </div>
                </div>
            </div>
        </div>
<?php
    }
}
?>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="container">

            <form>

                <div class="box-body">
                    <div class="row">

                        <div class="col-md-3 vcenter">
                            <label for="date_picker">Дата:</label>
                            <div id="date_picker" class="form-group">
                                <?php
                                echo DatePicker::widget([
                                    'name' => 'date',
                                    'value' => date('Y-m-d'),
                                    'options' => ['placeholder' => 'Выберите дату'],
                                    'pluginOptions' => [
                                        'format' => 'yyyy-mm-dd',
                                        'todayHighlight' => true
                                    ],
                                    'pluginEvents' => [
                                        'changeDate' => 'function(e) {
                    
                    var date = document.getElementById("w0").value;
                    document.getElementById("link").href = "/schedule/index?date="+date;
                }'
                                    ]
                                ]);
                                ?>
                            </div>
                        </div>

                        <?php 
                            if(Yii::$app->user->identity != null){
                                if(Yii::$app->user->identity->type == 'student'){
                                    $student = \app\models\Student::find()->where(['user_id' => Yii::$app->user->identity->id])->one();
                                    if (!$student) {?>

                        <div class="col-md-3 vcenter">
                            <label for="dropdown">Группа:</label>
                            <div name="group" id="dropdown" class="form-group">
                                <?= Html::dropDownList('group', null, $groups, ['prompt'=>'Группа', 'label' => 'name', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                        <?php
                        
                                    }           
                                }       
                            } else {
                                ?>
                                
                        <div class="col-md-3 vcenter">
                            <label for="dropdown">Группа:</label>
                            <div name="group" id="dropdown" class="form-group">
                                <?= Html::dropDownList('group', null, $groups, ['prompt'=>'Группа', 'label' => 'name', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                                <?php



                            }
                        ?>

                        <div class="col-md-3 vcenter">
                            <label for="teacher">Преподаватель:</label>
                            <div id="teacher" class="form-group">
                                <?= Html::dropDownList('teacher', null, $teachers, ['prompt'=>'Преподаватели', 'label' => 'full_name', 'class' => 'form-control']) ?>
                            </div>
                        </div>


                        <div class="col-md-3 vcenter">
                            <label for="teacher">Аудитория:</label>
                            <div id="room" class="form-group">
                                <?= Html::dropDownList('room', null, $rooms, ['prompt'=>'Аудитория', 'label' => 'number', 'class' => 'form-control']) ?>
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-md-3 vcenter">
                            <div class="form-group">
                                <a id="link" href="">
                                    <button class="btn btn-success">Перейти</button>
                                </a>
                            </div>
                        </div>
                    </div>


                </div>
            </form>
        </div>
    </div>
</div>

<?php
if ($time != null) {
    ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="container">
                <table style="background-color: white" class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th></th>
                        <?php
                        foreach ($days as $day) {
                            ?>
                            <th><?= $day->day ?></th>
                            <?php
                        }
                        ?>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    foreach ($time as $t) {
                        ?>
                        <tr>
                            <td><?= substr($t->start, 0, 5) ?>-<?= substr($t->end, 0, 5) ?></td>

                            <?php
                            foreach ($days as $day) {
                                ?>
                                <td>
                                    <?php
                                    foreach ($t->schedule as $schedule) {
                                        if ($schedule->day_id == $day->id) {

                                            echo '<b id=_' . $schedule->id . '> </b>';
                                            /*
                                            *  Указываю id записи в расписании,
                                             * которая редактировалось и передаю в js функцию,
                                             * чтобы выделить ячейку
                                            */
                                            if ($schedule->is_changed) {
                                                echo ' <script type="text/javascript">',
                                                    'showChanges(' .  $schedule->id  . ');',
                                                '</script>'
                                                ;
                                            }

                                            ?>
                                            <?= $schedule->subject ?>
                                            <br>

                                            <br>
                                            <?php if ($schedule->teacher != null) echo $schedule->teacher->full_name ?>

                                            <br>
                                            <?php if ($schedule->group != null) echo 'группа-' . $schedule->group->name ?>

                                            <br>
                                            <?php if ($schedule->room != null) echo 'a.' . $schedule->room->number ?>
                                            <?php
                                        }
                                    }
                                    ?>

                                </td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                    }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
} else {
    ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="container">
                <table style="background-color: white" class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Понедельник</th>
                        <th>Вторник</th>
                        <th>Среда</th>
                        <th>Четверг</th>
                        <th>Пятница</th>
                        <th>Суббота</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>09:00-10:20</td>

                        <td>

                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>10:00-11:20</td>

                        <td>

                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>12:00-13:20</td>

                        <td>

                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
}
?>



