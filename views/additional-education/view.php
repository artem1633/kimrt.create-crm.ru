<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AdditionalEducation */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Дополнительное образование', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="additional-education-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'hours',
            'location_time',
            'document_name',
            'document',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
