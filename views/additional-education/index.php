<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AdditionalEducationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if (Yii::$app->user->identity->type == "student") {
    $this->title = 'Дополнительное образование';
    $this->params['breadcrumbs'][] = $this->title;
}
?>

<div class="additional-education-index">
    <div class="box box-default"">
    <div class="box-body">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php
        if (Yii::$app->user->identity->type == "student") {
            ?>

            
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            

            <?php
        }
        ?>

        <?php
            if (Yii::$app->user->identity->type == "admin") {
                $isVisible = true;
            }
            else {
                $isVisible = false;
            }
        ?>
        

        <?= GridView::widget([
            'caption' => 'Дополнительное образование',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'name',
                'hours',
                'location_time',
                'document_name',

                [
                    'attribute' => 'document',
                    'format' => 'raw',
                    'value' => function ($model) {

                        return Html::a('Скачать', $model->document, ['target'=>'_blank', 'role' => 'modal-remote']);
                    },
                ],

                [
                    'header' => 'Button',
                    'content' => function($model) use ($student) {
                        return Html::a('Одобрить', '/additional-education/approve?id=' . $model->id  . '&student=' . $student, ['class' => 'btn btn-success']);
                    },
                    'visible' => $isVisible,
                ],

                ['class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            if (Yii::$app->user->identity->type != "admin" && $model->is_approved == true)
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-view'),
                                ]);
                            else
                                return false;
                        },

                        'update' => function ($url, $model) use ($student) {
                            if (Yii::$app->user->identity->type != "admin" && $model->is_approved == false)
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '/additional-education/update?id=' . $model->id . '&student=' . $student, [
                                    'title' => Yii::t('app', 'lead-update'),
                                ]);
                            else if (Yii::$app->user->identity->type == "admin")
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '/additional-education/update?id=' . $model->id . '&student=' . $student, [
                                    'title' => Yii::t('app', 'lead-update'),
                                ]);
                            else if (Yii::$app->user->identity->type != "admin" && $model->is_approved == true)
                                return false;
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('app', 'lead-delete'),
                            ]);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
</div>
