<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AdditionalEducation */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Дополнительное образование', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="additional-education-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
