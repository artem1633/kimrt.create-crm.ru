<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdditionalEducation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="additional-education-form">
    <div class="box box-default"">
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'hours')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'location_time')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'document_name')->dropDownList([
            'сертификат' => 'сертификат',
            'удостоверение' => 'удостоверение',
            'диплом'=>'диплом',
        ]);
        ?>

        <?= $form->field($model, 'file')->label(Yii::t('app', 'Документ'))
            ->fileInput() ?>

        <?php if (Yii::$app->user->identity->type == "admin") { ?>
            <?= $form->field($model, 'is_approved')->checkBox(['label' => 'Утверждено', 'selected' => $model->is_approved, 'disabled' => true]) ?>
        <?php } ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
</div>
