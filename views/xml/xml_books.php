<?php

/**
 * @var $model \app\models\Books
 * @var $books \app\models\Books[]
 */

?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL ?>
<Book BBK_Code="<?=$model->bbk?>" Section="<?=$model->bookSection->name?>" year_o_publishing="<?=$model->publishing_year?>" Publishing_house="<?=$model->publishing_name?>" Place_of_Publication="<?=$model->publishing_address?>" Title="<?=$model->title?>" author="<?=$model->author?>" ID_Book="<?=$model->id_book?>">
    <Library>
<?php foreach ($books as $book): ?>
        <Library_Foundation quantity="<?=$book->quantity?>" name="<?=$book->foundation->name?>"/>
<?php endforeach; ?>
    </Library>
</Book>
