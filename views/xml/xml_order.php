<?php

?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL ?>
<student ID_Student="<?=$model->user->id?>" FIO="<?=$model->user->name?>">
    <Book Date="<?=\Yii::$app->formatter->asDate($model->planing_date, 'php:Ymd')?>" Library_Foundation="<?=$model->book->libraryFond->name?>" year_o_publishing="<?=$model->book->publishing_year?>" Publishing_house="<?=$model->book->publishing_name?>" Place_of_Publication="<?=$model->book->publishing_address?>" Title="<?=$model->book->title?>" author="<?=$model->book->author?>" ID_Book="<?=$model->id?>"/>
</student>