<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Contest */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Конкурсы проектов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
