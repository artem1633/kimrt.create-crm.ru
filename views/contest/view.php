<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contest */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Конкурсы проектов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'level',
            'date',
            'theme',
            'result',
            'work',
            'document',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
