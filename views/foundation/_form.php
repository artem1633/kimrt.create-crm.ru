<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Foundation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="foundation-form">
	<div class="box box-default">
        <div class="box-body">

		    <?php $form = ActiveForm::begin(); ?>

		    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

		    <div class="form-group">
		        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
		    </div>

		    <?php ActiveForm::end(); ?>
		</div>
	</div>

</div>
