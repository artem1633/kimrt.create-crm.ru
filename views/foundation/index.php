<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FoundationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фонды библиотеки ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="foundation-index">
    <div class="box box-default"">
        <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-warning"> <b>Логирование </b></button>
            <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li> <?= Html::a( '<b>Просмотр</b>', ['/site/view-changing?table=foundation&title='.'Фонды библиотеки'], [])?></li>
                <li> <?= Html::a( '<b>Выгрузка</b>', ['/site/print-changing?table=foundation'], [])?></li>
                <li> <?= Html::a( '<b>Очистка</b>', ['/site/delete-changing?table=foundation'], [])?></li>
            </ul>
        </div>
        <br>
        <br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
        ],
    ]); ?>
</div>
</div>
</div>