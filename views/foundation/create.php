<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Foundation */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Фонды библиотеки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="foundation-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
