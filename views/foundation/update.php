<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Foundation */

$this->title = 'Изменить';
$this->params['breadcrumbs'][] = ['label' => 'Фонды библиотеки', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="foundation-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
