<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ScientificProjects */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Scientific Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scientific-projects-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'level',
            'date',
            'theme',
            'publication',
            'work',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
