<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ScientificProjects */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Научные проекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scientific-projects-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
