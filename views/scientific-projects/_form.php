<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ScientificProjects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="scientific-projects-form">
    <div class="box box-default"">
    <div class="box-body">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <?php if (Yii::$app->user->identity->type == "admin") { ?>
            <?= $form->field($model, 'is_approved')->checkBox(['label' => 'Утверждено', 'selected' => $model->is_approved, 'disabled' => true]) ?>
        <?php } ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'level')->dropDownList([
            'Внутривузовский' => 'Внутривузовский',
            'Межвузовский' => 'Межвузовский',
            'Районный'=>'Районный',
            'Городской'=>'Городской',
            'Областной'=>'Областной',
            'Федеральный'=>'Федеральный',
            'Международный'=>'Международный',
        ]);
        ?>

        <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
            //'language' => 'ru',
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]) ?>

        <?= $form->field($model, 'theme')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'publication')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'work')->fileInput() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
</div>
