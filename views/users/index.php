<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

CrudAsset::register($this);

$this->title = 'Пользователи';
?>
    <div class="box box-default">
        <div class="box-body">

        <?php
        if (Yii::$app->user->identity->type == "admin") {
            ?>
            <div class="box-header">
                <?php $form = ActiveForm::begin(['action'=>'/users/import', 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]) ?>

                <?= $form->field($model, 'file')->label(Yii::t('app', 'Загрузить .csv'),['class'=>'btn btn-primary'])
                    ->fileInput(['class'=>'sr-only']) ?>

                <button type="submit" class="btn btn-success btn-flat"><?= Yii::t('app', 'Отправить') ?></button>

                <?php ActiveForm::end() ?>
            </div>
        <?php } ?>

        </div>
    </div>
	
	<div class="box box-default">	
		<div class="box-body" style="overflow-x: auto;">    
            
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-warning"> <b>Логирование </b></button>
                    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li> <?= Html::a( '<b>Просмотр</b>', ['/site/view-changing?table=users&title='.'Пользователи'], [/*'role'=>'modal-remote',*/])?></li>
                        <li> <?= Html::a( '<b>Выгрузка</b>', ['/site/print-changing?table=users'], [/*'target' => 'blank', 'data-pjax' => 0*/])?></li>
                        <li> <?= Html::a( '<b>Очистка</b>', ['/site/delete-changing?table=users'], [/*'target' => 'blank', 'data-pjax' => 0*/])?></li>
                        
                    </ul>
                </div>
                <br>
                <br>
            	<?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'layout' => "{items}\n{summary}\n{pager}",
                    'columns' => [
                        'name',
                        'login',
                        [
                            'attribute' => 'type',
                            'label' => 'Тип',
                            'content' => function($data)
                            {
                                $type = 'admin';
                                if ($data->type == 'admin') {
                                    $type = 'Администратор';
                                }
                                else if ($data->type == 'student') {
                                    $type = 'Студент';
                                }
                                else if ($data->type == 'student') {
                                    $type = 'Учитель';
                                }

                                return $type;
                            },
                        ],
                        'email',
                        'comment',
                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
    	</div>
    </div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>