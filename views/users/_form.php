<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-default"">
            <div class="box box-default">
                <div class="box-body">
                    <?php $form = ActiveForm::begin(); ?>

                    <div class="row">
                        <div class="col-md-4 vcenter">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 vcenter">
                            <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 vcenter">
                            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 vcenter">
                            <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 vcenter">
                            <?= $form->field($model, 'type')->dropDownList($model->getAllType())?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 vcenter">
                            <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div style="display:none">
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
</div>
