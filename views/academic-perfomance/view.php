<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AcademicPerfomance */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Успеваемость', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academic-perfomance-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Уверены, что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'course_id',
            'semester',
            'subject_id',
            'hours',
            'controll',
            'result',
            'ball',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
