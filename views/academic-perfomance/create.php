<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AcademicPerfomance */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Успеваемость', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academic-perfomance-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
