<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AcademicPerfomanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Успеваемость';
$this->params['breadcrumbs'][] = $this->title;
?>



    <?php
    if (Yii::$app->user->identity->type == "admin") {
        ?>
        <div class="box box-default"">
    <div class="box-body">
        <div class="box-header">
            <?php $form = ActiveForm::begin(['action'=>'/academic-perfomance/import', 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]) ?>

            <?= $form->field($uploadModel, 'file')->label(Yii::t('app', 'Загрузить .csv'),['class'=>'btn btn-primary'])
                ->fileInput(['class'=>'sr-only']) ?>

            <button type="submit" class="btn btn-success btn-flat"><?= Yii::t('app', 'Отправить') ?></button>
            <?php ActiveForm::end() ?>
        </div>
        
    </div>
</div>

    <?php } ?>


<div class="academic-perfomance-index">
    <div class="box box-default"">
        <div class="box-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= Html::a('Добавить запись', ['create'], ['class' => 'btn btn-success']) ?>
            
            <br>
            <br>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'student.surname',
                    //'student.name',
                    //'student.patronymic',
                    [
                    'attribute' => 'student.surname',
                    'label' => 'Ф И О',
                    'class' => 'yii\grid\DataColumn', 
                    'value' => function ($data) {
                        $name = '';
                        if ($data->student['name']) {
                                $name = $data->student['name'].' ';
                        }
                        $surname = '';
                        if ($data->student['surname']) {
                                $surname = $data->student['surname'].' ';
                        }

                        $patronymic = '';
                        if ($data->student['patronymic']) {
                                $patronymic = $data->student['patronymic'];
                        }
                        return $surname .$name .$patronymic ; 
                    },
                ],
                    'course_id',
                    'semester',
                    'subject.name',
                    'hours',
                    'controll',
                    'result',
                    'ball',
                    // 'created_at',
                    // 'updated_at',

                    ['class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                if (Yii::$app->user->identity->type == "admin")
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                        'title' => Yii::t('app', 'lead-view'),
                                    ]);
                                else
                                    return false;
                            },

                            'update' => function ($url, $model) use ($student) {
                                if (Yii::$app->user->identity->type == "admin")
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '/academic-perfomance/update?id=' . $model->id, [
                                        'title' => Yii::t('app', 'lead-update'),
                                    ]);
                                
                                else 
                                    return false;
                            },
                            'delete' => function ($url, $model) {
                                if (Yii::$app->user->identity->type == "admin")
                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'lead-delete'),
                                ]);
                                
                                else 
                                    return false;
                            }
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
