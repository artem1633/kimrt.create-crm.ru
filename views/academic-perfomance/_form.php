<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AcademicPerfomance */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="academic-perfomance-form">
    <div class="box box-default"">
        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'course_id')->dropDownList($model->getCourses()) ?>

            <?= $form->field($model, 'student_id')->dropDownList($model->getStudents()) ?>

            <?= $form->field($model, 'semester')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'subject_id')->label('Дисциплина')->dropDownList($model->getSubjects()) ?>

            <?= $form->field($model, 'hours')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'controll')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'result')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ball')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
