<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LibrarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Библиотека';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="library-index">
    <?php if(Yii::$app->user->identity->type != 'student') { ?>
        <div class="box box-default">
            <div class="box-body">
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-warning"> <b>Логирование </b></button>
                    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li> <?= Html::a( '<b>Просмотр</b>', ['/site/view-changing?table=library&title='.'Библиотека'], [])?></li>
                        <li> <?= Html::a( '<b>Выгрузка</b>', ['/site/print-changing?table=library'], [])?></li>
                        <li> <?= Html::a( '<b>Очистка</b>', ['/site/delete-changing?table=library'], [])?></li>
                    </ul>
                </div>
                <br>
                <br>
            </div>
        </div>
    <div class="box box-default">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    //'id',
                    [
                        'attribute'=>'logotip',
                        'label' => 'Логотип',
                        'content'=>function ($data){    
                            $path = "http://".$_SERVER['SERVER_NAME']."/logotips/".$data->image_name;             
                            return '<center>'.Html::img($path, [
                                            //'style' => 'width:70px; height:70px;',
                                ]). '</center>';                            
                         },
                    ],
                    'description',
                    /*[
                        'attribute'=>'logotip',
                        'content'=>function ($data){                 
                            return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->logotip.'   ', ['view-file','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';                             
                         },
                    ],*/
                    //'ssilka',
                    [
                        'attribute'=>'ssilka',
                        'content'=>function ($data){   
                         $path = "http://".$data->ssilka;              
                            return '<a target="_blank" href="'.$path.'">'.$data->ssilka.'</a>';                             
                         },
                    ],
                  
                    ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
                ],
            ]); ?>
        </div>
    </div>
<?php } else { ?>
        <div class="box box-default">
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'attribute'=>'logotip',
                            'label' => 'Логотип',
                            'content'=>function ($data){    
                                $path = "http://".$_SERVER['SERVER_NAME']."/logotips/".$data->image_name;             
                                return '<center>'.Html::img($path, [
                                            //'style' => 'width:70px; height:70px;',
                                ]). '</center>';                           
                             },
                        ],
                        'description',
                        /*[
                            'attribute'=>'logotip',
                            'content'=>function ($data){                 
                                return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->logotip.'   ', ['view-file','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';                             
                             },
                        ],*/
                        //'ssilka',
                        [
                            'attribute'=>'ssilka',
                            'content'=>function ($data){   
                             $path = "http://".$data->ssilka;              
                                return '<a target="_blank" href="'.$path.'">'.$data->ssilka.'</a>';                             
                             },
                        ],
                    ],
                ]); ?>
            </div>
        </div>
<?php } ?>

</div>
