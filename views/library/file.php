<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$path = "http://".$_SERVER['SERVER_NAME']."/logotips/".$model->logotip;
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	<div class="row">
		<div class="col-md-11" style="margin-left: 30px;">
			<img class="img-responsive" src=<?= $path ?> alt=<?= $path ?> title="">
		</div>		
	</div>
  <?php ActiveForm::end(); ?>