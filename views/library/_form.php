<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Library */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="library-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="box box-default">
    	<div class="box-body">
			<div class="row">
				<div class="col-md-4 vcenter">
			    	<?= $form->field($model, 'ssilka')->textInput(['maxlength' => true]) ?>
			    </div>

			    <div class="col-md-4 vcenter">
				    <?= $form->field($model, 'logotip')->textInput(['maxlength' => true]) ?>
				</div>

				<div class="col-md-4 vcenter">
				    <?= $form->field($upload, 'File')->fileInput() ?>
				</div>
		    </div>
		    <div class="row">
			    <div class="col-md-12 vcenter">
			    	<?= $form->field($model, 'description')->textArea(['rows' => 6]) ?>
			    <div class="form-group">
			        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			    </div>
			    </div>

			</div>
	</div>
</div>

    <?php ActiveForm::end(); ?>

</div>
