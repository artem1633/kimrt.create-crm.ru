<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Library */

$this->title = 'Добавить библиотеку';
$this->params['breadcrumbs'][] = ['label' => 'Библиотеки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="library-create">


    <?= $this->render('_form', [
        'model' => $model,       
    	'upload' => $upload,
    ]) ?>

</div>
