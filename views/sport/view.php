<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sport */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Спортивные достижения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sport-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'sport',
            'level',
            'competition',
            'date',
            'result',
            'document',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
