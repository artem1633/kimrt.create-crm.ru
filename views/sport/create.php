<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Sport */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Спортивные достижения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sport-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
