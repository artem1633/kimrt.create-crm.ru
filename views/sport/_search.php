<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sport-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sport') ?>

    <?= $form->field($model, 'level') ?>

    <?= $form->field($model, 'competition') ?>

    <?= $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'result') ?>

    <?php // echo $form->field($model, 'document') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
