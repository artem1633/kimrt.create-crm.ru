<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Users;

$this->title = 'История';
?>
<div class="box box-default"">
    <div class="box-body">
		<div class="table-responsive">
			<div class="btn-group pull-right">
	            <button type="button" class="btn btn-warning"> <b>Действия </b></button>
	            <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
	                <span class="caret"></span>
	            </button>
	            <ul class="dropdown-menu" role="menu">
	                <li> <?= Html::a( '<b>Выгрузка</b>', ['/site/print-changing?table=all'], [])?></li>
	                <li> <?= Html::a( '<b>Очистка</b>', ['/site/delete-changing?table=all'], [])?></li>
	            </ul>
	        </div>
			<?= GridView::widget([
		        'dataProvider' => $dataProvider,
		        'columns' => [
		        	['class' => 'yii\grid\SerialColumn'],
		        	[
			            'attribute' => 'table_name',
			            'value' => function($model) {
			                return $model->getTableName($model->table_name);
			            },
			        ],
		            [
			            'attribute' => 'date_time',
			            'value' => function($model) {
			                return \Yii::$app->formatter->asDate($model->date_time, 'php:H:i d.m.Y');
			            },
			        ],
			        [
			            'attribute' => 'user_id',
			            'value' => function($model) {
			                $user = Users::findOne($model->user_id);
			                return $user->name;
			            },
			        ],
			        'field',
			        'new_value',
			        'old_value',
		        ],
		    ]); ?>
		</div>
	</div>
</div>