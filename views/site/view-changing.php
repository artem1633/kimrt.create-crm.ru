<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Users;

$this->title = 'История';
$this->params['breadcrumbs'][] = ['label' => $title, 'url' => [$url]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-default"">
    <div class="box-body">
		<div class="table-responsive">
			<?= GridView::widget([
		        'dataProvider' => $dataProvider,
		        'columns' => [
		        	['class' => 'yii\grid\SerialColumn'],
		            [
			            'attribute' => 'date_time',
			            'value' => function($model) {
			                return \Yii::$app->formatter->asDate($model->date_time, 'php:H:i d.m.Y');
			            },
			        ],
			        [
			            'attribute' => 'user_id',
			            'value' => function($model) {
			                $user = Users::findOne($model->user_id);
			                return $user->name;
			            },
			        ],
			        'field',
			        'new_value',
			        'old_value',
		        ],
		    ]); ?>
		</div>
	</div>
</div>
<!-- <div class="table-responsive">
	<table class="table table-bordered">
		<tr>
			<th>№</th>
			<th>Даты и времени события</th>
			<th>Пользователь</th>
			<th>Объект системы</th>
			<th>Действие</th>
			<th>Состояние объекта до изменения</th>
		</tr>
		<?php /*$i=0;
			foreach ($changings as $change) {	
			$i++;			
			$user = \app\models\Users::findOne($change->user_id);*/
		?>		
			<tr>
				<td><?php //$i?></td>
				<td><?php //\Yii::$app->formatter->asDate($change->date_time, 'php:H:i d.m.Y')?></td>
				<td><?php //$user->name?></td>
				<td><?php //$change->field?></td>
				<td><?php //$change->new_value?></td>
				<td><?php //$change->old_value?></td>
			</tr>
		<?php // } ?>		
	</table>
</div> -->