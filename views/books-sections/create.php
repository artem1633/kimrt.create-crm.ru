<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BooksSections */

$this->title = 'Добавить новый раздел';
$this->params['breadcrumbs'][] = ['label' => 'Разделы книг', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-sections-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
