<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BooksSectionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $modelsNames array*/

$this->title = 'Разделы книг';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-sections-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box">
        <div class="box-body">
            <p>
                <?= Html::a('<i class="fa fa-plus"></i> Добавить раздел', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'options' => ['style' => 'width: 50px;'],
                    ],

                    [
                        'class' => '\kartik\grid\EditableColumn',
                        'attribute' => 'name',
                        'value' => 'name',
//                        'filterType' => GridView::FILTER_TYPEAHEAD,
//                        'filterWidgetOptions' => [
//                            'pluginOptions' => ['highlight' => true],
//                            'dataset' => ['local' => $modelsNames],
//                        ],
                        'editableOptions' => [
                            'formOptions' => [
                                'action' => Url::toRoute(['books-sections/edit']),
                            ],
                        ],
                    ],

                    ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}', 'options' => ['style' => 'width: 50px;']],
                ],
            ]); ?>
        </div>
        <!-- ./box-body -->
    </div>
</div>
