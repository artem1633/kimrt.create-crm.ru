<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Olympiad */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Предметные олимпиады', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="olympiad-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
