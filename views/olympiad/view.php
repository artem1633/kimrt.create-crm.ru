<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Olympiad */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Предметные олимпиады', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="olympiad-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'level',
            'location',
            'time',
            'result',
            'document',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
