<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Olympiad */

$this->title = 'Изменить';
$this->params['breadcrumbs'][] = ['label' => 'Предметные олимпиады', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="olympiad-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
