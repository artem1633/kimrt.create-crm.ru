<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Date */

$this->title = 'Добавить учебный период';
$this->params['breadcrumbs'][] = ['label' => 'Dates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="date-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
