<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Work */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="work-form">
    <div class="box box-default"">
    <div class="box-body">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <?php if (Yii::$app->user->identity->type == "admin") { ?>
            <?= $form->field($model, 'is_approved')->checkBox(['label' => 'Утверждено', 'selected' => $model->is_approved, 'disabled' => true]) ?>
        <?php } ?>

        <?= $form->field($model, 'subject_id')->label('Дисциплина', ['required' => true])->dropDownList($model->getSubjects())?>

        <?= $form->field($model, 'theme')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'file')->label(Yii::t('app', 'Работа (файл)'))
            ->fileInput() ?>

        <?= $form->field($model, 'fileReview')->label(Yii::t('app', 'Рецензия (файл)'))
            ->fileInput() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
</div>


