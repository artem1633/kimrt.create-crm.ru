<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WeekDay */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Дни недели', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="week-day-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
