<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Student */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">

        <div class="row">
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'user_id')->label('Учетная запись', ['required' => true])->dropDownList($model->getUserAccounts())?>
            </div>

            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'patronymic')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'date_birth')->textInput(['type' => 'date']) ?>
            </div>
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'floor')->dropDownList(
                        [
                            '1' => 'Мужской',
                            '2'=>'Женский'
                        ])?>
            </div>
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'level')->dropDownList(
                        [
                            '1' => 'Бакалавриат',
                            '2'=>'Специалитет',
                            '3'=>'СПО'
                        ])?>
            </div>
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'training')->dropDownList(
                        [
                            '1' => 'Очная',
                            '2'=>'Заочная',
                        ])?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'specialty_id')->dropDownList($model->getAllSpecialty()) ?>
            </div>
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'course_id')->dropDownList($model->getAllCourse())?>
            </div>
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'group_id')->dropDownList($model->getAllGroup())?>
            </div>
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'payment')->dropDownList(
                [
                    '1' => 'Бюджет',
                    '2'=>'Договор',
                ])?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'enrollment')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'enrollment_date')->textInput(['type' => 'date']) ?>
            </div>
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'expulsion')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'expulsion_date')->textInput(['type' => 'date']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'status')->dropDownList(
                    [
                        '1' => 'Обучается',
                        '2'=>'Отчислен',
                        '3'=>'Выпускник',
                    ])?>
            </div>
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'students_number')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'comment')->textInput()?>
            </div>
        </div>

    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
