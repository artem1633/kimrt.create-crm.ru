<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
CrudAsset::register($this);
$this->title = 'Студенты';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="box box-default"">
        <div class="box-body">

        <?php
        if (Yii::$app->user->identity->type == "admin") {
            ?>
            <div class="box-header">
                <?php $form = ActiveForm::begin(['action'=>'/student/import', 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]) ?>

                <?= $form->field($model, 'file')->label(Yii::t('app', 'Загрузить .csv'),['class'=>'btn btn-primary'])
                    ->fileInput(['class'=>'sr-only']) ?>

                <button type="submit" class="btn btn-success btn-flat"><?= Yii::t('app', 'Отправить') ?></button>

                <?php ActiveForm::end() ?>
            </div>
        <?php } ?>

        </div>
    </div>

<div class="student-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-warning"> <b>Логирование </b></button>
            <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li> <?= Html::a( '<b>Просмотр</b>', ['/site/view-changing?table=student&title='.'Студенты'], [/*'role'=>'modal-remote',*/])?></li>
                <li> <?= Html::a( '<b>Выгрузка</b>', ['/site/print-changing?table=student'], [])?></li>
                <li> <?= Html::a( '<b>Очистка</b>', ['/site/delete-changing?table=student'], [])?></li>
            </ul>
        </div>
        <br>
        <br>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

               // 'code',
               // 'surname',
                [
                    'attribute' => 'surname',
                    'label' => 'Ф И О',
                    'class' => 'yii\grid\DataColumn', 
                    'value' => function ($data) {
                        return $data->surname.' '.$data->name.' '.$data->patronymic; 
                    },
                ],
                'students_number',
               // 'name',
              //  'patronymic',
                 'date_birth',
                // 'floor',
                [
                    'attribute' => 'group_id',
                    'value' => 'group.name'
                ],
                // 'level',
                [
                    'attribute' => 'specialty_id',
                    'value' => 'specialty.name'
                ],
                [
                    'attribute' => 'course_id',
                    'value' => 'course.name'
                ],
                // 'training',
                // 'payment',
                // 'enrollment',
                 'enrollment_date',
                // 'expulsion',
                 'expulsion_date',
                // 'status',
                // 'comment',

                ['class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '/portfolio/index?student=' . $model->id, [
                                'title' => Yii::t('app', 'lead-view'),
                            ]);
                        },

                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'lead-update'),
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('app', 'lead-delete'),
                            ]);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>