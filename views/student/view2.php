<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;

use app\services\YandexDisk;

/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    table tr th {
        font-size: 10px;
    }
</style>
<script>
    function addPayment(id)
    {
        var modal = $('.modal');
        $.get('/autobiography/create',{'client_id':id}, function(data) {
            modal.html(data).modal('show');
        });
        return false;
    }
</script>
<div class="client-view">


    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">Информация о студенте</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'code',
                    'surname',
                    'name',
                    'patronymic',
                    'date_birth',
                    'floor',
                    'group_id',
                    'level',
                    'specialty_id',
                    'course_id',
                    'training',
                    'payment',
                    'enrollment',
                    'enrollment_date',
                    'expulsion',
                    'expulsion_date',
                    'status',
                    'comment',
                ],
            ]) ?>
        </div>
    </div>

    <div class="box box-default"">
        <div class="box-body">
            <ul class="nav nav-tabs">

                <li class="active"><a data-toggle="tab" href="#home">Автобиография</a></li>
                <li class="<?php if (isset($tab)) if ($tab == 2) echo 'active' ?>"><a data-toggle="tab" href="#menu1">Работы</a></li>
                <li class="<?php if (isset($tab)) if ($tab == 3) echo 'active' ?>"><a data-toggle="tab" href="#menu2">Предметные олимпиады</a></li>
                <li class="<?php if (isset($tab)) if ($tab == 4) echo 'active' ?>"><a data-toggle="tab" href="#menu3">Дополнительное образованиие</a></li>
                <li class="<?php if (isset($tab)) if ($tab == 5) echo 'active' ?>"><a data-toggle="tab" href="#menu4">Научные проекты</a></li>
                <li class="<?php if (isset($tab)) if ($tab == 6) echo 'active' ?>"><a data-toggle="tab" href="#menu5">Конкурсы проектов</a></li>
                <li class="<?php if (isset($tab)) if ($tab == 7) echo 'active' ?>"><a data-toggle="tab" href="#menu6">Внеучебная активность</a></li>
                <li class="<?php if (isset($tab)) if ($tab == 8) echo 'active' ?>"><a data-toggle="tab" href="#menu7">Спортивные достижения</a></li>
                <li class="<?php if (isset($tab)) if ($tab == 9) echo 'active' ?>"><a data-toggle="tab" href="#menu8">Творческие достижения</a></li>
                <!-- <li><a data-toggle="tab" href="#menu3">Договоры</a></li> -->
            </ul>

            <div class="tab-content">
                <div id="home" class=" tab-pane fade in active">

                        <div class="box-header with-border">
                            <button class="btn btn-success" onclick="addPayment(<?=$model->id?>)">Добавить</button> </div>
                        <div class="box-body table-responsive no-padding">
                            <div id="w0" class="grid-view"><table class="table table-striped table-bordered"><thead>
                                    <tr>
                                        <th>
                                            <a href="/project/index?sort=user_id" data-sort="user_id">Текст</a>
                                        </th>
                                        <th>
                                            <a href="/project/index?sort=user_id" data-sort="user_id">Действие</a>
                                        </th>
                                    </thead>
                                    <tbody>
                                    <tr data-key="2">
                                        <td>
                                            фыфафывафывафцуацуаывмыва
                                        </td>
                                        <td>

                                            <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                                Утвердить
                                            </a>
                                            \
                                            <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                                Отказать
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/project/delete?id=2" title="Удалить" aria-label="Удалить" data-pjax="0" data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                            <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                                <span class="glyphicon glyphicon-pencil">
                                                </span>
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody></table>
                                <div class="summary">Показаны записи <b>1-1</b> из <b>1</b>.</div>
                            </div>    </div>

                </div>
                <div id="menu1" class=" tab-pane fade ">

                    <div class="box-header with-border">
                        <a class="btn btn-success btn-flat" href="/project/create">Добавить</a>    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="w0" class="grid-view"><table class="table table-striped table-bordered"><thead>
                                <tr>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Текст</a>
                                    </th>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Действие</a>
                                    </th>
                                </thead>
                                <tbody>
                                <tr data-key="2">
                                    <td>
                                        фыфафывафывафцуацуаывмыва
                                    </td>
                                    <td>

                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Утвердить
                                        </a>
                                        \
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Отказать
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/project/delete?id=2" title="Удалить" aria-label="Удалить" data-pjax="0" data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                                <span class="glyphicon glyphicon-pencil">
                                                </span>
                                        </a>
                                    </td>
                                </tr>
                                </tbody></table>
                            <div class="summary">Показаны записи <b>1-1</b> из <b>1</b>.</div>
                        </div>    </div>


                </div>
                <div id="menu2" class=" tab-pane fade <?php if (isset($tab))if ($tab == 3) echo ' in active' ?>">

                    <div class="box-header with-border">
                        <a class="btn btn-success btn-flat" href="/project/create">Добавить</a>    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="w0" class="grid-view"><table class="table table-striped table-bordered"><thead>
                                <tr>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Текст</a>
                                    </th>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Действие</a>
                                    </th>
                                </thead>
                                <tbody>
                                <tr data-key="2">
                                    <td>
                                        фыфафывафывафцуацуаывмыва
                                    </td>
                                    <td>

                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Утвердить
                                        </a>
                                        \
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Отказать
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/project/delete?id=2" title="Удалить" aria-label="Удалить" data-pjax="0" data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                                <span class="glyphicon glyphicon-pencil">
                                                </span>
                                        </a>
                                    </td>
                                </tr>
                                </tbody></table>
                            <div class="summary">Показаны записи <b>1-1</b> из <b>1</b>.</div>
                        </div>    </div>


                </div>
                <div id="menu3" class=" tab-pane fade <?php if (isset($tab))if ($tab == 2) echo ' in active' ?>">

                    <div class="box-header with-border">
                        <a class="btn btn-success btn-flat" href="/project/create">Добавить</a>    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="w0" class="grid-view"><table class="table table-striped table-bordered"><thead>
                                <tr>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Текст</a>
                                    </th>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Действие</a>
                                    </th>
                                </thead>
                                <tbody>
                                <tr data-key="2">
                                    <td>
                                        фыфафывафывафцуацуаывмыва
                                    </td>
                                    <td>

                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Утвердить
                                        </a>
                                        \
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Отказать
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/project/delete?id=2" title="Удалить" aria-label="Удалить" data-pjax="0" data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                                <span class="glyphicon glyphicon-pencil">
                                                </span>
                                        </a>
                                    </td>
                                </tr>
                                </tbody></table>
                            <div class="summary">Показаны записи <b>1-1</b> из <b>1</b>.</div>
                        </div>    </div>


                </div>
                <div id="menu4" class=" tab-pane fade <?php if (isset($tab))if ($tab == 2) echo ' in active' ?>">

                    <div class="box-header with-border">
                        <a class="btn btn-success btn-flat" href="/project/create">Добавить</a>    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="w0" class="grid-view"><table class="table table-striped table-bordered"><thead>
                                <tr>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Текст</a>
                                    </th>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Действие</a>
                                    </th>
                                </thead>
                                <tbody>
                                <tr data-key="2">
                                    <td>
                                        фыфафывафывафцуацуаывмыва
                                    </td>
                                    <td>

                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Утвердить
                                        </a>
                                        \
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Отказать
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/project/delete?id=2" title="Удалить" aria-label="Удалить" data-pjax="0" data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                                <span class="glyphicon glyphicon-pencil">
                                                </span>
                                        </a>
                                    </td>
                                </tr>
                                </tbody></table>
                            <div class="summary">Показаны записи <b>1-1</b> из <b>1</b>.</div>
                        </div>    </div>


                </div>
                <div id="menu5" class=" tab-pane fade <?php if (isset($tab))if ($tab == 2) echo ' in active' ?>">

                    <div class="box-header with-border">
                        <a class="btn btn-success btn-flat" href="/project/create">Добавить</a>    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="w0" class="grid-view"><table class="table table-striped table-bordered"><thead>
                                <tr>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Текст</a>
                                    </th>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Действие</a>
                                    </th>
                                </thead>
                                <tbody>
                                <tr data-key="2">
                                    <td>
                                        фыфафывафывафцуацуаывмыва
                                    </td>
                                    <td>

                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Утвердить
                                        </a>
                                        \
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Отказать
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/project/delete?id=2" title="Удалить" aria-label="Удалить" data-pjax="0" data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                                <span class="glyphicon glyphicon-pencil">
                                                </span>
                                        </a>
                                    </td>
                                </tr>
                                </tbody></table>
                            <div class="summary">Показаны записи <b>1-1</b> из <b>1</b>.</div>
                        </div>    </div>


                </div>
                <div id="menu6" class=" tab-pane fade <?php if (isset($tab))if ($tab == 2) echo ' in active' ?>">

                    <div class="box-header with-border">
                        <a class="btn btn-success btn-flat" href="/project/create">Добавить</a>    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="w0" class="grid-view"><table class="table table-striped table-bordered"><thead>
                                <tr>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Текст</a>
                                    </th>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Действие</a>
                                    </th>
                                </thead>
                                <tbody>
                                <tr data-key="2">
                                    <td>
                                        фыфафывафывафцуацуаывмыва
                                    </td>
                                    <td>

                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Утвердить
                                        </a>
                                        \
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Отказать
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/project/delete?id=2" title="Удалить" aria-label="Удалить" data-pjax="0" data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                                <span class="glyphicon glyphicon-pencil">
                                                </span>
                                        </a>
                                    </td>
                                </tr>
                                </tbody></table>
                            <div class="summary">Показаны записи <b>1-1</b> из <b>1</b>.</div>
                        </div>    </div>


                </div>
                <div id="menu7" class=" tab-pane fade <?php if (isset($tab))if ($tab == 2) echo ' in active' ?>">

                    <div class="box-header with-border">
                        <a class="btn btn-success btn-flat" href="/project/create">Добавить</a>    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="w0" class="grid-view"><table class="table table-striped table-bordered"><thead>
                                <tr>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Текст</a>
                                    </th>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Действие</a>
                                    </th>
                                </thead>
                                <tbody>
                                <tr data-key="2">
                                    <td>
                                        фыфафывафывафцуацуаывмыва
                                    </td>
                                    <td>

                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Утвердить
                                        </a>
                                        \
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Отказать
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/project/delete?id=2" title="Удалить" aria-label="Удалить" data-pjax="0" data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                                <span class="glyphicon glyphicon-pencil">
                                                </span>
                                        </a>
                                    </td>
                                </tr>
                                </tbody></table>
                            <div class="summary">Показаны записи <b>1-1</b> из <b>1</b>.</div>
                        </div>    </div>


                </div>
                <div id="menu8" class=" tab-pane fade <?php if (isset($tab))if ($tab == 2) echo ' in active' ?>">

                    <div class="box-header with-border">
                        <a class="btn btn-success btn-flat" href="/project/create">Добавить</a>    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="w0" class="grid-view"><table class="table table-striped table-bordered"><thead>
                                <tr>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Текст</a>
                                    </th>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Действие</a>
                                    </th>
                                </thead>
                                <tbody>
                                <tr data-key="2">
                                    <td>
                                        фыфафывафывафцуацуаывмыва
                                    </td>
                                    <td>

                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Утвердить
                                        </a>
                                        \
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Отказать
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/project/delete?id=2" title="Удалить" aria-label="Удалить" data-pjax="0" data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                                <span class="glyphicon glyphicon-pencil">
                                                </span>
                                        </a>
                                    </td>
                                </tr>
                                </tbody></table>
                            <div class="summary">Показаны записи <b>1-1</b> из <b>1</b>.</div>
                        </div>    </div>


                </div>
                <div id="menu9" class=" tab-pane fade <?php if (isset($tab))if ($tab == 2) echo ' in active' ?>">

                    <div class="box-header with-border">
                        <a class="btn btn-success btn-flat" href="/project/create">Добавить</a>    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="w0" class="grid-view"><table class="table table-striped table-bordered"><thead>
                                <tr>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Текст</a>
                                    </th>
                                    <th>
                                        <a href="/project/index?sort=user_id" data-sort="user_id">Действие</a>
                                    </th>
                                </thead>
                                <tbody>
                                <tr data-key="2">
                                    <td>
                                        фыфафывафывафцуацуаывмыва
                                    </td>
                                    <td>

                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Утвердить
                                        </a>
                                        \
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                            Отказать
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/project/delete?id=2" title="Удалить" aria-label="Удалить" data-pjax="0" data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                        <a href="/project/update?id=2" title="Редактировать" aria-label="Редактировать" data-pjax="0">
                                                <span class="glyphicon glyphicon-pencil">
                                                </span>
                                        </a>
                                    </td>
                                </tr>
                                </tbody></table>
                            <div class="summary">Показаны записи <b>1-1</b> из <b>1</b>.</div>
                        </div>    </div>


                </div>
            </div>

        </div>


        <div class="row">
            <div class="modal comment">

            </div>
        </div>