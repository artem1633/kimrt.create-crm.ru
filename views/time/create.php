<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Time */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Время', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
