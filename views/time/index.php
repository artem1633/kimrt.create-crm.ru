<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Время';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-index box box-primary">


            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <div class="box-header with-border">
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
            </div>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

    //            'id',
                    'start',
                    'end',
    //            'created_at',
    //            'updated_at',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
